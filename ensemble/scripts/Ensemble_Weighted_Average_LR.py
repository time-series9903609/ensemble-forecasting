import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from prophet import Prophet
import orbit
from orbit.models import DLT 
import fileUtils 
import matplotlib.pyplot as plt 
from sklearn.linear_model import LinearRegression 


def read_input_data(file_name):
    input_df = fileUtils.read_csv(file_name)
    print(input_df.shape)
    print(input_df.head(5))
    return input_df

def preprocess_input_data(input_df):
    input_df = fileUtils.find_update_missing_dates(input_df)
    fileUtils.verify_dates_completeness(input_df)
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')
    input_df = input_df.sort_values(by='date')
    input_df.rename(columns={'date':'ds','sales':'y'}, inplace=True)
    return input_df

def group_input_data(input_df):
    grouped_df = input_df.groupby(['store_nbr', 'ds']).agg({'y':'sum', 
                                                            'transactions':'sum', 
                                                            'onpromotion':'sum'}).reset_index()
    grouped_df['test_indicator'] = 0
    grouped_df.loc[grouped_df['ds'].dt.year >= 2017, 'test_indicator'] = 1
    return grouped_df

def read_holiday_data(file_name):
    holiday_df = fileUtils.read_csv(file_name)
    print(holiday_df.columns)
    holiday_df = holiday_df.drop(holiday_df.columns[holiday_df.columns.str.contains('Unnamed',case=False)], axis=1)
    print(holiday_df.columns)
    return holiday_df

def fit_and_predict_prophet(train_df, test_df, holiday_df):
    model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    weekly_seasonality=True,
                    changepoint_range=0.95,
                    holidays=holiday_df,
                    holidays_mode='multiplicative',
                    seasonality_mode='multiplicative')
    model.add_regressor('onpromotion')
    model.add_regressor('transactions')
    model.fit(train_df)
    p_fitted = forecast = model.predict(train_df)
    p_forecast = model.predict(test_df)
    return p_fitted, p_forecast

def fit_and_predict_orbit(train_df, test_df):
    column_names = ['onpromotion', 'transactions']
    model = DLT(response_col='y', 
                date_col='ds', 
                seasonality=365,
                estimator='stan-map',
                global_trend_option='loglinear', 
                damped_factor=0.4, 
                regressor_col=column_names)
    model.fit(train_df)
    o_fitted = model.predict(train_df)
    o_forecast = model.predict(test_df)
    return o_fitted, o_forecast

def weighted_averaging_ensemble(test_df, p_forecast, o_forecast, weight=0.5):
    test_df.reset_index(inplace=True)
    p_forecast.reset_index(inplace=True)
    o_forecast.reset_index(inplace=True)
    
    # Calculate weighted forecast
    weighted_forecast = (p_forecast['yhat'] * weight) + (o_forecast['prediction'] * (1-weight))
    
    ensemble_forecast = pd.DataFrame()
    ensemble_forecast['ds'] = test_df['ds']
    ensemble_forecast['prophet_forecast'] = p_forecast['yhat']
    ensemble_forecast['orbit_forecast'] = o_forecast['prediction']
    ensemble_forecast['ensemble_forecast'] = weighted_forecast
    
    return ensemble_forecast

def weighted_average_weights(train_df, p_fitted, o_fitted):
    # Prepare data for the regression model
    X_train = pd.DataFrame({'Prophet_Forecast': p_fitted['yhat'], 'Orbit_Forecast': o_fitted['prediction']})
    y_train = train_df['y'].astype(float)  # Use actual target values as the target variable

    # Train a linear regression model
    regression_model = LinearRegression()
    regression_model.fit(X_train, y_train)

    # Get the coefficients as weights
    weights = regression_model.coef_

    # Normalize weights (optional)
    weights_normalized = weights / np.sum(weights)

    return weights_normalized



def calculate_metrics(test_df, ensemble_forecast):
    lr_mse = mean_squared_error(test_df['y'].astype(float), ensemble_forecast['ensemble_forecast'])
    lr_mae = mean_absolute_error(test_df['y'].astype(float), ensemble_forecast['ensemble_forecast'])
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(ensemble_forecast['ensemble_forecast'])
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': ['Ensemble Model'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)],
                            'MSE': [lr_mse],
                            'MAE': [lr_mae]}).round(2)
    print(results_lr)


if __name__ == "__main__":
    # Read input data
    input_df = read_input_data('grocery_one_series')

    # Preprocess input data
    input_df = preprocess_input_data(input_df)

    # Group input data
    grouped_df = group_input_data(input_df)

    # Split data into train and test sets
    train_df = grouped_df.loc[grouped_df['test_indicator'] == 0]
    test_df = grouped_df.loc[grouped_df['test_indicator'] == 1]

    # Read holiday data
    holiday_df = read_holiday_data('prophet_holidays')

    # Fit and predict using Prophet model
    p_fitted, p_forecast = fit_and_predict_prophet(train_df, test_df, holiday_df)

    # Fit and predict using Orbit model
    o_fitted, o_forecast = fit_and_predict_orbit(train_df, test_df)

    # Calculate weights for the weighted average ensemble
    weights = weighted_average_weights(train_df, p_fitted, o_fitted)

    # Weighted Average Ensemble
    ensemble_forecast = pd.DataFrame()
    ensemble_forecast['ds'] = test_df['ds']
    ensemble_forecast['prophet_forecast'] = p_forecast['yhat'].values
    ensemble_forecast['orbit_forecast'] = o_forecast['prediction'].values
    ensemble_forecast['ensemble_forecast'] = (p_forecast['yhat'].values * weights[0]) + (o_forecast['prediction'].values * weights[1])

    # Simple Averaging Ensemble
    # ensemble_forecast = weighted_averaging_ensemble(test_df, p_forecast, o_forecast, 0.7)
    ensemble_forecast.to_csv('WA_LR_14_03_2024.csv')

    # Calculate metrics
    calculate_metrics(test_df, ensemble_forecast)

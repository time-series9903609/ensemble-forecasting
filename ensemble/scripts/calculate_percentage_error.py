import numpy as np 
import fileUtils
import percentage_error



if __name__ == "__main__":


    # dataframe = fileUtils.read_csv('prophet_forecast')
    # # Apply percentage error calculation to the entire dataframe
    # dataframe['Percentage_Error'] = dataframe.apply(lambda row: percentage_error.calculate_percentage_error(row,'actual_sales','yhat'), axis=1)
    # print(dataframe.head(5))
    # dataframe.to_csv('prophet_forecast_error.csv',index=False)

    # dataframe1 = fileUtils.read_csv('orbit_forecast')
    # # Apply percentage error calculation to the entire dataframe
    # dataframe1['Percentage_Error'] = dataframe1.apply(lambda row: percentage_error.calculate_percentage_error(row,'actual_sales','prediction'), axis=1)
    # print(dataframe1.head(5))
    # dataframe1.to_csv('orbit_forecast_error.csv',index=False)

    dataframe2 = fileUtils.read_csv('XGBoost_Boosting_forecast')
    # Apply percentage error calculation to the entire dataframe
    dataframe2['Percentage_Error'] = dataframe2.apply(lambda row: percentage_error.calculate_percentage_error(row,'actual_sales','ensemble_forecast'), axis=1)
    dataframe2.round(2)
    print(dataframe2.head(5))
    dataframe2.to_csv('XGBoost_Boosting_forecast_error.csv',index=False)


    # dataframe3 = fileUtils.read_csv('XGBoost_Boosting_forecast')
    # # Apply percentage error calculation to the entire dataframe
    # dataframe3['Percentage_Error'] = dataframe3.apply(lambda row: percentage_error.calculate_percentage_error(row,'actual_sales','ensemble_forecast'), axis=1)
    # print(dataframe3.head(5))
    # dataframe3.to_csv('XGBoost_Boosting_forecast_error.csv',index=False)

    # dataframe3 = fileUtils.read_csv('XGBoost_Stacking_Forecast')
    # # Apply percentage error calculation to the entire dataframe
    # dataframe3['Percentage_Error'] = dataframe3.apply(lambda row: percentage_error.calculate_percentage_error(row,'actual_sales','ensemble_forecast'), axis=1)
    # print(dataframe3.head(5))
    # dataframe3.to_csv('XGBoost_Stacking_Forecast_error.csv',index=False)

    







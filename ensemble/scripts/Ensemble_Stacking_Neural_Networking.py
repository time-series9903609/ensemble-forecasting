import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from prophet import Prophet
from orbit.models import DLT
import fileUtils
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from sklearn.preprocessing import StandardScaler


def read_input_data(file_name):
    input_df = fileUtils.read_csv(file_name)
    print(input_df.shape)
    print(input_df.head(5))
    return input_df


def preprocess_input_data(input_df):
    input_df = fileUtils.find_update_missing_dates(input_df)
    fileUtils.verify_dates_completeness(input_df)
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')
    input_df = input_df.sort_values(by='date')
    input_df.rename(columns={'date': 'ds', 'sales': 'y'}, inplace=True)
    return input_df


def group_input_data(input_df):
    grouped_df = input_df.groupby(['store_nbr', 'ds']).agg({'y': 'sum',
                                                            'transactions': 'sum',
                                                            'onpromotion': 'sum'}).reset_index()
    grouped_df['test_indicator'] = 0
    grouped_df.loc[grouped_df['ds'].dt.year >= 2017, 'test_indicator'] = 1
    return grouped_df


def read_holiday_data(file_name):
    holiday_df = fileUtils.read_csv(file_name)
    holiday_df = holiday_df.drop(holiday_df.columns[holiday_df.columns.str.contains('Unnamed', case=False)], axis=1)
    return holiday_df


def fit_and_predict_prophet(train_df, test_df, holiday_df):
    model = Prophet(interval_width=0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    changepoint_range=0.95,
                    holidays=holiday_df,
                    holidays_mode='additive',
                    seasonality_mode='additive')
    model.add_regressor('onpromotion')
    model.add_regressor('transactions')
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast[['ds', 'yhat']]


def fit_and_predict_orbit(train_df, test_df):
    column_names = ['onpromotion', 'transactions']
    model = DLT(response_col='y',
                date_col='ds',
                seasonality=365,
                global_trend_option='linear',
                damped_factor=0.4,
                regressor_col=column_names)
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast[['ds', 'prediction']]


def stacking_ensemble(train_df, test_df, holiday_df):
    # Fit base models and get predictions
    p_forecast_train = fit_and_predict_prophet(train_df, test_df, holiday_df)['yhat']  # Selecting only 'yhat' column
    o_forecast_train = fit_and_predict_orbit(train_df, test_df)['prediction']  # Selecting only 'prediction' column

    # Reshape predictions to have two dimensions
    p_forecast_train = p_forecast_train.values.reshape(-1, 1)
    o_forecast_train = o_forecast_train.values.reshape(-1, 1)

    # Neural Network Meta-Model
    # Scale the predictions
    scaler = StandardScaler()
    # Concatenate predictions before scaling
    combined_forecast_train = np.concatenate([p_forecast_train, o_forecast_train], axis=1)
    combined_forecast_scaled = scaler.fit_transform(combined_forecast_train)

    # Define and train the neural network
    model = Sequential([
        Dense(64, activation='relu', input_shape=(2,)),
        Dense(32, activation='relu'),
        Dense(1)
    ])
    model.compile(optimizer='adam', loss='mean_squared_error')
    model.fit(combined_forecast_scaled, test_df['y'], epochs=50, verbose=0)  # Use train_df here

    # Fit base models and get predictions for test data
    p_forecast_test = fit_and_predict_prophet(train_df, test_df, holiday_df)['yhat']  # Selecting only 'yhat' column
    o_forecast_test = fit_and_predict_orbit(train_df, test_df)['prediction']  # Selecting only 'prediction' column

    # Reshape predictions for test data to have two dimensions
    p_forecast_test = p_forecast_test.values.reshape(-1, 1)
    o_forecast_test = o_forecast_test.values.reshape(-1, 1)

    # Concatenate predictions for test data before scaling
    combined_forecast_test = np.concatenate([p_forecast_test, o_forecast_test], axis=1)

    # Scale the predictions for test data using the same scaler instance
    combined_forecast_test_scaled = scaler.transform(combined_forecast_test)

    # Combine predictions using the neural network
    stacked_forecast_scaled = model.predict(combined_forecast_test_scaled)

    # Inverse scaling for the stacked forecast
    stacked_forecast = scaler.inverse_transform(stacked_forecast_scaled)

    return stacked_forecast.flatten()




def calculate_metrics(test_df, ensemble_forecast):
    lr_mse = mean_squared_error(test_df['y'].astype(float), ensemble_forecast)
    lr_mae = mean_absolute_error(test_df['y'].astype(float), ensemble_forecast)
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(ensemble_forecast)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': ['Stacking Ensemble with Neural Network'],
                               'RMSLE': [lr_rmsle],
                               'RMSE': [np.sqrt(lr_mse)],
                               'MSE': [lr_mse],
                               'MAE': [lr_mae]}).round(2)
    print(results_lr)


if __name__ == "__main__":
    # Read input data
    input_df = read_input_data('grocery_one_series')

    # Preprocess input data
    input_df = preprocess_input_data(input_df)

    # Group input data
    grouped_df = group_input_data(input_df)

    # Split data into train and test sets
    train_df = grouped_df.loc[grouped_df['test_indicator'] == 0]
    test_df = grouped_df.loc[grouped_df['test_indicator'] == 1]

    # Read holiday data
    holiday_df = read_holiday_data('prophet_holidays')

    # Stacking Ensemble with Neural Network
    ensemble_forecast = stacking_ensemble(train_df, test_df, holiday_df)

    # Calculate metrics
    calculate_metrics(test_df, ensemble_forecast)

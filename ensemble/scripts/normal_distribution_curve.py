import numpy as np
import fileUtils
import matplotlib.pyplot as plt
from scipy.stats import norm

def plot_histogram_with_normal_fit(dataframe, label, color):
    # Create a histogram with bins
    plt.hist(dataframe['Percentage_Error'], bins=30, density=True, alpha=0.6, color=color, label=label)

    # Fit a normal distribution curve
    mu, std = norm.fit(dataframe['Percentage_Error'])
    xmin, xmax = plt.xlim()
    x = np.linspace(xmin, xmax, 100)
    p = norm.pdf(x, mu, std)

    # Plot the normal distribution curve
    plt.plot(x, p, 'k', linewidth=2)

if __name__ == "__main__":
    prophet_dataframe = fileUtils.read_csv('prophet_forecast_error')
    orbit_dataframe = fileUtils.read_csv('orbit_forecast_error')
    boosting_dataframe = fileUtils.read_csv('XGBoost_Boosting_forecast_error')

    # Plot histogram with normal distribution fit for each dataset
    plot_histogram_with_normal_fit(prophet_dataframe, 'Prophet', 'g')
    # Add labels, legend, and title
    plt.xlabel('Value')
    plt.ylabel('Frequency')
    plt.title('Histogram with Normal Distribution Fit (Prophet)')
    plt.legend()

    # Show plot
    plt.show()


    plot_histogram_with_normal_fit(orbit_dataframe, 'Orbit', 'y')
    # Add labels, legend, and title
    plt.xlabel('Value')
    plt.ylabel('Frequency')
    plt.title('Histogram with Normal Distribution Fit (Orbit)')
    plt.legend()

    # Show plot
    plt.show()

    plot_histogram_with_normal_fit(boosting_dataframe, 'Boosting', 'b')
    # Add labels, legend, and title
    plt.xlabel('Value')
    plt.ylabel('Frequency')
    plt.title('Histogram with Normal Distribution Fit (Boosting)')
    plt.legend()

    # Show plot
    plt.show()

    # Plot histogram with normal distribution fit for each dataset
    plot_histogram_with_normal_fit(prophet_dataframe, 'Prophet', 'g')
    plot_histogram_with_normal_fit(orbit_dataframe, 'Orbit', 'y')
    plot_histogram_with_normal_fit(boosting_dataframe, 'Boosting', 'b')
    # Add labels, legend, and title
    plt.xlabel('Value')
    plt.ylabel('Frequency')
    plt.title('Histogram with Normal Distribution Fit (Prophet, Orbit, Boosting)')
    plt.legend()

    # Show plot
    plt.show()

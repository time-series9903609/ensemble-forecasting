import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from prophet import Prophet
from orbit.models import DLT
import fileUtils
# import xgboost as xgb
import matplotlib.pyplot as plt
from sklearn.svm import SVR
from sklearn.ensemble import BaggingRegressor

# RMSE number is 799 which is far worst than individual forecasts. It is better to drop this model.

def read_input_data(file_name):
    input_df = fileUtils.read_csv(file_name)
    print(input_df.shape)
    print(input_df.head(5))
    print("forecast NaN values count :", input_df.isna().sum())
    return input_df

def preprocess_input_data(input_df):
    input_df = fileUtils.find_update_missing_dates(input_df)
    input_df = input_df.fillna(method='ffill')
    fileUtils.verify_dates_completeness(input_df)
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')
    input_df = input_df.sort_values(by='date')
    input_df.rename(columns={'date': 'ds', 'sales': 'y'}, inplace=True)
    return input_df

def group_input_data(input_df):
    grouped_df = input_df.groupby(['store_nbr', 'ds']).agg({'y': 'sum',
                                                            'transactions': 'sum',
                                                            'onpromotion': 'sum'}).reset_index()
    grouped_df['test_indicator'] = 0
    grouped_df.loc[grouped_df['ds'].dt.year >= 2017, 'test_indicator'] = 1
    return grouped_df

def read_holiday_data(file_name):
    holiday_df = fileUtils.read_csv(file_name)
    holiday_df = holiday_df.drop(holiday_df.columns[holiday_df.columns.str.contains('Unnamed', case=False)], axis=1)
    return holiday_df

def fit_and_predict_prophet(train_df, test_df, holiday_df):
    model = Prophet(interval_width=0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    changepoint_range=0.95,
                    holidays=holiday_df,
                    holidays_mode='additive',
                    seasonality_mode='additive')
    model.add_regressor('onpromotion')
    model.add_regressor('transactions')
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast

def fit_and_predict_orbit(train_df, test_df):
    column_names = ['onpromotion', 'transactions']
    model = DLT(response_col='y',
                date_col='ds',
                seasonality=365,
                global_trend_option='linear',
                damped_factor=0.4,
                regressor_col=column_names)
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast

def bagging_ensemble_with_svm(test_df, p_forecast, o_forecast, random_seed=42, n_estimators=10):
    np.random.seed(random_seed)

    # Train SVM regressors for Prophet forecast
    svm_prophet_models = []
    for _ in range(n_estimators):
        indices = np.random.choice(len(p_forecast), len(p_forecast), replace=True)
        svm_prophet = SVR()
        svm_prophet.fit(p_forecast.iloc[indices][['yhat', 'yhat_lower', 'yhat_upper']], test_df.iloc[indices]['y'])
        svm_prophet_models.append(svm_prophet)

    # Train SVM regressors for Orbit forecast
    svm_orbit_models = []
    for _ in range(n_estimators):
        indices = np.random.choice(len(o_forecast), len(o_forecast), replace=True)
        svm_orbit = SVR()
        svm_orbit.fit(o_forecast.iloc[indices][['prediction', 'prediction_5', 'prediction_95']], test_df.iloc[indices]['y'])
        svm_orbit_models.append(svm_orbit)

    # Predict using SVM models
    p_predictions = np.mean([model.predict(p_forecast[['yhat', 'yhat_lower', 'yhat_upper']]) for model in svm_prophet_models], axis=0)
    o_predictions = np.mean([model.predict(o_forecast[['prediction', 'prediction_5', 'prediction_95']]) for model in svm_orbit_models], axis=0)

    # Combine predictions using weighted average
    ensemble_forecast = (p_predictions + o_predictions) / 2

    ensemble_forecast = pd.DataFrame(ensemble_forecast, columns=['ensemble_forecast'])
    ensemble_forecast['ds'] = test_df['ds'].values
    ensemble_forecast = ensemble_forecast[['ds', 'ensemble_forecast']]
    ensemble_forecast['actual_sales'] = test_df['y'].values
    ensemble_forecast = ensemble_forecast[['ds', 'actual_sales', 'ensemble_forecast']]
    ensemble_forecast['prophet_prediction'] = p_forecast['yhat'].values
    ensemble_forecast['orbit_prediction'] = o_forecast['prediction'].values
    ensemble_forecast = ensemble_forecast.round(2)
    return ensemble_forecast

def calculate_metrics(test_df, ensemble_forecast):
    lr_mse = mean_squared_error(test_df['y'], ensemble_forecast['ensemble_forecast'])
    lr_rmsle = np.sqrt(mean_squared_log_error(test_df['y'], ensemble_forecast['ensemble_forecast']))
    results_lr = pd.DataFrame({'Model': ['Boosting Ensemble with XGBoost'],
                               'RMSLE': [lr_rmsle],
                               'RMSE': [np.sqrt(lr_mse)]}).round(2)
    print(results_lr)

if __name__ == "__main__":
    # Read input data
    input_df = read_input_data('grocery_one_series')

    # Preprocess input data
    input_df = preprocess_input_data(input_df)

    # Group input data
    grouped_df = group_input_data(input_df)

    # Split data into train and test sets
    train_df = grouped_df.loc[grouped_df['test_indicator'] == 0]
    test_df = grouped_df.loc[grouped_df['test_indicator'] == 1]

    # Read holiday data
    holiday_df = read_holiday_data('prophet_holidays')

    # Fit and predict using Prophet model
    p_forecast = fit_and_predict_prophet(train_df, test_df, holiday_df)

    # Fit and predict using Orbit model
    o_forecast = fit_and_predict_orbit(train_df, test_df)

    # Boosting Ensemble with XGBoost
    ensemble_forecast = bagging_ensemble_with_svm(test_df, p_forecast, o_forecast, random_seed=42)

    ensemble_forecast.to_csv('SVM_Bagging_forecast.csv')

    # Calculate metrics
    calculate_metrics(test_df, ensemble_forecast)

    plt.figure(figsize=(10, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual', color='red')
    plt.plot(test_df['ds'], ensemble_forecast['ensemble_forecast'], label='Ensemble Forecast', color='blue')
    plt.plot(p_forecast['ds'], p_forecast['yhat'], label='Prophet', color='yellow')
    plt.plot(o_forecast['ds'], o_forecast['prediction'], label='Orbit', color='green')
    plt.title('Bagging Ensemble (SVM) - Actual vs. Forecast Data')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()

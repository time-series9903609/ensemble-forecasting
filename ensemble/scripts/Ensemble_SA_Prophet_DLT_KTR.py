import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from prophet import Prophet
from orbit.models import DLT, KTR  # Import KTR model
import fileUtils
import matplotlib.pyplot as plt


def read_input_data(file_name):
    input_df = fileUtils.read_csv(file_name)
    print(input_df.shape)
    print(input_df.head(5))
    return input_df


def preprocess_input_data(input_df):
    input_df = fileUtils.find_update_missing_dates(input_df)
    fileUtils.verify_dates_completeness(input_df)
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')
    input_df = input_df.sort_values(by='date')
    input_df.rename(columns={'date': 'ds', 'sales': 'y'}, inplace=True)
    return input_df


def group_input_data(input_df):
    grouped_df = input_df.groupby(['store_nbr', 'ds']).agg({'y': 'sum',
                                                            'transactions': 'sum',
                                                            'onpromotion': 'sum'}).reset_index()
    grouped_df['test_indicator'] = 0
    grouped_df.loc[grouped_df['ds'].dt.year >= 2017, 'test_indicator'] = 1
    return grouped_df


def read_holiday_data(file_name):
    holiday_df = fileUtils.read_csv(file_name)
    holiday_df = holiday_df.drop(holiday_df.columns[holiday_df.columns.str.contains('Unnamed', case=False)],
                                 axis=1)
    return holiday_df


def fit_and_predict_prophet(train_df, test_df, holiday_df):
    model = Prophet(interval_width=0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    changepoint_range=0.95,
                    holidays=holiday_df,
                    holidays_mode='additive',
                    seasonality_mode='additive')
    model.add_regressor('onpromotion')
    model.add_regressor('transactions')
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast


def fit_and_predict_orbit(train_df, test_df):
    column_names = ['onpromotion', 'transactions']
    model = DLT(response_col='y',
                date_col='ds',
                seasonality=365,
                estimator='stan-map',
                global_trend_option='linear',
                damped_factor=0.4,
                regressor_col=column_names)
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast


def fit_and_predict_ktr(train_df, test_df, column_names):
    model = KTR(response_col='y',
                date_col='ds',
                estimator='pyro-svi',
                seed=2000,
                seasonality=[182.625, 365.25],
                regressor_col=column_names,
                prediction_percentiles=[5, 95],
                regressor_sign=['+', '+'])
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast


def simple_averaging_ensemble(test_df, p_forecast, o_forecast, k_forecast):
    ensemble_forecast = pd.DataFrame()
    ensemble_forecast['ds'] = test_df['ds']
    ensemble_forecast['prophet_forecast'] = p_forecast['yhat'].values
    ensemble_forecast['dlt_forecast'] = o_forecast['prediction'].values
    ensemble_forecast['ktr_forecast'] = k_forecast['prediction'].values
    ensemble_forecast['ensemble_forecast'] = (ensemble_forecast['prophet_forecast'] + ensemble_forecast['dlt_forecast']+ensemble_forecast['ktr_forecast']) / 3
    return ensemble_forecast


def calculate_metrics(test_df, ensemble_forecast):
    lr_mse = mean_squared_error(test_df['y'].astype(float), ensemble_forecast['ensemble_forecast'])
    lr_mae = mean_absolute_error(test_df['y'].astype(float), ensemble_forecast['ensemble_forecast'])
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(ensemble_forecast['ensemble_forecast'])
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': ['Simple Average Model'],
                               'RMSLE': [lr_rmsle],
                               'RMSE': [np.sqrt(lr_mse)],
                               'MSE': [lr_mse],
                               'MAE': [lr_mae]}).round(2)
    print(results_lr)


if __name__ == "__main__":
    # Read input data
    input_df = read_input_data('grocery_one_series')

    # Preprocess input data
    input_df = preprocess_input_data(input_df)

    # Group input data
    grouped_df = group_input_data(input_df)

    # Split data into train and test sets
    train_df = grouped_df.loc[grouped_df['test_indicator'] == 0]
    test_df = grouped_df.loc[grouped_df['test_indicator'] == 1]

    # Read holiday data
    holiday_df = read_holiday_data('prophet_holidays')

    # Fit and predict using Prophet model
    p_forecast = fit_and_predict_prophet(train_df, test_df, holiday_df)

    # Fit and predict using Orbit model
    o_forecast = fit_and_predict_orbit(train_df, test_df)

    # Fit and predict using KTR model
    column_names = ['onpromotion', 'transactions']
    ktr_forecast = fit_and_predict_ktr(train_df, test_df, column_names)

    # Simple Averaging Ensemble
    ensemble_forecast = simple_averaging_ensemble(test_df, p_forecast, o_forecast, ktr_forecast)

    print(ensemble_forecast.head(12))
    # Calculate metrics
    calculate_metrics(test_df, ensemble_forecast)

    plt.figure(figsize=(10, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual', color='blue')
    plt.plot(ensemble_forecast['ds'], ensemble_forecast['ensemble_forecast'], label='Forecast', color='red')
    # plt.fill_between(forecast['date'], forecast['prediction_5'], forecast['prediction_95'], color='gray', alpha=0.3)
    plt.title('Actual vs. Forecast Data')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()

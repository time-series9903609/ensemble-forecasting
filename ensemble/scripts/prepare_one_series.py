import pandas as pd 
import numpy as np 
# from download import download
import fileUtils
import matplotlib.pyplot as plt
import seaborn as sns
import os
from sklearn.preprocessing import MinMaxScaler
from statsmodels.tsa.seasonal import seasonal_decompose

if __name__=="__main__":
    
   train_df = fileUtils.read_csv('grocery')
   print(train_df.shape)
   print(train_df.columns)
   print(train_df.isna().sum())

   train_df = train_df[train_df.store_nbr==1]
   print(train_df.shape)
   print(train_df.columns)
   print(train_df.isna().sum())

   current_dir = os.path.dirname(os.path.abspath(__file__))
   data_folder = os.path.join(current_dir, '../data')
   filename_with_extension = 'grocery_one_series' + '.csv'
   file_path = os.path.join(data_folder, filename_with_extension)
   train_df.to_csv(file_path)









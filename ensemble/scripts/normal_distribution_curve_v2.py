import numpy as np
import fileUtils
import matplotlib.pyplot as plt
from scipy.stats import norm

def plot_histogram_with_normal_fit(dataframe, label, color):
    # Calculate the percentage of yhat relative to actual_sales
    dataframe = dataframe.round(2)
    percentage = (dataframe['yhat'] / dataframe['actual_sales']) * 100

    # Create a histogram with bins
    plt.hist(percentage, bins=30, density=True, alpha=0.6, color=color, label=label)

    # Fit a normal distribution curve
    mu, std = norm.fit(percentage)
    xmin, xmax = plt.xlim()
    x = np.linspace(xmin, xmax, 100)
    p = norm.pdf(x, mu, std)

    # Plot the normal distribution curve
    plt.plot(x, p, 'k', linewidth=2)

if __name__ == "__main__":
    dataframe = fileUtils.read_csv('prophet_forecast_error')

    # Plot histogram with normal distribution fit
    plot_histogram_with_normal_fit(dataframe, 'Data', 'g')

    # Add labels, legend, and title
    plt.xlabel('% of Actual Sales')
    plt.ylabel('Frequency')
    plt.title('Histogram with Normal Distribution Fit')
    plt.legend()

    # Show plot
    plt.show()

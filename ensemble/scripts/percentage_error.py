import pandas as pd



# Calculate percentage error
def calculate_percentage_error(row):
    actual = row['actual_sales']
    forecast = row['yhat']
    if actual == 0:
        return 100.0
    else:
        return ((forecast - actual) / actual) * 100
    
# Calculate percentage error
def calculate_percentage_error(row, actual, forecast):
    actual = row[f'{actual}']
    forecast = row[f'{forecast}']
    if actual == 0:
        return 100.0
    else:
        return ((forecast - actual) / actual) * 100

# # Apply percentage error calculation to the entire dataframe
# df['Percentage_Error'] = calculate_percentage_error(df['Actual'], df['Forecast'])

# # Write back to the same file
# df.to_csv('your_file.csv', index=False)


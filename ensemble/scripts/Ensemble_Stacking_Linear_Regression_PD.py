import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from prophet import Prophet
from orbit.models import DLT
import fileUtils
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt


def read_input_data(file_name):
    input_df = fileUtils.read_csv(file_name)
    print(input_df.shape)
    print(input_df.head(5))
    print("forecast NaN values count :", input_df.isna().sum())
    return input_df


def preprocess_input_data(input_df):
    input_df = fileUtils.find_update_missing_dates(input_df)
    input_df = input_df.fillna(method='ffill')
    fileUtils.verify_dates_completeness(input_df)
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')
    input_df = input_df.sort_values(by='date')
    input_df.rename(columns={'date': 'ds', 'sales': 'y'}, inplace=True)
    return input_df


def group_input_data(input_df):
    grouped_df = input_df.groupby(['store_nbr', 'ds']).agg({'y': 'sum',
                                                            'transactions': 'sum',
                                                            'onpromotion': 'sum'}).reset_index()
    grouped_df['test_indicator'] = 0
    grouped_df.loc[grouped_df['ds'].dt.year >= 2017, 'test_indicator'] = 1
    return grouped_df


def read_holiday_data(file_name):
    holiday_df = fileUtils.read_csv(file_name)
    holiday_df = holiday_df.drop(holiday_df.columns[holiday_df.columns.str.contains('Unnamed', case=False)], axis=1)
    return holiday_df


def fit_and_predict_prophet(train_df, test_df, holiday_df):
    model = Prophet(interval_width=0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    changepoint_range=0.95,
                    holidays=holiday_df,
                    holidays_mode='additive',
                    seasonality_mode='additive')
    model.add_regressor('onpromotion')
    model.add_regressor('transactions')
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast


def fit_and_predict_orbit(train_df, test_df):
    column_names = ['onpromotion', 'transactions']
    model = DLT(response_col='y',
                date_col='ds',
                seasonality=365,
                global_trend_option='linear',
                damped_factor=0.4,
                regressor_col=column_names)
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast


def stacking_ensemble(test_df, p_forecast, o_forecast):
    # Merge forecasts with training data on 'ds'
    train_with_forecasts = pd.merge(test_df, p_forecast, on='ds', how='left')
    train_with_forecasts = pd.merge(train_with_forecasts, o_forecast, on='ds', how='left')


    # Prepare input features for the meta-model
    X_train = train_with_forecasts[['yhat', 'prediction']]
    y_train = train_with_forecasts['y']

    # Train the linear regression model on the training data
    lr_model = LinearRegression()
    lr_model.fit(X_train, y_train)

    # Merge forecasts with test data on 'ds'
    test_with_forecasts = pd.merge(test_df, p_forecast, on='ds', how='left')
    test_with_forecasts = pd.merge(test_with_forecasts, o_forecast, on='ds', how='left')

    # Prepare input features for the test data
    X_test = test_with_forecasts[['yhat', 'prediction']]

    # Predict using the linear regression model
    ensemble_forecast = lr_model.predict(X_test)
    ensemble_forecast = pd.DataFrame(ensemble_forecast, columns=['ensemble_forecast'])
    ensemble_forecast['ds'] = test_df['ds'].values
    ensemble_forecast = ensemble_forecast[['ds','ensemble_forecast']]
    ensemble_forecast['actual_sales'] = test_df['y'].values
    ensemble_forecast = ensemble_forecast[['ds','actual_sales','ensemble_forecast']]
    ensemble_forecast[['prophet_prediction', 'orbit_prediction']] = train_with_forecasts[['yhat', 'prediction']].values
    print(ensemble_forecast.columns)

    return ensemble_forecast


def calculate_metrics(test_df, ensemble_forecast):
    lr_mse = mean_squared_error(test_df['y'], ensemble_forecast['ensemble_forecast'])
    # lr_mae = mean_absolute_error(test_df['y'], ensemble_forecast['ensemble_forecast'])
    lr_rmsle = np.sqrt(mean_squared_log_error(test_df['y'], ensemble_forecast['ensemble_forecast']))
    results_lr = pd.DataFrame({'Model': ['Stacking Ensemble (PD) with Linear Regression'],
                               'RMSLE': [lr_rmsle],
                               'RMSE': [np.sqrt(lr_mse)]}).round(2)
    print(results_lr)



if __name__ == "__main__":
    # Read input data
    input_df = read_input_data('grocery_one_series')

    # Preprocess input data
    input_df = preprocess_input_data(input_df)

    # Group input data
    grouped_df = group_input_data(input_df)

    # Split data into train and test sets
    train_df = grouped_df.loc[grouped_df['test_indicator'] == 0]
    test_df = grouped_df.loc[grouped_df['test_indicator'] == 1]


    # Read holiday data
    holiday_df = read_holiday_data('prophet_holidays')

    p_forecast = fit_and_predict_prophet(train_df, test_df, holiday_df)

    o_forecast = fit_and_predict_orbit(train_df, test_df)


    # Stacking Ensemble with Linear Regression
    ensemble_forecast = stacking_ensemble(test_df, p_forecast,o_forecast)

    print(ensemble_forecast.head(12))
    ensemble_forecast.to_csv('Linear_Regression_ensemble.csv')

    # Calculate metrics
    calculate_metrics(test_df, ensemble_forecast)

    plt.figure(figsize=(10, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual', color='red')
    plt.plot(ensemble_forecast['ds'], ensemble_forecast['ensemble_forecast'], label='Forecast', color='blue')
    plt.plot(ensemble_forecast['ds'], p_forecast['yhat'], label='Prophet', color='yellow')
    plt.plot(ensemble_forecast['ds'], o_forecast['prediction'], label='Orbit', color='green')
    # plt.fill_between(forecast['date'], forecast['prediction_5'], forecast['prediction_95'], color='gray', alpha=0.3)
    plt.title('Stacking(Linear Regression) - Actual vs. Forecast Data')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()

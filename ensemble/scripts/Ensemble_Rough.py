import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from prophet import Prophet
import orbit
from orbit.models import DLT 
import fileUtils 
import matplotlib.pyplot as plt  


if __name__ == "__main__":
    # Read input data
    input_df = fileUtils.read_csv('grocery_one_series')
    print(input_df.shape)
    print(input_df.head(5))

    # Preprocess input data
    input_df = fileUtils.find_update_missing_dates(input_df)
    fileUtils.verify_dates_completeness(input_df)
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')
    input_df = input_df.sort_values(by='date')
    input_df.rename(columns={'date':'ds','sales':'y'}, inplace=True)    

    # Group input data
    grouped_df = input_df.groupby(['store_nbr', 'ds']).agg({'y':'sum', 
                                                            'transactions':'sum', 
                                                            'onpromotion':'sum'}).reset_index()
    grouped_df['test_indicator'] = 0
    grouped_df.loc[grouped_df['ds'].dt.year >= 2017, 'test_indicator'] = 1

    # Split data into train and test sets
    train_df = grouped_df.loc[grouped_df['test_indicator'] == 0]
    test_df = grouped_df.loc[grouped_df['test_indicator'] == 1]

    # Read holiday data
    holiday_df = fileUtils.read_csv('prophet_holidays')
    print(holiday_df.columns)
    holiday_df = holiday_df.drop(holiday_df.columns[holiday_df.columns.str.contains('Unnamed',case=False)], axis=1)
    print(holiday_df.columns)

    # Fit and predict using Prophet model
    model_fitting_time = datetime.now()
    print("Model Fit starting time :", model_fitting_time)
    p_model = Prophet(interval_width = 0.95,
                      growth='linear',
                      yearly_seasonality=True,
                      changepoint_range=0.95,
                      holidays=holiday_df,
                      holidays_mode='additive',
                      seasonality_mode='additive')
    p_model.add_regressor('onpromotion')
    p_model.add_regressor('transactions')
    p_model.fit(train_df)
    model_fitting_timef = datetime.now()
    print("Model Fitting Ending time :", model_fitting_timef)
    print("Total Fitting time :",model_fitting_timef-model_fitting_time )
    print("Model Predict Start time :", model_fitting_timef)
    p_forecast = p_model.predict(test_df)
    model_predict_timef = datetime.now()
    print("Model Predict Ending time :", model_predict_timef)
    print("Total Predicting time :",model_predict_timef-model_predict_timef )
    print(p_forecast.head())

    # Fit and predict using Orbit model
    column_names = ['onpromotion', 'transactions']
    model_time = datetime.now()
    print("Model time:", model_time)
    o_model = DLT(response_col='y', 
                  date_col='ds', 
                  seasonality=365,
                  global_trend_option='linear', 
                  damped_factor=0.4, 
                  regressor_col=column_names)
    o_model.fit(train_df)
    predict_time = datetime.now()
    print("Total Fitting time :", predict_time-model_time)
    print("Predict start time :", predict_time)
    o_forecast = o_model.predict(test_df)
    predict_complete_time = datetime.now()
    print("Total Predict time :", predict_complete_time-predict_time)
    print(o_forecast.head(5))
    print(o_forecast.tail(5))
    
    # Simple Averaging Ensemble
    test_df.reset_index(inplace=True)
    p_forecast.reset_index(inplace=True)
    o_forecast.reset_index(inplace=True)
    print("test dataframe shape",test_df.shape)
    print("p forecast dataframe shape",p_forecast.shape)
    print("o forecast dataframe shape",o_forecast.shape)
    ensemble_forecast = pd.DataFrame()
    ensemble_forecast['ds'] = test_df['ds']  
    ensemble_forecast['prophet_forecast'] = p_forecast['yhat']
    ensemble_forecast['orbit_forecast'] = o_forecast['prediction']

    # Average the forecasts from both models
    ensemble_forecast['ensemble_forecast'] = (p_forecast['yhat'] + o_forecast['prediction']) / 2

    # Print or visualize the ensemble forecast
    print(ensemble_forecast)

    # Calculate metrics
    lr_mse = mean_squared_error(test_df['y'].astype(float), ensemble_forecast['ensemble_forecast'])
    lr_mae = mean_absolute_error(test_df['y'].astype(float), ensemble_forecast['ensemble_forecast'])

    # Apply the absolute value function to both y_eval and lr_predictions
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(ensemble_forecast['ensemble_forecast'])

    # Calculate the Root Mean Squared Logarithmic Error (RMSLE)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))

    # Create a DataFrame to store results for Linear Regression
    results_lr = pd.DataFrame({'Model': ['Simple Average Model'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)],
                            'MSE': [lr_mse],
                            'MAE': [lr_mae]}).round(2)

    # Print the results_lr dataframe
    print(results_lr)



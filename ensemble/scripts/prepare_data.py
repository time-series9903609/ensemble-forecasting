import pandas as pd 
import numpy as np 
# from download import download
import fileUtils
import matplotlib.pyplot as plt
import seaborn as sns
import os
from sklearn.preprocessing import MinMaxScaler
from statsmodels.tsa.seasonal import seasonal_decompose

if __name__=="__main__":
    
   train_df = fileUtils.read_csv('train')
   print(train_df.shape)
   print(train_df.columns)
   print(train_df.isna().sum())

   stores_df = fileUtils.read_csv('stores')
   print(stores_df.shape)
   print(stores_df.columns)
   print(stores_df.isna().sum())

   oil_df = fileUtils.read_csv('oil')
   print(oil_df.shape)
   print(oil_df.columns)
   print(oil_df.isna().sum())

   transactions_df = fileUtils.read_csv('transactions')
   print(transactions_df.shape)
   print(transactions_df.columns)
   print(transactions_df.isna().sum())

   holiday_df = fileUtils.read_csv('holidays_events')
   print(holiday_df.shape)
   print(holiday_df.columns)
   print(holiday_df.isna().any())
   print(holiday_df.isna().sum())
   
   train_store_df = pd.merge(train_df, stores_df, on='store_nbr', how='inner')
   train_store_transact_df = pd.merge(train_store_df,transactions_df, on=['date','store_nbr'], how='left')
   merged_df = pd.merge(train_store_transact_df, holiday_df, on='date', how='left')
   print(train_store_df.shape)
   print(train_store_df.columns)
   print(train_store_df.isna().sum())

   train_store_transact_df = pd.merge(train_store_df,transactions_df, on=['date','store_nbr'], how='left')
   print(train_store_transact_df.shape)
   print(train_store_transact_df.columns)
   print(train_store_transact_df.isna().sum())

   median_transactions = train_store_transact_df['transactions'].median()
   train_store_transact_df.fillna({'transactions':median_transactions}, inplace=True)
   print(train_store_transact_df.isna().sum())
   print(train_store_transact_df.shape)

   merged_df = pd.merge(train_store_transact_df, holiday_df, on='date', how='left')
   print(merged_df.shape)
   print(merged_df.columns)
   print(merged_df.isna().sum())

   merged_df = merged_df.drop(['type_y','locale','locale_name','description','transferred'], axis=1)
   print(merged_df.shape)
   print(merged_df.isna().sum())
   merged_df.rename(columns={'type_x':'store_type'}, inplace=True)
   merged_df.to_csv('combined_data.csv')

   merged_df = fileUtils.find_update_missing_dates(merged_df)

   fileUtils.verify_dates_completeness(merged_df)

   print("BEFORE: Number of NaN values in dataframe :",merged_df.isna().sum())
   merged_df.fillna(method='bfill', inplace=True)
   print("AFTER: Number of NaN values in dataframe :",merged_df.isna().sum())

   merged_df['family'] = merged_df['family'].str.replace('/', '_').str.replace(' ', '_').str.lower()
   merged_df['city'] = merged_df['city'].str.replace('/', '_').str.replace(' ', '_').str.lower()
   print("Columns in merged dataset :",merged_df.columns)

   merged_df.to_csv('prepared_data.csv')


   # Plot item sales over the time

   # Convert date column to datetime
   plot_df = merged_df.copy()
   merged_df['date'] = pd.to_datetime(merged_df['date'])

   # Define the category lists for each product category
   food_families = ['beverages', 'bread_bakery', 'frozen_foods', 
                     'meats', 'prepared_foods', 'deli','produce', 
                     'dairy','poultry','eggs','seafood']
   home_families = ['home_and_kitchen_i', 'home_and_kitchen_ii', 'home_appliances']
   clothing_families = ['lingerie', 'ladieswear']
   grocery_families = ['grocery_i', 'grocery_ii']
   stationery_families = ['books', 'magazines','school_and_office_supplies']
   cleaning_families = ['home_care', 'baby_care','personal_care']
   hardware_families = ['players_and_electronics','hardware']

   # Categorize the 'family' column based on the product categories
   plot_df['family'] = np.where(plot_df['family'].isin(food_families), 'foods', plot_df['family'])
   plot_df['family'] = np.where(plot_df['family'].isin(home_families), 'home', plot_df['family'])
   plot_df['family'] = np.where(plot_df['family'].isin(clothing_families), 'clothing', plot_df['family'])
   plot_df['family'] = np.where(plot_df['family'].isin(grocery_families), 'grocery', plot_df['family'])
   plot_df['family'] = np.where(plot_df['family'].isin(stationery_families), 'stationary', plot_df['family'])
   plot_df['family'] = np.where(plot_df['family'].isin(cleaning_families), 'cleaning', plot_df['family'])
   plot_df['family'] = np.where(plot_df['family'].isin(hardware_families), 'hardware', plot_df['family'])
   
   print("Modified family names are :",plot_df.family.unique())
   # Group data by item
   grouped_df = plot_df.groupby(['family', 'date']).agg({'sales':'sum', 
                                                       'onpromotion':'sum',
                                                       'transactions':'sum'})

   # Reset index after grouping
   grouped_df.reset_index(inplace=True)

   # Plot graph -- Entire Dataset
   plt.figure(figsize=(12, 8))

   for family, group in grouped_df.groupby('family'):
    plt.plot(group['date'], group['sales'], label=family)

   plt.title('Sales of Items Over Time')
   plt.xlabel('Date')
   plt.ylabel('Sales')
   plt.legend(title='Item')
   plt.grid(True)
   plt.xticks(rotation=45)
   plt.tight_layout()
   plt.show()

   # Grocery family is considered for analysis to focus on forecasting

   plot2_df = merged_df.copy()
   grocery_df = plot2_df[plot2_df['family'].isin(grocery_families)]
   grocery_df['date'] = pd.to_datetime(grocery_df['date'])
   print("Grocery Family Dataset shape :", grocery_df.shape)
   print("Grocery Family Dataset details :", grocery_df.columns)
   print("Grocery Family Unique values :", grocery_df['family'].unique())

   # Group data by item
   grouped2_df = grocery_df.groupby(['family', 'date']).agg({'sales':'sum', 
                                                       'onpromotion':'sum',
                                                       'transactions':'sum'}).reset_index()
    # Plot graph -- Entire Dataset
   plt.figure(figsize=(12, 8))

   for family, group in grouped2_df.groupby('family'):
    plt.plot(group['date'], group['sales'], label=family)

   plt.title('Sales of Groceries Over Time')
   plt.xlabel('Date')
   plt.ylabel('Sales')
   plt.legend(title='Groceries')
   plt.grid(True)
   plt.show()


   plot3_df = merged_df.copy()
   grocery1_df = plot3_df[plot3_df['family'].isin(['grocery_i'])]
   current_dir = os.path.dirname(os.path.abspath(__file__))
   data_folder = os.path.join(current_dir, '../data')
   filename_with_extension = 'grocery' + '.csv'
   file_path = os.path.join(data_folder, filename_with_extension)
   grocery1_df.to_csv(file_path)
   grocery1_df['date'] = pd.to_datetime(grocery1_df['date'])
   grocery1_df=grocery1_df.reset_index()

   # Ensure that the DataFrame is sorted by date
   grocery1_df = grocery1_df.sort_values('date')

   # Reset index and assign back to grocery1_df
   grocery1_df.reset_index(drop=True, inplace=True)
   print("Grocery Family Dataset shape :", grocery1_df.shape)
   print("Grocery Family Dataset details :", grocery1_df.columns)
   print("Grocery Family Unique values :", grocery1_df['family'].unique())

    # Plot graph -- Entire Dataset
   plt.figure(figsize=(12, 8))

   plt.plot(grocery1_df['date'], grocery1_df['sales'], color='green')
   plt.title('Sales of Grocery_I Over Time')
   plt.xlabel('Date')
   plt.ylabel('Sales')
#    plt.ylim(0, 1000)
   plt.show()

   # Rolling window plot

   # Calculate the rolling mean with different window sizes
   short_window_size = 7  # Short-term window size (e.g., one week)
   long_window_size = 30  # Long-term window size (e.g., one month)

   grocery1_df['rolling_mean_short'] = grocery1_df['sales'].rolling(window=short_window_size).mean()
   grocery1_df['rolling_mean_long'] = grocery1_df['sales'].rolling(window=long_window_size).mean()

   # Plot the data and rolling means
   plt.figure(figsize=(10, 6))
   plt.plot(grocery1_df['date'], grocery1_df['sales'], label='Actual Sales', color='blue', alpha=0.5)
   plt.plot(grocery1_df['date'], grocery1_df['rolling_mean_short'], label=f'Short-term Rolling Mean ({short_window_size} days)', color='red')
   plt.plot(grocery1_df['date'], grocery1_df['rolling_mean_long'], label=f'Long-term Rolling Mean ({long_window_size} days)', color='green')

   plt.title('Rolling Mean Plot for Sales Data')
   plt.xlabel('Date')
   plt.ylabel('Sales')
   plt.legend()
   plt.xticks(rotation=45)
   plt.tight_layout()
   plt.show()

   print("Shape of Grocery dataset :",grocery1_df.shape)
   print("Description of dataset : \n", grocery1_df.describe())
   print("Description of dataset : \n", grocery1_df.info())
   print("Number of Stores in the dataset :",grocery1_df['store_nbr'].count())
   print("Number of Promotions in the dataset :",grocery1_df['onpromotion'].count())
   print("Number of Transactions in the dataset :",grocery1_df['transactions'].count())
   print("Sales Summary in the dataset : \n", grocery1_df['sales'].describe())

   sales_df = grocery1_df.groupby(['date']).agg({'sales':'sum',
                                                 'transactions':'sum',
                                                 'onpromotion':'sum'}).reset_index()

   # Sales distribution
   plt.figure(figsize=(10, 6))
   plt.hist(sales_df['sales'], bins=20, color='skyblue', edgecolor='black', alpha=0.7)
   plt.title('Distribution of Sales')
   plt.xlabel('Sales')
   plt.ylabel('Frequency')
   plt.show()

   plt.figure(figsize=(10, 6))
   sns.kdeplot(sales_df['sales'], fill=True, color='skyblue')
   plt.title('Distribution of Sales (KDE)')
   plt.xlabel('Sales')
   plt.ylabel('Density')
   plt.show()

   plt.figure(figsize=(10, 6))
   sns.kdeplot(sales_df['onpromotion'], fill=True, color='skyblue')
   plt.title('Distribution of On-Promotion (KDE)')
   plt.xlabel('On-Promotion')
   plt.ylabel('Density')
   plt.show()

   plt.figure(figsize=(10, 6))
   sns.kdeplot(sales_df['transactions'], fill=True, color='skyblue')
   plt.title('Distribution of Transactions (KDE)')
   plt.xlabel('Transactions')
   plt.ylabel('Density')
   plt.show()
   

   # Assuming you have a DataFrame named 'data' containing columns 'city', 'store_type', and 'sales'
   grocery1_df = grocery1_df.groupby(['date',
                                      'store_nbr',
                                      'family',
                                      'city',
                                      'store_type']).agg({'sales':'sum',
                                                          'onpromotion':'sum',
                                                          'transactions':'sum'}).reset_index()
   # Sales over different cities
   plt.figure(figsize=(10, 6))
   sns.barplot(x='city', y='sales', data=grocery1_df, errorbar=None)
   plt.title('Sales Over Different Cities')
   plt.xlabel('City')
   plt.ylabel('Sales')
   plt.xticks(rotation=45)
   plt.show()

   # Sales over different store types
   plt.figure(figsize=(10, 6))
   sns.barplot(x='store_type', y='sales', data=grocery1_df, errorbar=None)
   plt.title('Sales Over Different Store Types')
   plt.xlabel('Store Type')
   plt.ylabel('Sales')
   plt.xticks(rotation=45)
   plt.show()

   #Bi-Variate Analysis
   # Plot the time series(Line plot)
   plt.figure(figsize=(10, 6))
   plt.plot(grocery1_df['date'], grocery1_df['sales'], marker='o', linestyle='-')
   plt.title('Sales Over Time')
   plt.xlabel('Date')
   plt.ylabel('Sales')
   plt.tight_layout()
   plt.show()
   
   # Plot the scatter plot
   plt.figure(figsize=(10, 6))
   plt.scatter(grocery1_df['date'], grocery1_df['sales'], marker='o', color='blue')
   plt.title('Scatter Plot of Sales against Dates')
   plt.xlabel('Date')
   plt.ylabel('Sales')
   plt.tight_layout()
   plt.show()

   # Plot the box plot(Store_Number vs Sales)
   plt.figure(figsize=(10, 6))
   sns.boxplot(x='store_nbr', y='sales', data=grocery1_df)
   plt.title('Sales Distribution by Store Number')
   plt.xlabel('Store Number')
   plt.ylabel('Sales')
   plt.tight_layout()
   plt.show()

   # Plot the scatter plot
   plt.figure(figsize=(10, 6))
   plt.scatter(grocery1_df['store_nbr'], grocery1_df['sales'], marker='o', color='blue')
   plt.title('Scatter Plot of Sales against Store Numbers')
   plt.xlabel('Store Number')
   plt.ylabel('Sales')
   plt.tight_layout()
   plt.show()

   # Group data by 'onpromotion' and calculate the mean sales for each group
   sales_by_promotion = grocery1_df.groupby('onpromotion')['sales'].mean().reset_index()

   # Plot the grouped bar plot
   plt.figure(figsize=(8, 6))
   sns.barplot(x='onpromotion', y='sales', data=sales_by_promotion)
   plt.title('Mean Sales by Promotion Status')
   plt.xlabel('Promotion Status')
   plt.ylabel('Mean Sales')
   plt.show()

   # Plot the box plot
   plt.figure(figsize=(8, 6))
   sns.boxplot(x='onpromotion', y='sales', data=grocery1_df)
   plt.title('Sales Distribution by Promotion Status')
   plt.xlabel('Promotion Status')
   plt.ylabel('Sales')
   plt.show()

   # Plot the scatter plot
   plt.figure(figsize=(10, 6))
   sns.scatterplot(x='transactions', y='sales', data=grocery1_df)
   plt.title('Scatter Plot of Sales against Transactions')
   plt.xlabel('Number of Transactions')
   plt.ylabel('Sales')
   plt.tight_layout()
   plt.show()

   # Calculate the correlation matrix
   correlation_matrix = grocery1_df[['sales', 'transactions', 'onpromotion']].corr()

   # Plot the heatmap
   plt.figure(figsize=(8, 6))
   sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt=".2f")
   plt.title('Correlation Between Sales, Transactions, and On Promotion')
   plt.show()

   # Scale the data using MinMaxScaler
   scaler = MinMaxScaler()
   scaled_data = scaler.fit_transform(grocery1_df[['sales', 'transactions', 'onpromotion']])
   scaled_df = pd.DataFrame(scaled_data, columns=['sales', 'transactions', 'onpromotion'])
   plt.plot(grocery1_df['date'],scaled_df['sales'],label='Sales', color='green')
   plt.plot(grocery1_df['date'],scaled_df['transactions'],label='Transactions', color='yellow')
   plt.plot(grocery1_df['date'],scaled_df['onpromotion'],label='On-Promotion', color='blue')
   plt.title('Relationship Between Sales, Transactions, and On-Promotion')
   plt.legend()
   plt.show()

   # Perform seasonal decomposition
   decomposition = seasonal_decompose(grocery1_df['sales'], model='additive', period=365)

   # Plot the decomposition components
   plt.figure(figsize=(10, 8))

   plt.subplot(411)
   plt.plot(grocery1_df['sales'], label='Original')
   plt.legend(loc='upper left')

   plt.subplot(412)
   plt.plot(decomposition.trend, label='Trend')
   plt.legend(loc='upper left')

   plt.subplot(413)
   plt.plot(decomposition.seasonal, label='Seasonality')
   plt.legend(loc='upper left')

   plt.subplot(414)
   plt.plot(decomposition.resid, label='Residuals')
   plt.legend(loc='upper left')

   plt.suptitle('Seasonal Decomposition of Sales Time Series')
   plt.show()









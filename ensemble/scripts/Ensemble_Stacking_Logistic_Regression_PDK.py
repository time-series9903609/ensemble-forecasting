from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
import pandas as pd
import numpy as np
from prophet import Prophet
from orbit.models import DLT, KTR
import fileUtils

# Define the function to read input data
def read_input_data(file_name):
    input_df = fileUtils.read_csv(file_name)
    print(input_df.shape)
    print(input_df.head(5))
    print("forecast NaN values count :", input_df.isna().sum())
    return input_df

# Define the function to preprocess input data
def preprocess_input_data(input_df):
    input_df = fileUtils.find_update_missing_dates(input_df)
    input_df = input_df.fillna(method='ffill')
    fileUtils.verify_dates_completeness(input_df)
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')
    input_df = input_df.sort_values(by='date')
    input_df.rename(columns={'date': 'ds', 'sales': 'y'}, inplace=True)
    return input_df

# Define the function to group input data
def group_input_data(input_df):
    grouped_df = input_df.groupby(['store_nbr', 'ds']).agg({'y': 'sum',
                                                            'transactions': 'sum',
                                                            'onpromotion': 'sum'}).reset_index()
    grouped_df['test_indicator'] = 0
    grouped_df.loc[grouped_df['ds'].dt.year >= 2017, 'test_indicator'] = 1
    return grouped_df

# Define the function to read holiday data
def read_holiday_data(file_name):
    holiday_df = fileUtils.read_csv(file_name)
    holiday_df = holiday_df.drop(holiday_df.columns[holiday_df.columns.str.contains('Unnamed', case=False)], axis=1)
    return holiday_df

# Define the function to fit and predict using Prophet model
def fit_and_predict_prophet(train_df, test_df, holiday_df):
    model = Prophet(interval_width=0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    changepoint_range=0.95,
                    holidays=holiday_df,
                    holidays_mode='additive',
                    seasonality_mode='additive')
    model.add_regressor('onpromotion')
    model.add_regressor('transactions')
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast

# Define the function to fit and predict using Orbit model
def fit_and_predict_orbit(train_df, test_df):
    column_names = ['onpromotion', 'transactions']
    model = DLT(response_col='y',
                date_col='ds',
                seasonality=365,
                global_trend_option='linear',
                damped_factor=0.4,
                regressor_col=column_names)
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast

# Define the function to fit and predict using KTR model
def fit_and_predict_ktr(train_df, test_df, column_names):
    model = KTR(response_col='y',
                date_col='ds',
                estimator='pyro-svi',
                seed=2000,
                seasonality=[182.625, 365.25],
                regressor_col=column_names,
                prediction_percentiles=[5, 95],
                regressor_sign=['+', '+'])
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast

# Define the stacking ensemble function
def stacking_ensemble(test_df, p_forecast, o_forecast, k_forecast):
    # Merge forecasts with training data on 'ds'
    train_with_forecasts = pd.merge(test_df, p_forecast, on='ds', how='left')
    train_with_forecasts = pd.merge(train_with_forecasts, o_forecast, on='ds', how='left')
    train_with_forecasts = pd.merge(train_with_forecasts, k_forecast, on='ds', how='left')

    # Prepare input features for the meta-model
    X_train = train_with_forecasts[['yhat', 'prediction', 'y']]

    # Convert target variable to binary labels
    y_train = (train_with_forecasts['y'] > 0).astype(int)

    # Train the logistic regression model on the training data
    lr_model = LogisticRegression()
    lr_model.fit(X_train, y_train)

    # Merge forecasts with test data on 'ds'
    test_with_forecasts = pd.merge(test_df, p_forecast, on='ds', how='left')
    test_with_forecasts = pd.merge(test_with_forecasts, o_forecast, on='ds', how='left')
    test_with_forecasts = pd.merge(test_with_forecasts, k_forecast, on='ds', how='left')

    # Prepare input features for the test data
    X_test = test_with_forecasts[['yhat', 'prediction', 'y']]

    # Predict using the logistic regression model
    ensemble_forecast = lr_model.predict(X_test)

    return ensemble_forecast

# Define the function to calculate metrics
def calculate_metrics(test_df, ensemble_forecast):
    accuracy = accuracy_score(test_df['y'] > 0, ensemble_forecast)
    precision = precision_score(test_df['y'] > 0, ensemble_forecast)
    recall = recall_score(test_df['y'] > 0, ensemble_forecast)
    f1 = f1_score(test_df['y'] > 0, ensemble_forecast)

    results_lr = pd.DataFrame({'Model': ['Stacking Ensemble with Logistic Regression'],
                               'Accuracy': [accuracy],
                               'Precision': [precision],
                               'Recall': [recall],
                               'F1 Score': [f1]}).round(2)
    print(results_lr)

# Read input data
input_df = read_input_data('grocery_one_series')

# Preprocess input data
input_df = preprocess_input_data(input_df)

# Group input data
grouped_df = group_input_data(input_df)

# Split data into train and test sets
train_df = grouped_df.loc[grouped_df['test_indicator'] == 0]
test_df = grouped_df.loc[grouped_df['test_indicator'] == 1]

# Read holiday data
holiday_df = read_holiday_data('prophet_holidays')

# Fit and predict using Prophet model
p_forecast = fit_and_predict_prophet(train_df, test_df, holiday_df)

# Fit and predict using Orbit model
o_forecast = fit_and_predict_orbit(train_df, test_df)

# Fit and predict using KTR model
column_names = ['onpromotion', 'transactions']
k_forecast = fit_and_predict_ktr(train_df, test_df, column_names)

# Stacking Ensemble with Logistic Regression
ensemble_forecast = stacking_ensemble(test_df, p_forecast, o_forecast, k_forecast)

# Calculate metrics
calculate_metrics(test_df, ensemble_forecast)

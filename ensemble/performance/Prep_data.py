import pandas as pd 
import numpy as np 
import fileUtils
import datetime





if __name__=="__main__":
    nyc_df = fileUtils.read_csv('nyc_energy_consumption')
    print(nyc_df.isna().sum())
    nyc_df = nyc_df.fillna(method='ffill')
    print(nyc_df.isna().sum())
    nyc_df['timeStamp'] = pd.to_datetime(nyc_df['timeStamp'])
    daily_nyc_df = nyc_df.groupby(pd.Grouper(key='timeStamp', freq='D')).agg({'demand': 'sum','precip':'mean','temp':'mean'})
    daily_nyc_df.reset_index(inplace=True)
    print(daily_nyc_df.shape)
    print(daily_nyc_df.head(5))
    print(daily_nyc_df.tail(5))
    daily_nyc_df.to_csv('nyc_daily_energy.csv')

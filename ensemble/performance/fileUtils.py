import os
import pandas as pd 
import numpy as np 


def read_csv(filename:str):
    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = filename + '.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    df = pd.read_csv(file_path)
    return df

def to_dateType(dataframe:pd.DataFrame):

    if 'date' in dataframe.columns:
        dataframe['date'] = pd.to_datetime(dataframe['date'])
    
    return dataframe

def find_update_missing_dates(df:pd.DataFrame):

    # Initialize min and max dates
    min_date = df['date'].min()
    max_date = df['date'].max()

    # Create date range
    expected_dates = pd.date_range(start=min_date, end=max_date)

    # Filter missing dates in dataframe
    missing_dates = expected_dates[~expected_dates.isin(df['date'])]

    # Verify completeness of the data
    if len(missing_dates) == 0:
        print("The train dataset is complete. It includes all the required dates.")
    else:
        print("The train dataset is incomplete. The following dates are missing:")
        print(missing_dates)
    
    # Create a DataFrame with the missing dates, using the 'date' column
    missing_data = pd.DataFrame({'date': missing_dates})

    # Concatenate the original train dataset and the missing data DataFrame
    # ignore_index=True ensures a new index is assigned to the resulting DataFrame
    df = pd.concat([df, missing_data], ignore_index=True)

    # Sort the DataFrame based on the 'date' column in ascending order
    df['date'] = pd.to_datetime(df['date'])
    df.sort_values('date', inplace=True)
    return df

def verify_dates_completeness(df:pd.DataFrame):

    # Initialize min and max dates
    min_date = df['date'].min()
    max_date = df['date'].max()

    # Create date range
    expected_dates = pd.date_range(start=min_date, end=max_date)

    # Filter missing dates in dataframe
    missing_dates = expected_dates[~expected_dates.isin(df['date'])]

    # Verify completeness of the data
    if len(missing_dates) == 0:
        print("The train dataset is complete. It includes all the required dates.")
    else:
        print("The train dataset is incomplete. The following dates are missing:")
        print(missing_dates)






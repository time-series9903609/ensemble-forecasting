import pandas as pd
import numpy as np
# from sklearn.preprocessing import StandardScaler
# from multiprocessing import Pool, cpu_count
from typing import List, Any
import orbit
from orbit.models import DLT 
from functools import partial
import fileUtils
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
import matplotlib.pyplot as plt
from datetime import datetime 

# def fit_model(train_data):
#     try:
#         column_names = ['onpromotion', 'transactions']
        
#         model = DLT(response_col='sales', 
#                     date_col='date', 
#                     seasonality=365,
#                     global_trend_option='linear', 
#                     damped_factor=0.4, 
#                     regressor_col=column_names)
#         model.fit(train_data)
#         return model
#     except Exception as e:
#         print(f"Error fitting model: {e}")
#         return None

# def predict_forecast(model, test_data):
#     try:
#         forecast = model.predict(test_data)
#         return forecast
#     except Exception as e:
#         print(f"Error predicting forecast: {e}")
#         return None

# def fit_and_predict(df:pd.DataFrame):
#     train_data = df.loc[df['test_indicator'] == 0]
#     test_data = df.loc[df['test_indicator'] == 1]
#     model = fit_model(train_data)
#     if model:
#         forecast = predict_forecast(model, test_data)
#         return forecast
#     else:
#         return None

if __name__ == "__main__":
    # try:
    input_df = fileUtils.read_csv('synthetic_time_series_data')
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['date'] = pd.to_datetime(input_df['date'])
    input_df.rename(columns={'date':'ds','energy_demand':'y'}, inplace=True)
    train_df = input_df[input_df['ds'].dt.year.isin([2015, 2016, 2017, 2018,2019])]
    test_df = input_df[input_df['ds'].dt.year == 2020]
    
    column_names = ['precipitation', 'temperature']
    
    model_time = datetime.now()
    print("Model time:", model_time)
    model = DLT(response_col='y', 
                date_col='ds', 
                seasonality=365,
                estimator = 'stan-map',
                global_trend_option='linear', 
                # seasonality_sm_input=1,
                damped_factor=0.8, 
                regressor_col=column_names)
    model.fit(train_df)
    predict_time = datetime.now()
    print("Total Fitting time :", predict_time-model_time)
    print("Predict start time :", predict_time)
    forecast = model.predict(test_df)
    # fitted_values = model.predict(train_data)
    forecast['actual_aqi'] = test_df['y'].values
    predict_complete_time = datetime.now()
    print("Total Predict time :", predict_complete_time-predict_time)
    
    # fitted_values_data = pd.concat([fitted_values, forecast], axis=1)
    # forecast.to_csv('orbit_forecast_map.csv')
    # fitted_values_data.to_csv('orbit_fitted_values.csv')
    # forecast[['date','prediction','actual_sales']]
    # print(forecast[['date','prediction','actual_sales']].head(20))
    # forecast[['date','prediction','actual_sales']].to_csv('orbit_forecast_14_03_2024.csv')

    # Calculate metrics
    lr_mse = mean_squared_error(test_df['y'].astype(float), forecast['prediction'])
    lr_mae = mean_absolute_error(test_df['y'].astype(float), forecast['prediction'])

    # Apply the absolute value function to both y_eval and lr_predictions
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(forecast['prediction'])

    # Calculate the Root Mean Squared Logarithmic Error (RMSLE)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))

    # Create a DataFrame to store results for Linear Regression
    results_lr = pd.DataFrame({'Model': ['Orbit Model'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]}).round(2)

    # Print the results_lr dataframe
    print(results_lr)

    plt.figure(figsize=(10, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual', color='blue')
    plt.plot(forecast['ds'], forecast['prediction'], label='Forecast', color='red')
    # plt.fill_between(forecast['date'], forecast['prediction_5'], forecast['prediction_95'], color='gray', alpha=0.3)
    plt.title('Actual vs. Forecast Data')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()


    # plt.plot(test_data.index, test_data.values, label='Actual Data')
    # plt.plot(forecast.index, forecast.values, label='Forecast Data')
    # plt.xlabel('Date')
    # plt.ylabel('Sales')  
    # plt.title('Actual Data vs Forecast Data')
    # plt.legend()
    # plt.show()

import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from prophet import Prophet
import orbit
from orbit.models import DLT 
import fileUtils 
import matplotlib.pyplot as plt  


def read_input_data(file_name):
    input_df = fileUtils.read_csv(file_name)
    print(input_df.shape)
    print(input_df.head(5))
    return input_df

def preprocess_input_data(input_df):
    input_df = fileUtils.find_update_missing_dates(input_df)
    fileUtils.verify_dates_completeness(input_df)
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')
    input_df = input_df.sort_values(by='date')
    input_df.rename(columns={'date':'ds','sales':'y'}, inplace=True)
    return input_df

def group_input_data(input_df):
    grouped_df = input_df.groupby(['store_nbr', 'ds']).agg({'y':'sum', 
                                                            'transactions':'sum', 
                                                            'onpromotion':'sum'}).reset_index()
    grouped_df['test_indicator'] = 0
    grouped_df.loc[grouped_df['ds'].dt.year >= 2017, 'test_indicator'] = 1
    return grouped_df

def read_holiday_data(file_name):
    holiday_df = fileUtils.read_csv(file_name)
    print(holiday_df.columns)
    holiday_df = holiday_df.drop(holiday_df.columns[holiday_df.columns.str.contains('Unnamed',case=False)], axis=1)
    print(holiday_df.columns)
    return holiday_df

def fit_and_predict_prophet(train_df, test_df):
    model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=10,
                    weekly_seasonality=False,
                    changepoint_range=0.95,
                    # holidays=holiday_df,
                    # holidays_mode='multiplicative',
                    seasonality_mode='additive')
    # model.add_seasonality(name='weekly_on_season',period=7,fourier_order=3)
    model.add_regressor('PM2.5')
    model.add_regressor('PM10')
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast

def fit_and_predict_orbit(train_df, test_df):
    column_names = ['PM2.5', 'PM10']
    model = DLT(response_col='y', 
                date_col='ds', 
                seasonality=365,
                estimator='stan-map',
                global_trend_option='linear', 
                damped_factor=0.4, 
                regressor_col=column_names)
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast

def simple_averaging_ensemble(test_df, p_forecast, o_forecast):
    test_df.reset_index(inplace=True)
    p_forecast.reset_index(inplace=True)
    o_forecast.reset_index(inplace=True)
    print("test dataframe shape",test_df.shape)
    print("p forecast dataframe shape",p_forecast.shape)
    print("o forecast dataframe shape",o_forecast.shape)
    ensemble_forecast = pd.DataFrame()
    ensemble_forecast['ds'] = test_df['ds'] 
    ensemble_forecast['actual_aqi'] = test_df['y'] 
    ensemble_forecast['prophet_forecast'] = p_forecast['yhat']
    ensemble_forecast['orbit_forecast'] = o_forecast['prediction']
    ensemble_forecast['ensemble_forecast'] = (p_forecast['yhat'] + o_forecast['prediction']) / 2
    print(ensemble_forecast)
    return ensemble_forecast

def calculate_metrics(test_df, ensemble_forecast):
    lr_mse = mean_squared_error(test_df['y'].astype(float), ensemble_forecast['ensemble_forecast'])
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(ensemble_forecast['ensemble_forecast'])
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': ['Simple Average Model'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    print(results_lr)


if __name__ == "__main__":
    # Read input data
    input_df = read_input_data('aq_hyd_cleaned_data')

    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['Date'] = pd.to_datetime(input_df['Date'])
    input_df.rename(columns={'Date':'ds','AQI':'y'}, inplace=True)
    train_df = input_df[input_df['ds'].dt.year.isin([2015, 2016, 2017, 2018,2019])]
    test_df = input_df[input_df['ds'].dt.year == 2020]

    # # Preprocess input data
    # input_df = preprocess_input_data(input_df)

    # # Group input data
    # grouped_df = group_input_data(input_df)

    # # Split data into train and test sets
    # train_df = grouped_df.loc[grouped_df['test_indicator'] == 0]
    # test_df = grouped_df.loc[grouped_df['test_indicator'] == 1]

    # # Read holiday data
    # holiday_df = read_holiday_data('prophet_holidays')

    # Fit and predict using Prophet model
    p_forecast = fit_and_predict_prophet(train_df, test_df)
    print(p_forecast.head())

    # Fit and predict using Orbit model
    o_forecast = fit_and_predict_orbit(train_df, test_df)
    print(o_forecast.head())

    # Simple Averaging Ensemble
    ensemble_forecast = simple_averaging_ensemble(test_df, p_forecast, o_forecast)
    print(ensemble_forecast.head())

    ensemble_forecast.to_csv('Simple_average_14_03_2024.csv')

    # Calculate metrics
    calculate_metrics(test_df, ensemble_forecast)

    # Plotting the test vs. forecast data
    plt.figure(figsize=(10, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual', color='blue')
    plt.plot(test_df['ds'], p_forecast['yhat'], label='Prophet', color='yellow')
    plt.plot(test_df['ds'], o_forecast['prediction'], label='Orbit', color='green')
    plt.plot(ensemble_forecast['ds'], ensemble_forecast['ensemble_forecast'], label='Forecast', color='red')
    # plt.fill_between(forecast_df['ds'], forecast_df['yhat_lower'], forecast_df['yhat_upper'], color='gray', alpha=0.3)
    plt.title('Actual vs. Forecast Data (Simple Average)')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()

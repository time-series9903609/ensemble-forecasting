import pandas as pd 
import numpy as np 
import fileUtils
import datetime
from pandas.api.types import CategoricalDtype
import os


cat_type = CategoricalDtype(categories=['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'], ordered=True)

def create_features(df, label=None):
    """
    Creates time series features from datetime index.
    """
    df = df.copy()
    df['date'] = df.index
    df['hour'] = df['date'].dt.hour
    df['dayofweek'] = df['date'].dt.dayofweek
    df['weekday'] = df['date'].dt.day_name()
    df['weekday'] = df['weekday'].astype(cat_type)
    df['quarter'] = df['date'].dt.quarter
    df['month'] = df['date'].dt.month
    df['year'] = df['date'].dt.year
    df['dayofyear'] = df['date'].dt.dayofyear
    df['dayofmonth'] = df['date'].dt.day
    df['weekofyear'] = df['date'].dt.isocalendar().week  # Use isocalendar().week
    df['date_offset'] = (df.date.dt.month * 100 + df.date.dt.day - 320) % 1300

    df['season'] = pd.cut(df['date_offset'], [0, 300, 602, 900, 1300], labels=['Spring', 'Summer', 'Fall', 'Winter'])
    X = df[['hour', 'dayofweek', 'quarter', 'month', 'year', 'dayofyear', 'dayofmonth', 'weekofyear', 'weekday', 'season']]
    if label:
        y = df[label]
        return X, y
    return X



if __name__=="__main__":
    load_df = fileUtils.read_csv('continuous dataset')
    print(type(load_df))
    load_df['datetime'] = pd.to_datetime(load_df['datetime'])
    load_df.rename(columns={'datetime':'date'}, inplace=True)
    load_df.set_index('date', inplace=True)
    X, y = create_features(load_df, label='nat_demand')
    features_and_target = pd.concat([X, y], axis=1)
    print(features_and_target.head(10))

    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'load_forecasting_cleaned_data' + '.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    load_df.to_csv(file_path, index=False)
    # print(load_df.index.year)
    # print(load_df.tail(10))

    print(X.tail(10))

    # Q1 = load_df['nat_demand'].quantile(0.25)
    # Q3 = load_df['nat_demand'].quantile(0.75)
    # IQR = Q3 - Q1
    # lower_bound = Q1 - 1.5 * IQR
    # upper_bound = Q3 + 1.5 * IQR


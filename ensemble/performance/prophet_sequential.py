import pandas as pd
import os 
import numpy as np
import fileUtils 
from sklearn.preprocessing import StandardScaler
from prophet import Prophet
import matplotlib.pyplot as plt  
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from datetime import datetime
import matplotlib.pyplot as plt

if __name__ == "__main__":

    input_df = fileUtils.read_csv('aq_hyd_cleaned_data')
    # print(input_df.shape)
    # print(input_df.head(5))
    print(input_df.info())

    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['Date'] = pd.to_datetime(input_df['Date'])
    input_df.rename(columns={'Date':'ds','AQI':'y'}, inplace=True)
    train_df = input_df[input_df['ds'].dt.year.isin([2015, 2016, 2017, 2018,2019])]
    test_df = input_df[input_df['ds'].dt.year == 2020]

    # holiday_df = fileUtils.read_csv('prophet_holidays')
    # print(holiday_df.columns)
    # holiday_df = holiday_df.drop(holiday_df.columns[holiday_df.columns.str.contains('Unnamed',case=False)], axis=1)
    # print(holiday_df.columns)
    plt.plot(train_df['ds'], train_df['y'], label='AQI')
    plt.xlabel('date')
    plt.ylabel('AQI')
    plt.show()

    model_fitting_time = datetime.now()
    print("Model Fit starting time :", model_fitting_time)
    model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    # weekly_seasonality=True,
                    changepoint_range=0.95,
                    # holidays=holiday_df,
                    # holidays_mode='multiplicative',
                    seasonality_mode='additive')
    # model.add_seasonality(name='weekly_on_season',period=7,fourier_order=3)
    # model.add_seasonality(name='weekly_on_season',period=7,fourier_order=3, condition_name='off_season')
    model.add_regressor('PM2.5')
    model.add_regressor('PM10')

    model.fit(train_df)
    model_fitting_timef = datetime.now()
    print("Model Fitting Ending time :", model_fitting_timef)
    print("Total Fitting time :",model_fitting_timef-model_fitting_time )
    print("Model Predict Start time :", model_fitting_timef)
    # fitted_df = model.predict(train_df)
    forecast_df = model.predict(test_df)
    forecast_df['actual_aqi'] = test_df['y'].values
    model_predict_timef = datetime.now()
    print("Model Predict Ending time :", model_predict_timef)
    print("Total Predicting time :",model_predict_timef-model_predict_timef )

    print(forecast_df[['ds','yhat','actual_aqi']].head())

    # fitted_values_df = pd.concat([fitted_df, forecast_df], axis=1)

    # fitted_values_df.to_csv('prophet_fitted_values.csv')
    # forecast_df[['ds','yhat','actual_sales']].to_csv('prophet_forecast_14_03_2024.csv')

    # Calculate metrics
    lr_mse = mean_squared_error(test_df['y'].astype(float), forecast_df['yhat'])
    lr_mae = mean_absolute_error(test_df['y'].astype(float), forecast_df['yhat'])

    # Apply the absolute value function to both y_eval and lr_predictions
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(forecast_df['yhat'])

    # Calculate the Root Mean Squared Logarithmic Error (RMSLE)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))

    # Create a DataFrame to store results for Linear Regression
    results_lr = pd.DataFrame({'Model': ['Prophet Model'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]}).round(2)

    # Print the results_lr dataframe
    print(results_lr)

    # Plotting the test vs. forecast data
    plt.figure(figsize=(10, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual', color='blue')
    plt.plot(forecast_df['ds'], forecast_df['yhat'], label='Forecast', color='red')
    plt.fill_between(forecast_df['ds'], forecast_df['yhat_lower'], forecast_df['yhat_upper'], color='gray', alpha=0.3)
    plt.title('Actual vs. Forecast Data')
    plt.xlabel('Date')
    plt.ylabel('AQI')
    plt.legend()
    plt.show()





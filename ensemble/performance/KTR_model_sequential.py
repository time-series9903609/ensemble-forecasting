import pandas as pd
import numpy as np
# from sklearn.preprocessing import StandardScaler
from multiprocessing import Pool, cpu_count
from typing import List, Any
import orbit
from orbit.models import KTR 
from functools import partial
import fileUtils
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
import matplotlib.pyplot as plt
from datetime import datetime 



if __name__ == "__main__":
    
    # Read input data
    input_df = fileUtils.read_csv('aq_hyd_cleaned_data')
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['Date'] = pd.to_datetime(input_df['Date'])
    input_df.rename(columns={'Date':'ds','AQI':'y'}, inplace=True)
    train_df = input_df[input_df['ds'].dt.year.isin([2015, 2016, 2017, 2018,2019])]
    test_df = input_df[input_df['ds'].dt.year == 2020]


    column_names = ['PM2.5', 'PM10']
    

    model = KTR(response_col='y', 
                date_col='ds',
                estimator='pyro-svi',
                seed=2000, 
                seasonality=[365.25],
                regressor_col=column_names,
                prediction_percentiles=[5, 95],
                regressor_sign=['+','+'])
    model.fit(train_df)
    forecast = model.predict(test_df)


    # plt.figure(figsize=(10, 6))
    # plt.plot(test_data['date'], test_data['sales'], label='Actual', color='blue')
    # plt.plot(forecast['date'], forecast['prediction'], label='Forecast', color='red')
    # plt.fill_between(forecast['date'], forecast['prediction_5'], forecast['prediction_95'], color='gray', alpha=0.3)
    # plt.title('Actual vs. Forecast Data')
    # plt.xlabel('Date')
    # plt.ylabel('Sales')
    # plt.legend()
    # plt.show()

    # Calculate metrics
    lr_mse = mean_squared_error(test_df['y'].astype(float), forecast['prediction'])
    lr_mae = mean_absolute_error(test_df['y'].astype(float), forecast['prediction'])

    # Apply the absolute value function to both y_eval and lr_predictions
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(forecast['prediction'])

    # Calculate the Root Mean Squared Logarithmic Error (RMSLE)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))

    # Create a DataFrame to store results for Linear Regression
    results_lr = pd.DataFrame({'Model': ['Orbit KTR Model'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)

    # Print the results_lr dataframe
    print(results_lr)


    # plt.plot(test_data.index, test_data.values, label='Actual Data')
    # plt.plot(forecast.index, forecast.values, label='Forecast Data')
    # plt.xlabel('Date')
    # plt.ylabel('Sales')  
    # plt.title('Actual Data vs Forecast Data')
    # plt.legend()
    # plt.show()

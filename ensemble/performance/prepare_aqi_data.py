import pandas as pd 
import numpy as np 
import fileUtils
import os
import seaborn as sns
import matplotlib.pyplot as plt


if __name__ == "__main__":
    input_df = fileUtils.read_csv('city_day')
    print(input_df.shape)
    print(input_df.head())
    print(input_df.isna().sum())
    hyd_df = input_df[input_df['City']=='Hyderabad']
    print(hyd_df['City'].unique())
    print(hyd_df.shape)
    print(hyd_df.isna().sum())
    hyd_df = hyd_df.fillna(method='ffill')
    hyd_df[['PM2.5','PM10','NH3','AQI','AQI_Bucket']] = hyd_df[['PM2.5','PM10','NH3','AQI','AQI_Bucket']].fillna(method='bfill')
    print(hyd_df.isna().sum())
    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'aq_hyd_cleaned_data' + '.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    hyd_df.to_csv(file_path, index=False)

    #correlation matrix
    # Select only the numeric columns for correlation calculation
    numeric_columns = ['PM2.5', 'PM10', 'NO', 'NO2', 'NOx', 'NH3', 'CO', 'SO2', 'O3', 'Benzene', 'Toluene', 'Xylene', 'AQI']
    numeric_data = hyd_df[numeric_columns]

    correlation_matrix = numeric_data.corr()
    print(correlation_matrix)

    plt.figure(figsize=(12, 8))
    sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt=".2f", linewidths=0.5)
    plt.title('Correlation Matrix')
    plt.show()

    hyd_df['Date'] = pd.to_datetime(hyd_df['Date'])
    plt.plot(hyd_df['Date'], hyd_df['AQI'], label='AQI')
    plt.xlabel('date')
    plt.ylabel('AQI')
    plt.show()

 


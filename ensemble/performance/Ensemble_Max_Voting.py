import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from prophet import Prophet
import orbit
from orbit.models import DLT 
import fileUtils 
import matplotlib.pyplot as plt  


def fit_and_predict_prophet(train_df, test_df, holiday_df):
    model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    changepoint_range=0.95,
                    holidays=holiday_df,
                    holidays_mode='additive',
                    seasonality_mode='additive')
    model.add_regressor('onpromotion')
    model.add_regressor('transactions')
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast

def fit_and_predict_orbit(train_df, test_df):
    column_names = ['onpromotion', 'transactions']
    model = DLT(response_col='y', 
                date_col='ds', 
                seasonality=365,
                global_trend_option='linear', 
                damped_factor=0.4, 
                regressor_col=column_names)
    model.fit(train_df)
    forecast = model.predict(test_df)
    return forecast

def max_voting_ensemble(test_df, p_forecast, o_forecast):
    test_df.reset_index(inplace=True)
    p_forecast.reset_index(inplace=True)
    o_forecast.reset_index(inplace=True)
    
    # Select the maximum forecast value between Prophet and Orbit models
    ensemble_forecast = pd.DataFrame()
    ensemble_forecast['ds'] = test_df['ds']
    ensemble_forecast['prophet_forecast'] = p_forecast['yhat']
    ensemble_forecast['orbit_forecast'] = o_forecast['prediction']
    ensemble_forecast['ensemble_forecast'] = np.maximum(p_forecast['yhat'], o_forecast['prediction'])
    
    return ensemble_forecast



def calculate_metrics(test_df, ensemble_forecast):
    lr_mse = mean_squared_error(test_df['y'].astype(float), ensemble_forecast['ensemble_forecast'])
    lr_mae = mean_absolute_error(test_df['y'].astype(float), ensemble_forecast['ensemble_forecast'])
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(ensemble_forecast['ensemble_forecast'])
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': ['Ensemble Model'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)],
                            'MSE': [lr_mse],
                            'MAE': [lr_mae]}).round(2)
    print(results_lr)


if __name__ == "__main__":
    # Read input data
    input_df = read_input_data('grocery_one_series')

    # Preprocess input data
    input_df = preprocess_input_data(input_df)

    # Group input data
    grouped_df = group_input_data(input_df)

    # Split data into train and test sets
    train_df = grouped_df.loc[grouped_df['test_indicator'] == 0]
    test_df = grouped_df.loc[grouped_df['test_indicator'] == 1]

    # Read holiday data
    holiday_df = read_holiday_data('prophet_holidays')

    # Fit and predict using Prophet model
    p_forecast = fit_and_predict_prophet(train_df, test_df, holiday_df)

    # Fit and predict using Orbit model
    o_forecast = fit_and_predict_orbit(train_df, test_df)

    # Simple Averaging Ensemble
    ensemble_forecast = max_voting_ensemble(test_df, p_forecast, o_forecast)

    # Calculate metrics
    calculate_metrics(test_df, ensemble_forecast)

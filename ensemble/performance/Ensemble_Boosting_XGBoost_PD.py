import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from prophet import Prophet
from orbit.models import DLT
import fileUtils
import xgboost as xgb
import matplotlib.pyplot as plt

def read_input_data(file_name):
    input_df = fileUtils.read_csv(file_name)
    print(input_df.shape)
    print(input_df.head(5))
    print("forecast NaN values count :", input_df.isna().sum())
    return input_df

def preprocess_input_data(input_df):
    input_df = fileUtils.find_update_missing_dates(input_df)
    input_df = input_df.fillna(method='ffill')
    fileUtils.verify_dates_completeness(input_df)
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')
    input_df = input_df.sort_values(by='date')
    input_df.rename(columns={'date': 'ds', 'sales': 'y'}, inplace=True)
    return input_df

def group_input_data(input_df):
    grouped_df = input_df.groupby(['store_nbr', 'ds']).agg({'y': 'sum',
                                                            'transactions': 'sum',
                                                            'onpromotion': 'sum'}).reset_index()
    grouped_df['test_indicator'] = 0
    grouped_df.loc[grouped_df['ds'].dt.year >= 2017, 'test_indicator'] = 1
    return grouped_df

def read_holiday_data(file_name):
    holiday_df = fileUtils.read_csv(file_name)
    holiday_df = holiday_df.drop(holiday_df.columns[holiday_df.columns.str.contains('Unnamed', case=False)], axis=1)
    return holiday_df

def fit_and_predict_prophet(train_df, test_df):
    model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=10,
                    weekly_seasonality=False,
                    changepoint_range=0.95,
                    # holidays=holiday_df,
                    # holidays_mode='multiplicative',
                    seasonality_mode='additive')
    # model.add_seasonality(name='weekly_on_season',period=7,fourier_order=3)
    model.add_regressor('PM2.5')
    model.add_regressor('PM10')
    model.fit(train_df)
    p_fitted_values = model.predict(train_df)
    p_forecast = model.predict(test_df)
    return p_fitted_values, p_forecast

def fit_and_predict_orbit(train_df, test_df):
    column_names = ['PM2.5', 'PM10']
    model = DLT(response_col='y',
                date_col='ds',
                seasonality=365,
                estimator='stan-map',
                global_trend_option='linear',
                damped_factor=0.9,
                regressor_col=column_names)
    model.fit(train_df)
    o_fitted_values = model.predict(train_df)
    o_forecast = model.predict(test_df)
    return o_fitted_values, o_forecast

def boosting_ensemble_with_xgboost(train_df, p_fitted, o_fitted, test_df, p_forecast, o_forecast, random_seed=42):
    # Train XGBoost model for Prophet forecast
    xgb_prophet = xgb.XGBRegressor(seed=random_seed)
    xgb_prophet.fit(p_fitted[['yhat']], train_df['y'])

    # Train XGBoost model for Orbit forecast
    xgb_orbit = xgb.XGBRegressor(seed=random_seed)
    xgb_orbit.fit(o_fitted[['prediction']], train_df['y'])

    # Predict using XGBoost models
    p_forecast['ensemble_forecast'] = xgb_prophet.predict(p_forecast[['yhat']])
    o_forecast['ensemble_forecast'] = xgb_orbit.predict(o_forecast[['prediction']])

    # Combine predictions using simple average
    ensemble_forecast = (p_forecast['yhat'] * 0.4) + (o_forecast['prediction'] * 0.6)
    # ensemble_forecast = (p_forecast['ensemble_forecast'] + o_forecast['ensemble_forecast']) / 2

    ensemble_forecast = pd.DataFrame(ensemble_forecast, columns=['ensemble_forecast'])
    ensemble_forecast['ds'] = test_df['ds'].values
    ensemble_forecast = ensemble_forecast[['ds', 'ensemble_forecast']]
    ensemble_forecast['actual_sales'] = test_df['y'].values
    ensemble_forecast = ensemble_forecast[['ds', 'actual_sales', 'ensemble_forecast']]
    ensemble_forecast['prophet_prediction'] = p_forecast['yhat'].values
    ensemble_forecast['orbit_prediction'] = o_forecast['prediction'].values
    return ensemble_forecast

def calculate_metrics(test_df, ensemble_forecast):
    lr_mse = mean_squared_error(test_df['y'], ensemble_forecast['ensemble_forecast'])
    lr_rmsle = np.sqrt(mean_squared_log_error(test_df['y'], ensemble_forecast['ensemble_forecast']))
    results_lr = pd.DataFrame({'Model': ['Boosting Ensemble with XGBoost'],
                               'RMSLE': [lr_rmsle],
                               'RMSE': [np.sqrt(lr_mse)]}).round(2)
    print(results_lr)

if __name__ == "__main__":
    # Read input data
    input_df = read_input_data('aq_hyd_cleaned_data')

    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['Date'] = pd.to_datetime(input_df['Date'])
    input_df.rename(columns={'Date':'ds','AQI':'y'}, inplace=True)
    train_df = input_df[input_df['ds'].dt.year.isin([2015, 2016, 2017, 2018,2019])]
    test_df = input_df[input_df['ds'].dt.year == 2020]

    # Read holiday data
    # holiday_df = read_holiday_data('prophet_holidays')

    # Fit and predict using Prophet model
    p_fitted, p_forecast = fit_and_predict_prophet(train_df, test_df)

    # Fit and predict using Orbit model
    o_fitted, o_forecast = fit_and_predict_orbit(train_df, test_df)

    # Boosting Ensemble with XGBoost
    ensemble_forecast = boosting_ensemble_with_xgboost(train_df, p_fitted, o_fitted, test_df, p_forecast, o_forecast, random_seed=42)

    ensemble_forecast.to_csv('XGBoost_Boosting_forecast.csv')

    # Calculate metrics
    calculate_metrics(test_df, ensemble_forecast)

    plt.figure(figsize=(10, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual', color='red')
    plt.plot(test_df['ds'], ensemble_forecast['ensemble_forecast'], label='Ensemble Forecast', color='blue')
    # plt.plot(p_forecast['ds'], p_forecast['yhat'], label='Prophet', color='yellow')
    # plt.plot(o_forecast['ds'], o_forecast['prediction'], label='Orbit', color='green')
    plt.title('Boosting Ensemble (XGBoost) - Actual vs. Forecast Data')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()

    plt.figure(figsize=(10, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual', color='red')
    plt.plot(test_df['ds'], ensemble_forecast['ensemble_forecast'], label='Ensemble Forecast', color='blue')
    plt.plot(p_forecast['ds'], p_forecast['yhat'], label='Prophet', color='yellow')
    plt.plot(o_forecast['ds'], o_forecast['prediction'], label='Orbit', color='green')
    plt.title('Boosting Ensemble (XGBoost) - Actual vs. Forecast Data (Combined)')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()

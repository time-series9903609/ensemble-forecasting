import pandas as pd
from numpy.random import randn, normal
from datetime import datetime, timedelta
# from holidays import USholidays
import numpy as np
import os


def generate_timeseries(start_date, end_date):
  """
  Generates dummy time series data with trend, seasonality, and holiday effects.

  Args:
      start_date: Start date as string in format YYYY-MM-DD.
      end_date: End date as string in format YYYY-MM-DD.

  Returns:
      pandas.DataFrame: DataFrame containing time series data.
  """

  # Convert dates to datetime format
  start_date = datetime.strptime(start_date, "%Y-%m-%d")
  end_date = datetime.strptime(end_date, "%Y-%m-%d")

  # Create date range
  dates = pd.date_range(start_date, end_date, freq="D")

  # Generate base demand with trend and seasonality
  base_demand = 1000 + 50 * dates.dayofyear / 365 + 20 * np.sin(2 * np.pi * dates.month / 12)
  base_demand += randn(len(dates)) * 10  # Add random noise

  # Generate time-varying regression coefficients
  trend_coeff = np.linspace(0.1, 0.01, len(dates))
  seasonality_coeff = np.sin(2 * np.pi * dates.dayofyear / 365)

  # Calculate holiday impact
  # Create a dataframe for US holidays
  us_holidays = pd.DataFrame({
  'holiday': 'us_holidays',
  'ds': pd.to_datetime(['2017-01-01', '2017-07-04', '2017-11-23',
                        '2017-12-25', '2018-01-01', '2018-07-04',
                        '2018-11-22', '2018-12-25', '2019-01-01',
                        '2019-07-04', '2019-11-28', '2019-12-25',
                        '2020-01-01', '2020-07-04', '2020-11-26',
                        '2020-12-25', '2021-01-01', '2021-07-04',
                        '2021-11-25', '2021-12-25', '2022-01-01',
                        '2022-07-04', '2022-11-24', '2022-12-25',
                        '2023-01-01']),
  'lower_window': 0,
  'upper_window': 1,
})
  holiday_impact = dates.isin(us_holidays) * normal(-10, 5, len(dates))

  # Generate final energy demand with effects
  energy_demand = base_demand + trend_coeff * dates.day * trend_coeff + seasonality_coeff * base_demand + holiday_impact

  # Generate precipitation and temperature with some correlation to demand
  precipitation = energy_demand * 0.1 + normal(0, 1, len(dates))
  temperature = energy_demand * 0.2 + normal(10, 2, len(dates))

  # Create DataFrame
  df = pd.DataFrame({
      "date": dates,
      "energy_demand": energy_demand,
      "precipitation": precipitation,
      "temperature": temperature
  })

  return df


# Generate data
data = generate_timeseries("2017-01-01", "2023-08-01")

# Print or save the data
print(data.head())  # Print the first few rows
# data.to_csv("dummy_timeseries.csv", index=False)
current_dir = os.path.dirname(os.path.abspath(__file__))
data_folder = os.path.join(current_dir, '../data')
filename_with_extension = 'synthetic_time_series_data' + '.csv'
file_path = os.path.join(data_folder, filename_with_extension)
data.to_csv(file_path, index=False)
import pandas as pd
import numpy as np
from collections import Counter
from sklearn.ensemble import StackingRegressor
from sklearn.linear_model import LinearRegression
from xgboost import XGBRegressor
from prophet import Prophet
import orbit
from orbit.models import DLT
import fileUtils
from sklearn.metrics import mean_squared_error

# Custom Regressor Wrapper for Prophet
class ProphetRegressor:
    def __init__(self, interval_width=0.95, growth='linear', yearly_seasonality=True,
                 changepoint_range=0.95, seasonality_mode='additive'):
        self.model = Prophet(interval_width=interval_width, growth=growth,
                             yearly_seasonality=yearly_seasonality,
                             changepoint_range=changepoint_range,
                             seasonality_mode=seasonality_mode)

    def fit(self, X, y):
        df = X.copy()
        df['y'] = y
        self.model.fit(df)
        return self

    def predict(self, X):
        return self.model.predict(X)['yhat'].values

# Custom Regressor Wrapper for Orbit DLT
class DLTRegressor:
    def __init__(self, response_col='y', date_col='ds', seasonality=365,
                 estimator='stan-map', global_trend_option='linear',
                 damped_factor=0.85, regressor_col=None):
        self.model = DLT(response_col=response_col, date_col=date_col,
                         seasonality=seasonality, estimator=estimator,
                         global_trend_option=global_trend_option,
                         damped_factor=damped_factor, regressor_col=regressor_col)

    def fit(self, X, y):
        df = X.copy()
        df['y'] = y
        self.model.fit(df)
        return self

    def predict(self, X):
        return self.model.predict(X)['prediction'].values

if __name__ == "__main__":
    # Load and preprocess data
    input_df = fileUtils.read_csv('egg_one_store')

    # Data preprocessing
    # input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    # input_df = input_df.drop(columns=['id'])
    # input_df['date'] = pd.to_datetime(input_df['date'])
    # min_date = input_df['date'].min()
    # max_date = input_df['date'].max()
    # expected_dates = pd.date_range(start=min_date, end=max_date)
    # missing_dates = expected_dates[~expected_dates.isin(input_df['date'])]
    # if len(missing_dates) == 0:
    #     print("The train dataset is complete. It includes all the required dates.")
    # else:
    #     print("The train dataset is incomplete. The following dates are missing:")
    #     print(missing_dates)
    #     missing_df = pd.DataFrame({'date':missing_dates})
    #     eggs1_dataframe = pd.concat([input_df, missing_df], ignore_index=True)
    #     missing_dates = expected_dates[~expected_dates.isin(eggs1_dataframe['date'])]
    #     if len(missing_dates) == 0:
    #         print("The train dataset is complete. It includes all the required dates.")
    # input_df.rename(columns={'date':'ds','sales':'y'}, inplace=True)
    # input_df = input_df.fillna(method='ffill')
    # input_df['test_indicator'] = 0
    # input_df.loc[input_df['ds'].dt.year == 2017, 'test_indicator'] = 1

    # Drop duplicates
    input_df['ds'] = pd.to_datetime(input_df['ds'])
    input_df.drop_duplicates(subset=['ds'], keep='first', inplace=True)
    train_df = input_df[input_df['test_indicator'] == 0]
    test_df = input_df[input_df['test_indicator'] == 1]

    # Prophet Model
    p_model = ProphetRegressor()
    p_model.fit(train_df[['ds', 'onpromotion', 'transactions', 'dcoilwtico']], train_df['y'])
    p_fitted = p_model.predict(train_df[['ds', 'onpromotion', 'transactions', 'dcoilwtico']])
    p_forecast = p_model.predict(test_df[['ds', 'onpromotion', 'transactions', 'dcoilwtico']])

    # Orbit model
    o_model = DLTRegressor()
    o_model.fit(train_df[['ds', 'onpromotion', 'transactions', 'dcoilwtico']], train_df['y'])
    o_fitted = o_model.predict(train_df[['ds', 'onpromotion', 'transactions', 'dcoilwtico']])
    o_forecast = o_model.predict(test_df[['ds', 'onpromotion', 'transactions', 'dcoilwtico']])

    # Define base estimators for StackingRegressor
    estimator_list = [
        ('prophet', p_model),
        ('orbit', o_model)
    ]

    # Build stack model
    stack_model = StackingRegressor(
        estimators=estimator_list, final_estimator=LinearRegression()
    )

    # Prepare data for training
    X_train = train_df[['onpromotion', 'transactions', 'dcoilwtico']]
    y_train = train_df['y']
    X_test = test_df[['onpromotion', 'transactions', 'dcoilwtico']]

    # Train stacked model
    stack_model.fit(X_train, y_train)

    # Make predictions
    y_train_pred = stack_model.predict(X_train)
    y_test_pred = stack_model.predict(X_test)

    print("StackingRegressor Train RMSE:", np.sqrt(mean_squared_error(y_train, y_train_pred)))
    print("StackingRegressor Test RMSE:", np.sqrt(mean_squared_error(test_df['y'], y_test_pred)))

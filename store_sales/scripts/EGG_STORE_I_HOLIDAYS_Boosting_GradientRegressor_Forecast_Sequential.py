import pandas as pd
import numpy as np
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from sklearn.base import BaseEstimator, RegressorMixin
from prophet import Prophet
from orbit.models.dlt import DLT
import matplotlib.pyplot as plt
import fileUtils
import os

# Custom estimator for Prophet
class ProphetEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols, holiday_df):
        self.model = None
        self.regressor_cols = regressor_cols
        self.holiday_df = holiday_df
        
    def fit(self, X, y):
        # Convert numpy array back to DataFrame if necessary
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        df = X.copy()
        df['y'] = y  # Ensure y is a pandas Series and align indices
        self.model = Prophet(interval_width=0.95,
                             growth='linear',
                             yearly_seasonality=True,
                             weekly_seasonality=True,
                             changepoint_range=0.95,
                             changepoint_prior_scale=0.1,
                             holidays=self.holiday_df,
                             holidays_mode='additive',
                             seasonality_mode='additive',
                             seasonality_prior_scale=1.0)
        for reg in self.regressor_cols:
            if reg != 'change_point_effect':  # Exclude change_point_effect for Prophet
                self.model.add_regressor(reg)
        self.model.fit(df)
        return self
    
    def predict(self, X):
        # Convert numpy array back to DataFrame if necessary
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        forecast = self.model.predict(X)
        return forecast['yhat'].values

# Custom estimator for Orbit's DLT
class DLTModelEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols):
        self.model = None
        self.regressor_cols = regressor_cols
        
    def fit(self, X, y):
        # Convert numpy array back to DataFrame if necessary
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        df = X.copy()
        df['y'] = y  # Ensure y is a pandas Series and align indices
        self.model = DLT(response_col='y',
                         date_col='ds',
                         seasonality=730,
                         estimator='stan-map',
                         global_trend_option='linear',
                         damped_factor=0.7,
                         regressor_col=self.regressor_cols)
        self.model.fit(df)
        return self
    
    def predict(self, X):
        # Convert numpy array back to DataFrame if necessary
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        forecast = self.model.predict(X)
        return forecast['prediction'].values

def fit_and_predict(input_df, holiday_df):
    train_data = input_df.loc[input_df['test_indicator'] == 0]
    test_data = input_df.loc[input_df['test_indicator'] == 1]
    
    prophet_model = ProphetEstimator(regressor_cols, holiday_df).fit(train_data, train_data['y'])
    dlt_model = DLTModelEstimator(regressor_cols).fit(train_data, train_data['y'])
    
    prophet_train_forecast = prophet_model.predict(train_data)
    prophet_test_forecast = prophet_model.predict(test_data)
    
    dlt_train_forecast = dlt_model.predict(train_data)
    dlt_test_forecast = dlt_model.predict(test_data)
    
    train_forecast = pd.DataFrame({
        'tsid': train_data['tsid'].values,
        'city': train_data['city'].values,
        'ds': train_data['ds'].values,
        'y': train_data['y'].values,
        'prophet_yhat': prophet_train_forecast,
        'orbit_prediction': dlt_train_forecast
    })
    
    test_forecast = pd.DataFrame({
        'tsid': test_data['tsid'].values,
        'city': test_data['city'].values,
        'ds': test_data['ds'].values,
        'y': test_data['y'].values,
        'prophet_yhat': prophet_test_forecast,
        'orbit_prediction': dlt_test_forecast
    })
    
    forecast = pd.concat([train_forecast, test_forecast])
    return forecast

# Function to calculate RMSE
def rmse(y_true, y_pred):
    return np.sqrt(mean_squared_error(y_true, y_pred))

def calculate_metrics(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    return results_lr

if __name__ == "__main__":

    input_df = fileUtils.read_csv('egg_one_store')
    holiday_df = fileUtils.read_csv('eggs_store1_holidays')  
    holiday_df['type'] = 'non_working_day'
    holiday_df['lower_window'] = 0
    holiday_df['upper_window'] = 1
    holiday_df.rename(columns={'type':'holiday','holiday':'ds'}, inplace=True)
    print(holiday_df.head(5))

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    input_df = input_df.sort_values(by='ds').drop_duplicates(subset=['ds'], keep='first')
    change_points = pd.to_datetime(holiday_df['ds'].values)
    change_point_series = pd.Series(0, index=input_df.index)
    for cp in change_points:
        # Mark the period after each change point
        change_point_series.loc[input_df['ds'] >= cp] += 1
    input_df['change_point_effect'] = change_point_series

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    train_df = input_df[input_df['test_indicator'] == 0]
    test_df = input_df[input_df['test_indicator'] == 1]

    # Define regressor columns
    regressor_cols = ['onpromotion', 'dcoilwtico', 'transactions', 'change_point_effect']

    # Ensure 'ds' column is included in the training and test DataFrames
    train_df_with_ds = train_df[regressor_cols + ['ds']]
    test_df_with_ds = test_df[regressor_cols + ['ds']]

    # Create and fit the custom boosting estimator
    prophet_estimator = ProphetEstimator(regressor_cols=regressor_cols, holiday_df=holiday_df)
    dlt_estimator = DLTModelEstimator(regressor_cols=regressor_cols)
    
    # Fit the estimators
    prophet_estimator.fit(train_df_with_ds, train_df['y'])
    dlt_estimator.fit(train_df_with_ds, train_df['y'])

    # Predict on the training and test data to create features for GradientBoostingRegressor
    prophet_train_pred = prophet_estimator.predict(train_df_with_ds)
    dlt_train_pred = dlt_estimator.predict(train_df_with_ds)
    
    prophet_test_pred = prophet_estimator.predict(test_df_with_ds)
    dlt_test_pred = dlt_estimator.predict(test_df_with_ds)

    # Combine predictions as features for boosting
    combined_train_pred = np.vstack((prophet_train_pred, dlt_train_pred)).T
    combined_test_pred = np.vstack((prophet_test_pred, dlt_test_pred)).T

    # Train GradientBoostingRegressor with the combined features
    gbr_model = GradientBoostingRegressor(n_estimators=100, learning_rate=0.68,max_depth=3,loss="squared_error",random_state=42)
    gbr_model.fit(combined_train_pred, train_df['y'])

    # Predict using GradientBoostingRegressor
    final_predictions = gbr_model.predict(combined_test_pred)

    # Evaluate the model's performance
    test_rmse = rmse(test_df['y'], final_predictions)
    print(f'Final RMSE of Gradient Boosting ensemble: {test_rmse}')

    # Plot the results
    plt.figure(figsize=(12, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual')
    plt.plot(test_df['ds'], final_predictions, label='Boosting Prediction')
    plt.xlabel('Date')
    plt.ylabel('Value')
    plt.legend()
    plt.show()

    # Save the results
    forecasted_data = pd.DataFrame({
        'date': test_df['ds'].values,
        'Sales': test_df['y'].values,
        'Prophet': prophet_test_pred,
        'Orbit': dlt_test_pred,
        'GradientBoosting': final_predictions}).round(2)

    print(forecasted_data.head(10))

    # Define the directory and filename for saving the forecasted data
    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'egg_store1_boosting_forecasting.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    
    # Save the forecasted data to a CSV file
    forecasted_data.to_csv(file_path, index=False)

   

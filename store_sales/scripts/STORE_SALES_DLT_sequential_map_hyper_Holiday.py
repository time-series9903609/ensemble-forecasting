import pandas as pd
import numpy as np
from collections import Counter
from orbit.models import DLT 
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import fileUtils

if __name__ == "__main__":
    # Read input data
    input_df = fileUtils.read_csv('agg_store_sales')
    holiday_df = fileUtils.read_csv('eggs_store1_holidays')
    holiday_df['type'] = 'non_working_day'
    holiday_df['lower_window'] = 0
    holiday_df['upper_window'] = 1
    holiday_df.rename(columns={'type':'holiday','holiday':'ds'}, inplace=True)
    input_df['ds'] = pd.to_datetime(input_df['ds'])
    input_df = input_df.sort_values(by='ds').drop_duplicates(subset=['ds'], keep='first')
    # input_df.drop_duplicates(subset=['ds'], keep='first', inplace=True)
    change_points = pd.to_datetime(holiday_df['ds'].values)
    change_point_series = pd.Series(0, index=input_df.index)
    for cp in change_points:
        # Mark the period after each change point
        change_point_series.loc[input_df['ds'] >= cp] += 1
    input_df['change_point_effect'] = change_point_series

    # Splitting the data into train and test
    train_df = input_df[input_df['test_indicator'] == 0]
    test_df = input_df[input_df['test_indicator'] == 1]

    # Parameter grid for hyperparameter tuning
    damped_factors = [0.7, 0.8,0.85, 0.95]
    seasonality_values = [182, 365, 730]  # Assuming yearly and bi-yearly seasonality
    trend_options = ['linear', 'loglinear']

    best_rmse = float('inf')
    best_params = {}

    for damped_factor in damped_factors:
        for seasonality in seasonality_values:
            for trend_option in trend_options:
                model = DLT(response_col='y', 
                            date_col='ds', 
                            seasonality=seasonality,
                            estimator='stan-map',
                            global_trend_option=trend_option, 
                            damped_factor=damped_factor, 
                            regressor_col=['onpromotion', 'dcoilwtico', 'transactions','change_point_effect'])
                model.fit(train_df)
                forecast = model.predict(test_df)
                
                # Calculate RMSE for the current set of parameters
                rmse = np.sqrt(mean_squared_error(test_df['y'], forecast['prediction']))

                if rmse < best_rmse:
                    best_rmse = rmse
                    best_params = {'damped_factor': damped_factor, 'seasonality': seasonality, 'trend_option': trend_option}

    # Output the best parameters and RMSE
    print("Best Parameters:", best_params)
    print("Lowest RMSE:", best_rmse)

    # Fit the best model
    model = DLT(response_col='y', 
                date_col='ds', 
                seasonality=best_params['seasonality'],
                estimator='stan-map',
                global_trend_option=best_params['trend_option'], 
                damped_factor=best_params['damped_factor'], 
                regressor_col=['onpromotion', 'dcoilwtico', 'transactions','change_point_effect'])
    model.fit(train_df)
    forecast = model.predict(test_df)

    # Plot results
    plt.figure(figsize=(10, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual', color='blue')
    plt.plot(forecast['ds'], forecast['prediction'], label='Forecast', color='red')
    plt.title('Actual vs. Forecast Data')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()

import pandas as pd
import numpy as np
# from sklearn.preprocessing import StandardScaler
from multiprocessing import Pool, cpu_count
from typing import List, Any
import orbit
from orbit.models import KTR 
from functools import partial
import fileUtils
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
import matplotlib.pyplot as plt
from datetime import datetime
import itertools 

# Function to calculate RMSE
def rmse(y_true, y_pred):
    return np.sqrt(mean_squared_error(y_true, y_pred))

if __name__ == "__main__":
    
    # Read input data
    input_df = fileUtils.read_csv('aq_hyd_cleaned_data')
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['Date'] = pd.to_datetime(input_df['Date'])
    input_df.rename(columns={'Date':'ds','AQI':'y'}, inplace=True)
    input_df['test_indicator'] = 0
    input_df.loc[input_df['ds'].dt.year == 2020, 'test_indicator'] = 1
    train_df = input_df[input_df['test_indicator'] == 0]
    test_df = input_df[input_df['test_indicator'] == 1]

    param_grid = {
    'seasonality': [[365.25], [7, 365.25]],  # Yearly and weekly seasonality
    'estimator': ['pyro-svi'],
    'num_steps': [50, 100],  # Only applicable if using 'pyro-svi'
    'learning_rate': [0.01, 0.1]  # Only applicable if using 'pyro-svi'
    }

    
    column_names = ['PM2.5', 'PM10']

    # Best parameters tracker
    best_rmse = float('inf')
    best_params = {}

    # Iterate over all combinations of parameters
    for params in itertools.product(*param_grid.values()):
        param_dict = dict(zip(param_grid.keys(), params))
        
        # Configure the model with the current set of parameters
        try:
            model = KTR(
                response_col='y',
                date_col='ds',
                seasonality=param_dict.get('seasonality', [365.25]),
                estimator=param_dict.get('estimator', 'pyro-svi'),
                regressor_col=column_names,
                prediction_percentiles=[5, 95],
                regressor_sign=['+', '+'],
                seed=2000,
                num_steps=param_dict.get('num_steps', 100),  # default if not pyro-svi
                learning_rate=param_dict.get('learning_rate', 0.1)  # default if not pyro-svi
            )
            
            # Fit the model
            model.fit(train_df)
            forecast = model.predict(test_df)
            predictions = forecast['prediction']

            # Evaluate the model
            current_rmse = rmse(test_df['y'], predictions)
            print(f"Testing with params: {param_dict}, RMSE: {current_rmse}")

            # Check if this is the best model so far
            if current_rmse < best_rmse:
                best_rmse = current_rmse
                best_params = param_dict
        except Exception as e:
            print(f"Error with parameters {param_dict}: {e}")

    print(f"Best RMSE: {best_rmse} with parameters {best_params}")


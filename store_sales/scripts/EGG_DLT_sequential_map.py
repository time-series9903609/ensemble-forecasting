import pandas as pd
import numpy as np
# from sklearn.preprocessing import StandardScaler
# from multiprocessing import Pool, cpu_count
from collections import Counter
from typing import List, Any
import orbit
from orbit.models import DLT 
from functools import partial
import fileUtils
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
import matplotlib.pyplot as plt
from datetime import datetime 


if __name__ == "__main__":
    input_df = fileUtils.read_csv('egg_one_store')
    # input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    # input_df = input_df.drop(columns=['id'])
    # print(input_df.head(5))
    # print(input_df.tail(5))
    # input_df['date'] = pd.to_datetime(input_df['date'])
    # min_date = input_df['date'].min()
    # max_date = input_df['date'].max()
    # expected_dates = pd.date_range(start=min_date, end=max_date)
    # missing_dates = expected_dates[~expected_dates.isin(input_df['date'])]
    # if len(missing_dates) == 0:
    #     print("The train dataset is complete. It includes all the required dates.")
    # else:
    #     print("The train dataset is incomplete. The following dates are missing:")
    #     print(missing_dates)
    #     missing_df = pd.DataFrame({'date':missing_dates})
    #     eggs1_dataframe = pd.concat([input_df, missing_df], ignore_index=True)
    #     missing_dates = expected_dates[~expected_dates.isin(eggs1_dataframe['date'])]
    #     if len(missing_dates) == 0:
    #         print("The train dataset is complete. It includes all the required dates.")
    # input_df.rename(columns={'date':'ds','sales':'y'}, inplace=True)
    # input_df = input_df.fillna(method='ffill')
    # input_df['test_indicator'] = 0
    input_df['ds'] = pd.to_datetime(input_df['ds'])
    input_df.loc[input_df['ds'].dt.year == 2017, 'test_indicator'] = 1
    input_df['TSId'] = 1

    input_df.drop_duplicates(subset=['ds'], keep='first', inplace=True)
    train_df = input_df[input_df['test_indicator']==0]
    test_df = input_df[input_df['test_indicator']==1]

    # Count the occurrences of each date
    date_counts = Counter(input_df['ds'])

    # Find duplicate dates
    duplicates = {date: count for date, count in date_counts.items() if count > 1}

    # Print the duplicate dates
    for date, count in duplicates.items():
        print(f"{date} is duplicated {count} times.")

    column_names = ['onpromotion', 'transactions','dcoilwtico']

    model = DLT(response_col='y', 
                date_col='ds', 
                seasonality=365,
                estimator = 'stan-map',
                global_trend_option='linear', 
                # seasonality_sm_input=1,
                damped_factor=0.85, 
                regressor_col=column_names)
    model.fit(train_df)
    forecast = model.predict(test_df)
    fitted_values = model.predict(train_df)
    forecast['TSId'] = test_df['TSId'].values
    forecast['actual_sales'] = test_df['y'].values

    print(forecast.head(5))
    

    # Calculate metrics
    lr_mse = mean_squared_error(test_df['y'].astype(float), forecast['prediction'])
    lr_mae = mean_absolute_error(test_df['y'].astype(float), forecast['prediction'])

    # Apply the absolute value function to both y_eval and lr_predictions
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(forecast['prediction'])

    # Calculate the Root Mean Squared Logarithmic Error (RMSLE)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))

    # Create a DataFrame to store results for Linear Regression
    results_lr = pd.DataFrame({'Model': ['Orbit Model'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]}).round(2)

    # Print the results_lr dataframe
    print(results_lr)

    plt.figure(figsize=(10, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual', color='blue')
    plt.plot(forecast['ds'], forecast['prediction'], label='Forecast', color='red')
    # plt.fill_between(forecast['ds'], forecast['prediction_5'], forecast['prediction_95'], color='gray', alpha=0.3)
    plt.title('Actual vs. Forecast Data')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()


import pandas as pd
import os
import numpy as np
import fileUtils
from prophet import Prophet
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from multiprocessing import Pool, cpu_count
from typing import List, Any
from functools import partial
from orbit.models import DLT
from sklearn.ensemble import AdaBoostRegressor

def fit_prophet_model(train_data, holiday_df):
    try:
        model = Prophet(interval_width=0.95,
                        growth='linear',
                        yearly_seasonality=False,
                        weekly_seasonality=False,
                        changepoint_range=0.95,
                        holidays=holiday_df,
                        holidays_mode='multiplicative',
                        seasonality_mode='additive',
                        seasonality_prior_scale=0.01)
        model.add_regressor('pm2_5')
        model.add_regressor('pm10')
        model.add_regressor('no')
        model.add_regressor('no2')
        model.add_regressor('nox')
        model.add_regressor('nh3')
        model.add_regressor('benzene')
        model.fit(train_data)
        return model
    except Exception as e:
        print(f"Error fitting Prophet model: {e}")
        return None

def fit_orbit_model(train_data):
    try:
        column_names = ['pm2_5', 'pm10', 'no', 'no2', 'nox', 'nh3', 'benzene','change_point_effect']
        model = DLT(response_col='y', 
                    date_col='ds', 
                    seasonality=730,
                    estimator='stan-map',
                    global_trend_option='linear', 
                    damped_factor=0.85, 
                    regressor_col=column_names)
        model.fit(train_data)
        return model
    except Exception as e:
        print(f"Error fitting Orbit model: {e}")
        return None

def predict_prophet(model, data):
    try:
        forecast = model.predict(data)
        return forecast
    except Exception as e:
        print(f"Error predicting Prophet forecast: {e} for TSId {data['tsid']}")
        return None

def predict_orbit(model, data):
    try:
        forecast = model.predict(data)
        return forecast
    except Exception as e:
        print(f"Error predicting Orbit forecast: {e}")
        return None

def fit_and_predict(input_df, holiday_df):
    train_data = input_df.loc[input_df['test_indicator'] == 0]
    test_data = input_df.loc[input_df['test_indicator'] == 1]
    
    prophet_model = fit_prophet_model(train_data, holiday_df)
    orbit_model = fit_orbit_model(train_data)
    
    if prophet_model and orbit_model:
        prophet_train_forecast = predict_prophet(prophet_model, train_data)
        prophet_test_forecast = predict_prophet(prophet_model, test_data)
        
        orbit_train_forecast = predict_orbit(orbit_model, train_data)
        orbit_test_forecast = predict_orbit(orbit_model, test_data)
        
        train_forecast = pd.DataFrame({
            'tsid': train_data['tsid'].values,
            'city': train_data['city'].values,
            'ds': train_data['ds'].values,
            'y': train_data['y'].values,
            'prophet_yhat': prophet_train_forecast['yhat'],
            'orbit_prediction': orbit_train_forecast['prediction']
        })
        
        test_forecast = pd.DataFrame({
            'tsid': test_data['tsid'].values,
            'city': test_data['city'].values,
            'ds': test_data['ds'].values,
            'y': test_data['y'].values,
            'prophet_yhat': prophet_test_forecast['yhat'],
            'orbit_prediction': orbit_test_forecast['prediction']
        })
        
        forecast = pd.concat([train_forecast, test_forecast])
        return forecast
    else:
        return None

def calculate_metrics(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    y_eval_abs = abs(test_value.astype(float))
    lr_predictions_abs = abs(forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)

    return results_lr

if __name__ == "__main__":
    input_df = fileUtils.read_csv('aq_major_cities_cleaned_data')
    holiday_df = fileUtils.read_csv('aqi_delhi_holidays')
    holiday_df['type'] = 'non_working_day'
    holiday_df['lower_window'] = 0
    holiday_df['upper_window'] = 1
    holiday_df.rename(columns={'type':'holiday','holiday':'ds'}, inplace=True)
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    change_points = pd.to_datetime(holiday_df['ds'].values)
    change_point_series = pd.Series(0, index=input_df.index)
    for cp in change_points:
        # Mark the period after each change point
        change_point_series.loc[input_df['ds'] >= cp] += 1
    input_df['change_point_effect'] = change_point_series

    series = [input_df.loc[input_df.tsid == x] for x in input_df.tsid.unique()]

    fit_and_predict_with_holidays = partial(fit_and_predict, holiday_df=holiday_df)

    p = Pool(cpu_count())        
    forecasts: List[Any] = list(p.imap(fit_and_predict_with_holidays, series))

    forecast_dataframe = pd.concat([f for f in forecasts if f is not None])
    forecast_dataframe = forecast_dataframe[['tsid', 'city', 'ds', 'y', 'prophet_yhat', 'orbit_prediction']]
    print(forecast_dataframe.head(10))
    print(forecast_dataframe.tail(10))
    print("Forecast data set shape is", forecast_dataframe.shape)

    train_df = forecast_dataframe[forecast_dataframe['ds'] < '2020-01-01']
    test_df = forecast_dataframe[forecast_dataframe['ds'] >= '2020-01-01']

    combined_forecast_train = train_df[['prophet_yhat', 'orbit_prediction']]
    combined_forecast_test = test_df[['prophet_yhat', 'orbit_prediction']]
    y_train = train_df['y']
    y_test = test_df['y']

    ada_boost_model = AdaBoostRegressor(n_estimators=50, learning_rate=1.0, random_state=42)
    ada_boost_model.fit(combined_forecast_train, y_train)

    final_predictions = ada_boost_model.predict(combined_forecast_test)
    final_predictions_df = pd.DataFrame({
        'ds': test_df['ds'].values,
        'adaboost_predictions': final_predictions
    })
    print(final_predictions_df.head(10))

    mse = mean_squared_error(y_test, final_predictions_df['adaboost_predictions'])
    rmse = np.sqrt(mse)
    print(f'Final RMSE of AdaBoost ensemble: {rmse}')

    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'aqi_multiprocessing_adaboost_forecasting.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    final_predictions_df.to_csv(file_path, index=False)

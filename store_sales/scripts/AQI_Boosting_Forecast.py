import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from prophet import Prophet
import orbit
from orbit.models import DLT 
import fileUtils 
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import xgboost as xgb
import os
from collections import Counter   

def fit_and_predict_prophet(train_df, test_df):
    model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    weekly_seasonality=True,
                    changepoint_range=0.95,
                    changepoint_prior_scale=0.1,
                    # holidays=holiday_df,
                    # holidays_mode='multiplicative',
                    seasonality_mode='additive',
                    seasonality_prior_scale=10.0)
    model.add_regressor('PM2.5')
    model.add_regressor('PM10')
    # model.add_regressor('dcoilwtico')
    model.fit(train_df)
    p_fitted = model.predict(train_df)
    p_forecast = model.predict(test_df)
    return p_fitted, p_forecast

def fit_and_predict_orbit(train_df, test_df):
    column_names = ['PM2.5', 'PM10']
    model = DLT(response_col='y', 
                date_col='ds', 
                seasonality=730,
                estimator='stan-map',
                global_trend_option='linear', 
                damped_factor=0.7, 
                regressor_col=column_names)
    model.fit(train_df)
    o_fitted = model.predict(train_df)
    o_forecast = model.predict(test_df)
    return o_fitted, o_forecast

def boosting_ensemble_with_xgboost(train_df, 
                                   p_fitted, 
                                   o_fitted, 
                                   test_df, 
                                   p_forecast, 
                                   o_forecast, 
                                   random_seed=42):
    # Initialize XGBoost models
    xgb_prophet = xgb.XGBRegressor(
                                n_estimators=170,     # More trees (default is 100)
                                learning_rate=0.1,    # Lower learning rate
                                max_depth=3,          # Deeper trees, careful tuning needed
                                min_child_weight=1,   # Minimum sum of instance weight (hessian) needed in a child
                                subsample=0.8,        # Subsample ratio of the training instances
                                colsample_bytree=0.8, # Subsample ratio of columns when constructing each tree
                                gamma=0,              # Minimum loss reduction required to make a further partition
                                reg_lambda=1,         # L2 regularization term on weights
                                reg_alpha=0,          # L1 regularization term on weights
                                seed=random_seed      # For reproducibility
                                )

    # xgb_prophet = xgb.XGBRegressor(seed=random_seed)
    xgb_orbit = xgb.XGBRegressor(
                                n_estimators=170,     # More trees (default is 100)
                                learning_rate=0.1,    # Lower learning rate
                                max_depth=3,          # Deeper trees, careful tuning needed
                                min_child_weight=1,   # Minimum sum of instance weight (hessian) needed in a child
                                subsample=0.8,        # Subsample ratio of the training instances
                                colsample_bytree=0.8, # Subsample ratio of columns when constructing each tree
                                gamma=0,              # Minimum loss reduction required to make a further partition
                                reg_lambda=1,         # L2 regularization term on weights
                                reg_alpha=0,          # L1 regularization term on weights
                                seed=random_seed      # For reproducibility
                                )
    # xgb_orbit = xgb.XGBRegressor(seed=random_seed)
    
    # Fit XGBoost models on the fitted (training) forecasts
    # Assuming 'yhat' from Prophet and 'prediction' from Orbit as features,
    # and 'y' from train_df as the target
    xgb_prophet.fit(p_fitted[['yhat']], train_df['y'])
    xgb_orbit.fit(o_fitted[['prediction']], train_df['y'])
    
    # Use the XGBoost models to refine the forecasts on the test set
    # Refining forecasts from Prophet
    p_forecast_refined = xgb_prophet.predict(p_forecast[['yhat']])
    p_forecast_refined = pd.DataFrame(p_forecast_refined, columns=['p_yhat'])

    # Refining forecasts from Orbit
    o_forecast_refined = xgb_orbit.predict(o_forecast[['prediction']])
    o_forecast_refined = pd.DataFrame(o_forecast_refined, columns=['o_prediction'])
    
    # Combine the refined forecasts to create an ensemble forecast
    # Here, we take the simple average of the refined forecasts


    # Prepare data for the regression model
    X_train = pd.DataFrame({'Prophet_Forecast': p_fitted['yhat'], 'Orbit_Forecast': o_fitted['prediction']})
    y_train = train_df['y'].astype(float)  # Use actual target values as the target variable

    # Train a linear regression model
    regression_model = LinearRegression()
    regression_model.fit(X_train, y_train)

    # Get the coefficients as weights
    weights = regression_model.coef_ 

    # Normalize weights (optional)
    weights_normalized = weights / np.sum(weights)
    print(weights_normalized)

     # Calculate weights for the weighted average ensemble
    weights = weights / np.sum(weights)

    # Before combining, convert to DataFrame to handle operations easily
    p_forecast_refined_df = pd.DataFrame(p_forecast_refined, columns=['p_yhat'])
    o_forecast_refined_df = pd.DataFrame(o_forecast_refined, columns=['o_prediction'])

    # Weighted average calculation might be misplaced, here's how to combine correctly:
    ensemble_forecast_values = (p_forecast_refined_df['p_yhat'] * weights[0] + o_forecast_refined_df['o_prediction'] * weights[1]).values

     # Weighted Average Ensemble
    weighted_ensemble_forecast = pd.DataFrame()
    weighted_ensemble_forecast['ds'] = test_df['ds']
    weighted_ensemble_forecast['prophet_forecast'] = p_forecast['yhat'].values
    weighted_ensemble_forecast['orbit_forecast'] = o_forecast['prediction'].values
    weighted_ensemble_forecast['ensemble_forecast'] = ensemble_forecast_values

    
    # Create a DataFrame to hold the ensemble forecast along with the actuals and original forecasts
    ensemble_forecast_df = pd.DataFrame({
        'ds': test_df['ds'].values,
        'actual_aqi': test_df['y'].values,
        'ensemble_forecast': ensemble_forecast_values,
        'prophet_prediction': p_forecast['yhat'].values,
        'orbit_prediction': o_forecast['prediction'].values
    })
    
    return ensemble_forecast_df

def calculate_metrics(test_df, ensemble_forecast, model_name):
    lr_mse = mean_squared_error(test_df['y'].astype(float), ensemble_forecast['ensemble_forecast'])
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(ensemble_forecast['ensemble_forecast'])
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    # print(results_lr)
    return np.sqrt(lr_mse)

def calculate_metrics_2(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    # print(results_lr)
    return np.sqrt(lr_mse)


if __name__ == "__main__":

    # Read input data
    input_df = fileUtils.read_csv('aq_hyd_cleaned_data')
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['Date'] = pd.to_datetime(input_df['Date'])
    input_df.rename(columns={'Date':'ds','AQI':'y'}, inplace=True)
    # train_df = input_df[input_df['ds'].dt.year.isin([2015, 2016, 2017, 2018,2019])]
    # test_df = input_df[input_df['ds'].dt.year == 2020]

    # Splitting the data into train and test
    input_df['test_indicator'] = 0
    input_df.loc[input_df['ds'].dt.year == 2020, 'test_indicator'] = 1
    train_df = input_df[input_df['test_indicator'] == 0]
    test_df = input_df[input_df['test_indicator'] == 1]

   # Fit and predict using Prophet model
    p_fitted, p_forecast = fit_and_predict_prophet(train_df, test_df)

    prophet_rmse = calculate_metrics_2(test_df['y'], p_forecast['yhat'], 'Prophet')

    # Fit and predict using Orbit model
    o_fitted, o_forecast = fit_and_predict_orbit(train_df, test_df)

    orbit_rmse = calculate_metrics_2(test_df['y'], o_forecast['prediction'],'Orbit')
    
    # Boosting Ensemble with XGBoost
    boosting_ensemble_forecast = boosting_ensemble_with_xgboost(train_df, 
                                                                p_fitted, 
                                                                o_fitted, 
                                                                test_df, 
                                                                p_forecast, 
                                                                o_forecast, random_seed=42)

    # Calculate metrics
    boosting_rmse = calculate_metrics_2(test_df['y'], boosting_ensemble_forecast['ensemble_forecast'], 'Boosting Average Model')

    rmse_results = pd.DataFrame({'Ensemble Models': ['RMSE'],
                                 'Prophet': [prophet_rmse],
                                 'Orbit': [orbit_rmse],
                                 'Boosting': [boosting_rmse]
                            }).round(2)
    
    print(rmse_results)

    forecasted_data = pd.DataFrame({'date': test_df['ds'].values,
                                    'AQI': test_df['y'].values,
                                    'Prophet': p_forecast['yhat'],
                                    'Orbit': o_forecast['prediction'],
                                    'Boosting': boosting_ensemble_forecast['ensemble_forecast'],}).round(2)

    print(forecasted_data.head(10))

    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'aqi_hyd_xgboosting_forecasting' + '.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    forecasted_data.to_csv(file_path, index=False)



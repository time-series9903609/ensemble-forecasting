import pandas as pd
import numpy as np
from collections import Counter
from orbit.models import DLT 
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import fileUtils

if __name__ == "__main__":
    # Read input data
    input_df = fileUtils.read_csv('aq_hyd_cleaned_data')
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['Date'] = pd.to_datetime(input_df['Date'])
    input_df.rename(columns={'Date':'ds','AQI':'y'}, inplace=True)
    # train_df = input_df[input_df['ds'].dt.year.isin([2015, 2016, 2017, 2018,2019])]
    # test_df = input_df[input_df['ds'].dt.year == 2020]

    # Splitting the data into train and test
    input_df['test_indicator'] = 0
    input_df.loc[input_df['ds'].dt.year == 2020, 'test_indicator'] = 1
    train_df = input_df[input_df['test_indicator'] == 0]
    test_df = input_df[input_df['test_indicator'] == 1]

    # Parameter grid for hyperparameter tuning
    damped_factors = [0.7, 0.8,0.85, 0.95]
    seasonality_values = [182, 365, 730]  # Assuming yearly and bi-yearly seasonality
    trend_options = ['linear', 'loglinear']

    best_rmse = float('inf')
    best_params = {}

    for damped_factor in damped_factors:
        for seasonality in seasonality_values:
            for trend_option in trend_options:
                model = DLT(response_col='y', 
                            date_col='ds', 
                            seasonality=seasonality,
                            estimator='stan-map',
                            global_trend_option=trend_option, 
                            damped_factor=damped_factor, 
                            regressor_col=['PM2.5', 'PM10'])
                model.fit(train_df)
                forecast = model.predict(test_df)
                
                # Calculate RMSE for the current set of parameters
                rmse = np.sqrt(mean_squared_error(test_df['y'], forecast['prediction']))

                if rmse < best_rmse:
                    best_rmse = rmse
                    best_params = {'damped_factor': damped_factor, 'seasonality': seasonality, 'trend_option': trend_option}

    # Output the best parameters and RMSE
    print("Best Parameters:", best_params)
    print("Lowest RMSE:", best_rmse)

    # Fit the best model
    model = DLT(response_col='y', 
                date_col='ds', 
                seasonality=best_params['seasonality'],
                estimator='stan-map',
                global_trend_option=best_params['trend_option'], 
                damped_factor=best_params['damped_factor'], 
                regressor_col=['PM2.5', 'PM10'])
    model.fit(train_df)
    forecast = model.predict(test_df)

    # Plot results
    plt.figure(figsize=(10, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual', color='blue')
    plt.plot(forecast['ds'], forecast['prediction'], label='Forecast', color='red')
    plt.title('Actual vs. Forecast Data')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()

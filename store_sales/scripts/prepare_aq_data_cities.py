import pandas as pd 
import numpy as np 
import fileUtils
import os
import seaborn as sns
import matplotlib.pyplot as plt


if __name__ == "__main__":

    input_df = fileUtils.read_csv('city_day')
    print(input_df.shape)
    print(input_df.head())
    print(input_df.isna().sum())
    input_df = input_df.fillna(method='ffill')
    input_df[['PM2.5','PM10','NH3','AQI','AQI_Bucket']] = input_df[['PM2.5','PM10','NH3','AQI','AQI_Bucket']].fillna(method='bfill')
    print(input_df.isna().sum())

    # Data analysis
    # cities_df = fileUtils.read_csv('aq_cities_cleaned_data')
    print(input_df['City'].unique())
    print("No of cities",len(input_df['City'].unique()))
    input_df['TSId'] = 1
    print(input_df.head(10))
    # series = [grouped_df.loc[grouped_df.store_nbr == x] for x in grouped_df.store_nbr.unique()]
    # series = [cities_df.loc[cities_df.City == x] for x in cities_df.City.unique()]

    # cities_df['TSId'] = range(1, len(cities_df) + 1)
    # print("Count of TSId in dataframe ",len(cities_df['TSId'].unique()))
    input_df['TSId'] = pd.factorize(input_df['City'])[0] + 1
    print("Count of TSId in dataframe ",len(input_df['TSId'].unique()))
    input_df.columns = [col.replace('.', '_').lower() for col in input_df.columns]
    print(input_df.head(10))
    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'aq_cities_cleaned_data' + '.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    input_df.to_csv(file_path, index=False)


    # counter = 0
    # for city in cities_df['City'].unique():        
    #     counter = counter+1
    #     if cities_df[cities_df['City']==city]: 
    #         cities_df['TSId'] = counter
    # print("Count of TSId in dataframe ",len(cities_df['TSId'].unique())) 


    # #correlation matrix
    # # Select only the numeric columns for correlation calculation
    # numeric_columns = ['PM2.5', 'PM10', 'NO', 'NO2', 'NOx', 'NH3', 'CO', 'SO2', 'O3', 'Benzene', 'Toluene', 'Xylene', 'AQI']
    # numeric_data = hyd_df[numeric_columns]

    # correlation_matrix = numeric_data.corr()
    # print(correlation_matrix)

    # plt.figure(figsize=(12, 8))
    # sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt=".2f", linewidths=0.5)
    # plt.title('Correlation Matrix')
    # plt.show()

    # hyd_df['Date'] = pd.to_datetime(hyd_df['Date'])
    # plt.plot(hyd_df['Date'], hyd_df['AQI'], label='AQI')
    # plt.xlabel('date')
    # plt.ylabel('AQI')
    # plt.show()

 


import pandas as pd 
import numpy as np 
import fileUtils
import os
import seaborn as sns
import matplotlib.pyplot as plt


if __name__ == "__main__":
    input_df = fileUtils.read_csv('city_day')
    print(input_df.shape)
    print(input_df.head())
    print(input_df.isna().sum())
    dataframe = input_df[input_df['City'].isin(['Ahmedabad', 'Bengaluru', 'Chennai', 'Delhi', 'Lucknow', 'Mumbai'])]
    print(dataframe['City'].unique())
    print(dataframe.shape)
    print(dataframe.isna().sum())
    dataframe = dataframe.fillna(method='ffill')
    dataframe[['PM2.5','PM10','NH3','AQI','AQI_Bucket']] = dataframe[['PM2.5','PM10','NH3','AQI','AQI_Bucket']].fillna(method='bfill')
    print(dataframe.isna().sum())
    dataframe.columns = dataframe.columns.str.lower().str.replace(r'[^a-z0-9]','_', regex=True)
    print(dataframe.head())
    dataframe['tsid'] = 1
    dataframe['tsid'] = pd.factorize(dataframe['city'])[0] + 1
    print("Count of TSId in dataframe ",dataframe['tsid'].unique())
 
    dataframe = dataframe.drop(dataframe.columns[dataframe.columns.str.contains('Unnamed', case=False)], axis=1)
    dataframe['date'] = pd.to_datetime(dataframe['date'])
    dataframe.rename(columns={'date':'ds','aqi':'y'}, inplace=True)
    dataframe['test_indicator'] = 0
    dataframe.loc[dataframe['ds'].dt.year == 2020, 'test_indicator'] = 1
    train_df = dataframe[dataframe['test_indicator'] == 0]
    test_df = dataframe[dataframe['test_indicator'] == 1]
    print("test dataframe size ",test_df.shape)

    print(dataframe['city'].unique())

    series = [dataframe.loc[dataframe.tsid == x] for x in dataframe.tsid.unique()]
    sizes = [df.shape[0] for df in series]
    print(sizes)
    sizes_with_tsid = {x: dataframe.loc[dataframe.tsid == x].shape[0] for x in dataframe.tsid.unique()}
    print(sizes_with_tsid)

    test_sizes_with_tsid = {x: test_df.loc[test_df.tsid == x].shape[0] for x in test_df.tsid.unique()}
    print("test dataframes size is \n",test_sizes_with_tsid)


    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'aq_major_cities_cleaned_data' + '.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    dataframe.to_csv(file_path, index=False)

    # #correlation matrix
    # # Select only the numeric columns for correlation calculation
    # numeric_columns = ['PM2.5', 'PM10', 'NO', 'NO2', 'NOx', 'NH3', 'CO', 'SO2', 'O3', 'Benzene', 'Toluene', 'Xylene', 'AQI']
    # numeric_data = hyd_df[numeric_columns]

    # correlation_matrix = numeric_data.corr()
    # print(correlation_matrix)

    # plt.figure(figsize=(12, 8))
    # sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt=".2f", linewidths=0.5)
    # plt.title('Correlation Matrix')
    # plt.show()

    # hyd_df['Date'] = pd.to_datetime(hyd_df['Date'])
    # plt.plot(hyd_df['Date'], hyd_df['AQI'], label='AQI')
    # plt.xlabel('date')
    # plt.ylabel('AQI')
    # plt.show()

 


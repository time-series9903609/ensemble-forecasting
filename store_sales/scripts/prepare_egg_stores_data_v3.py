import pandas as pd
import numpy as np
import os
import re
import fileUtils
from collections import Counter

def preprocess_data(train, test, holidays, stores, oil, transactions):
    """
    Preprocess and merge data from multiple sources.
    """
    train['test_indicator'] = 0
    test['test_indicator'] = 1

    data = pd.concat([train, test], axis=0)
    data = data.merge(holidays, on='date', how='left')
    data = data.merge(stores, on='store_nbr', how='left')
    data = data.merge(oil, on='date', how='left')
    data = data.merge(transactions, on=['date', 'store_nbr'], how='left')

    data['sales'] = data['sales'].ffill()
    data['dcoilwtico'] = data['dcoilwtico'].bfill()
    data['transactions'] = data['transactions'].fillna(data['transactions'].mean())

    data.drop(columns=['type_x', 'locale', 'locale_name', 'description', 'transferred'], inplace=True)
    data = data.ffill()

    return data

def process_eggs_data(data):
    """
    Process and filter data specifically for the 'eggs' family.
    """
    data['family'] = data['family'].apply(lambda x: x.lower() if isinstance(x, str) else x)
    data['family'] = data['family'].apply(lambda x: re.sub(r'[^a-zA-Z0-9.]+', '_', x))

    eggs_data = data[data['family'] == 'eggs']
    eggs_data['store_nbr_family'] = eggs_data['store_nbr'].astype(str) + '_' + eggs_data['family']
    eggs_data['date'] = pd.to_datetime(eggs_data['date'])
    
    unique_values = eggs_data['store_nbr_family'].unique()
    id_mapping = {value: index for index, value in enumerate(unique_values)}
    eggs_data['tsid'] = eggs_data['store_nbr_family'].map(id_mapping)
    
    eggs_data.drop(columns=['id', 'family', 'city', 'state', 'type_y', 'cluster'], inplace=True)
    eggs_data.rename(columns={'date': 'ds', 'sales': 'y'}, inplace=True)

    return eggs_data

def fill_missing_dates(df):
    """
    Ensure there are no missing dates in the DataFrame and fill any missing values appropriately.
    """
    df['ds'] = pd.to_datetime(df['ds'])
    min_date, max_date = df['ds'].min(), df['ds'].max()
    expected_dates = pd.date_range(start=min_date, end=max_date)
    missing_dates = expected_dates[~expected_dates.isin(df['ds'])]

    if len(missing_dates) > 0:
        missing_df = pd.DataFrame({'ds': missing_dates})
        df = pd.concat([df, missing_df], ignore_index=True)

    df = df.drop_duplicates(subset=['ds'], keep='first')
    df = df.sort_values(by='ds')
    df['y'] = df['y'].bfill().astype(int)
    df['dcoilwtico'] = df['dcoilwtico'].bfill().astype(int)
    df['transactions'] = df['transactions'].fillna(df['transactions'].mean()).astype(int)
    df['store_nbr'] = df['store_nbr'].fillna(df['store_nbr'].mode()[0]).astype(int)
    df['onpromotion'] = df['onpromotion'].bfill().astype(int)
    df['test_indicator'] = df['test_indicator'].fillna(df['test_indicator'].mode()[0]).astype(int)
    df['store_nbr_family'] = df['store_nbr_family'].fillna(df['store_nbr_family'].mode()[0])
    df['tsid'] = df['tsid'].fillna(df['tsid'].mode()[0]).astype(int)
    df['test_indicator'] = df['ds'].apply(lambda x: 0 if x.year < 2017 else 1)

    return df

if __name__ == "__main__":
    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    train = pd.read_csv(os.path.join(data_folder, "train.csv"))
    test = pd.read_csv(os.path.join(data_folder, "test.csv"))
    oil = pd.read_csv(os.path.join(data_folder, "oil.csv"))
    stores = pd.read_csv(os.path.join(data_folder, "stores.csv"))
    transactions = pd.read_csv(os.path.join(data_folder, "transactions.csv"))
    holidays = pd.read_csv(os.path.join(data_folder, "holidays_events.csv"))

    data = preprocess_data(train, test, holidays, stores, oil, transactions)
    eggs_data = process_eggs_data(data)
    eggs_data.to_csv(os.path.join(data_folder, "eggs_stores_preprocessed.csv"), index=False)

    egg_stores_df = fileUtils.read_csv('eggs_stores_preprocessed')

    unique_ids = egg_stores_df['tsid'].unique()
    eggs_df = pd.DataFrame()
    missing_dates_counter = Counter()

    for unique_id in unique_ids:
        tsid_df = egg_stores_df[egg_stores_df['tsid'] == unique_id]
        tsid_df = fill_missing_dates(tsid_df)
        missing_dates_counter.update(tsid_df['ds'][tsid_df['y'].isna()])

        print(tsid_df.isna().sum())
        print(tsid_df.info())
        eggs_df = pd.concat([eggs_df, tsid_df])

    print("Missing dates counter:")
    print(missing_dates_counter)
    
    print(eggs_df.head(5))
    print(eggs_df[eggs_df['tsid'] == 0].tail(5))
    print(eggs_df.shape)
    print(eggs_df.tail(5))
    print(eggs_df.columns)
    print(eggs_df.describe())
    print(eggs_df.info())
    eggs_df.to_csv(os.path.join(data_folder, "eggs_stores_processed.csv"), index=False)

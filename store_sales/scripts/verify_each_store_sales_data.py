import pandas as pd
import numpy as np
import datetime 
import os
import re


if __name__ == "__main__":

    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    store_1 =  pd.read_csv(os.path.join(data_folder, "egg_1_store.csv"))
    print("Shape of the dataframe store_1 is ",store_1.shape)
    print("Columns in dataframe are ",store_1.columns)
    duplicate_dates = store_1[store_1['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_1 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_1.isna().sum())

    store_2 =  pd.read_csv(os.path.join(data_folder, "egg_2_store.csv"))
    print("Shape of the dataframe store_2 is ",store_2.shape)
    print("Columns in dataframe are ",store_2.columns)
    duplicate_dates = store_2[store_2['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_2 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_2.isna().sum())

    store_3 =  pd.read_csv(os.path.join(data_folder, "egg_3_store.csv"))
    print("Shape of the dataframe store_3 is ",store_3.shape)
    print("Columns in dataframe are ",store_3.columns)
    duplicate_dates = store_3[store_3['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_3 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_3.isna().sum())

    store_4 =  pd.read_csv(os.path.join(data_folder, "egg_4_store.csv"))
    print("Shape of the dataframe store_4 is ",store_4.shape)
    print("Columns in dataframe are ",store_4.columns)
    duplicate_dates = store_4[store_4['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_4 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_4.isna().sum())

    store_5 =  pd.read_csv(os.path.join(data_folder, "egg_5_store.csv"))
    print("Shape of the dataframe store_5 is ",store_5.shape)
    print("Columns in dataframe are ",store_5.columns)
    duplicate_dates = store_5[store_5['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_5 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_5.isna().sum())

    store_6 =  pd.read_csv(os.path.join(data_folder, "egg_6_store.csv"))
    print("Shape of the dataframe store_6 is ",store_6.shape)
    print("Columns in dataframe are ",store_6.columns)
    duplicate_dates = store_6[store_6['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_6 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_6.isna().sum())

    store_7 =  pd.read_csv(os.path.join(data_folder, "egg_7_store.csv"))
    print("Shape of the dataframe store_7 is ",store_7.shape)
    print("Columns in dataframe are ",store_7.columns)
    duplicate_dates = store_7[store_7['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_7 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_7.isna().sum())

    store_8 =  pd.read_csv(os.path.join(data_folder, "egg_8_store.csv"))
    print("Shape of the dataframe store_8 is ",store_8.shape)
    print("Columns in dataframe are ",store_8.columns)
    duplicate_dates = store_8[store_8['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_8 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_8.isna().sum())

    store_9 =  pd.read_csv(os.path.join(data_folder, "egg_9_store.csv"))
    print("Shape of the dataframe store_9 is ",store_9.shape)
    print("Columns in dataframe are ",store_9.columns)
    duplicate_dates = store_9[store_9['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_9 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_9.isna().sum())

    store_10 =  pd.read_csv(os.path.join(data_folder, "egg_10_store.csv"))
    print("Shape of the dataframe store_10 is ",store_10.shape)
    print("Columns in dataframe are ",store_10.columns)
    duplicate_dates = store_10[store_10['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_10 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_10.isna().sum())

    store_11 =  pd.read_csv(os.path.join(data_folder, "egg_11_store.csv"))
    print("Shape of the dataframe store_11 is ",store_11.shape)
    print("Columns in dataframe are ",store_11.columns)
    duplicate_dates = store_11[store_11['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_11 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_11.isna().sum())

    store_12 =  pd.read_csv(os.path.join(data_folder, "egg_12_store.csv"))
    print("Shape of the dataframe store_12 is ",store_12.shape)
    print("Columns in dataframe are ",store_12.columns)
    duplicate_dates = store_12[store_12['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_12 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_12.isna().sum())

    store_13 =  pd.read_csv(os.path.join(data_folder, "egg_13_store.csv"))
    print("Shape of the dataframe store_13 is ",store_13.shape)
    print("Columns in dataframe are ",store_13.columns)
    duplicate_dates = store_13[store_13['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_1 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_13.isna().sum())

    store_14 =  pd.read_csv(os.path.join(data_folder, "egg_14_store.csv"))
    print("Shape of the dataframe store_14 is ",store_14.shape)
    print("Columns in dataframe are ",store_14.columns)
    duplicate_dates = store_14[store_14['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_14 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_14.isna().sum())

    store_15 =  pd.read_csv(os.path.join(data_folder, "egg_15_store.csv"))
    print("Shape of the dataframe store_15 is ",store_15.shape)
    print("Columns in dataframe are ",store_15.columns)
    duplicate_dates = store_15[store_15['ds'].duplicated(keep=False)]
    print("Duplicate dates in store_15 are \n",duplicate_dates)
    print("Number of NaN values in each columns are \n",store_15.isna().sum())

    dfs = [store_1, store_2, store_3, store_4,store_5, store_6, store_7, store_8,store_9, store_10, store_11, store_12, store_13,store_14,store_15 ]

    results = pd.concat(dfs, axis=0)

    print(type(results))
    print(results.shape)

    results.to_csv(os.path.join(data_folder, "15stores_data.csv"))


    






    
    
    

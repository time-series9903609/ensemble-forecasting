import pandas as pd
import os 
import numpy as np
import fileUtils 
from sklearn.preprocessing import StandardScaler
from prophet import Prophet
import matplotlib.pyplot as plt  
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from multiprocessing import Pool, cpu_count
from typing import List, Any
from datetime import datetime
import orbit 
from orbit.models import DLT  

""" Input file for this model comes from prepare_aq_data_multiple_cities.py 
file where required cleaning is being performed. Only divide data into train & test data size as per the 
test_indicator column values 0 & 1"""

def fit_model(train_data):
    try:
        column_names = ['pm2_5', 'pm10', 'no', 'no2', 'nox', 'nh3', 'benzene','change_point_effect']
        
        model = DLT(response_col='y', 
                    date_col='ds', 
                    seasonality=730,
                    estimator='stan-map',
                    global_trend_option='linear', 
                    damped_factor=0.85, 
                    regressor_col=column_names
                    # obs_sigma=0.000001
                    )
        print("TSId values in this iteration",train_data['tsid'].unique())
        # print("Train",duplicate_mask)
        model.fit(train_data)
        return model
    except Exception as e:
        print(f"Error fitting model: {e}")
        return None

def predict_forecast(model, test_data):
    try:
        forecast = model.predict(test_data)
        forecast[['tsid','city']] = test_data[['tsid','city']].values
        forecast['y'] = test_data['y'].values
        return forecast
    except Exception as e:
        print(f"Error predicting forecast: {e}")
        return None

def fit_and_predict(df:pd.DataFrame):
    train_data = df.loc[df['test_indicator'] == 0]
    test_data = df.loc[df['test_indicator'] == 1]
    model = fit_model(train_data)
    if model:
        forecast = predict_forecast(model, test_data)
        # forecast['city','tsid','y'] = test_data[['city','tsid','y']].values
        return forecast
    else:
        return None
    
def calculate_metrics_2(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    y_eval_abs = abs(test_value.astype(float))
    lr_predictions_abs = abs(forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    return results_lr

if __name__ == "__main__":

   # Read input data
    input_df = fileUtils.read_csv('aq_major_cities_cleaned_data')
    holiday_df = fileUtils.read_csv('aqi_delhi_holidays')
   
    holiday_df['type'] = 'non_working_day'
    holiday_df['lower_window'] = 0
    holiday_df['upper_window'] = 1
    holiday_df.rename(columns={'type':'holiday','holiday':'ds'}, inplace=True)
    input_df['ds'] = pd.to_datetime(input_df['ds'])
    
    change_points = pd.to_datetime(holiday_df['ds'].values)
    change_point_series = pd.Series(0, index=input_df.index)
    for cp in change_points:
        # Mark the period after each change point
        change_point_series.loc[input_df['ds'] >= cp] += 1
    input_df['change_point_effect'] = change_point_series

    # Splitting the data into train and test
    # input_df['test_indicator'] = 0
    # input_df.loc[input_df['ds'].dt.year == 2020, 'test_indicator'] = 1
    # train_df = input_df[input_df['test_indicator'] == 0]
    # test_df = input_df[input_df['test_indicator'] == 1]
    # train_df = input_df[input_df['test_indicator'] == 0]
    # test_df = input_df[input_df['test_indicator'] == 1]

    # print(input_df.columns)

    series = [input_df.loc[input_df.tsid == x] for x in input_df.tsid.unique()]

    p = Pool(cpu_count())        
    forecast: List[Any] = list(p.imap(fit_and_predict, series))

    forecast_dataframe = pd.concat(forecast)
    forecast_dataframe = forecast_dataframe[['tsid','city','ds','y','prediction']]
    print(forecast_dataframe.head(10))
    print("Forecast data set shape is",forecast_dataframe.shape)

    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'aqi_dlt_major_cities_holiday_forecasted_data' + '.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    forecast_dataframe.to_csv(file_path, index=False)

    rmse_results = calculate_metrics_2(forecast_dataframe['y'],forecast_dataframe['prediction'],'DLT Model')
    print(rmse_results)




import pandas as pd
import os
import numpy as np
from sklearn.metrics import mean_squared_error, mean_squared_log_error
from sklearn.base import BaseEstimator, RegressorMixin
from prophet import Prophet
import orbit
from orbit.models.dlt import DLT
import matplotlib.pyplot as plt
import xgboost as xgb
import fileUtils
from multiprocessing import Pool, cpu_count
from functools import partial
from typing import List, Any

# Custom estimator for Prophet
class ProphetEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols, holiday_df):
        self.model = None
        self.regressor_cols = regressor_cols
        self.holiday_df = holiday_df
        
    def fit(self, X, y):
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        df = X.copy()
        df['y'] = y
        self.model = Prophet(interval_width=0.95,
                             growth='linear',
                             yearly_seasonality=False,
                             weekly_seasonality=False,
                             changepoint_range=0.95,
                             changepoint_prior_scale=0.1,
                             holidays=self.holiday_df,
                             holidays_mode='multiplicative',
                             seasonality_mode='additive',
                             seasonality_prior_scale=0.01)
        for reg in self.regressor_cols:
            if reg != 'change_point_effect':
                self.model.add_regressor(reg)
        self.model.fit(df)
        return self
    
    def predict(self, X):
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        forecast = self.model.predict(X)
        return forecast['yhat'].values

# Custom estimator for Orbit's DLT
class DLTModelEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols):
        self.model = None
        self.regressor_cols = regressor_cols
        
    def fit(self, X, y):
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        df = X.copy()
        df['y'] = y
        self.model = DLT(response_col='y',
                         date_col='ds',
                         seasonality=730,
                         estimator='stan-map',
                         global_trend_option='linear',
                         damped_factor=0.7,
                         regressor_col=self.regressor_cols)
        self.model.fit(df)
        return self
    
    def predict(self, X):
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        forecast = self.model.predict(X)
        return forecast['prediction'].values

# Custom stacked estimator that combines Prophet and DLT
class StackedEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols, holiday_df):
        self.regressor_cols = regressor_cols
        self.holiday_df = holiday_df
        self.prophet_estimator = ProphetEstimator(regressor_cols, holiday_df)
        self.dlt_estimator = DLTModelEstimator(regressor_cols)
        
    def fit(self, X, y):
        self.prophet_estimator.fit(X, y)
        self.dlt_estimator.fit(X, y)
        return self
    
    def predict(self, X):
        prophet_pred = self.prophet_estimator.predict(X)
        dlt_pred = self.dlt_estimator.predict(X)
        return np.vstack((prophet_pred, dlt_pred)).T  # Combine predictions as features
    
    def get_params(self, deep=True):
        return {'regressor_cols': self.regressor_cols, 'holiday_df': self.holiday_df}
    
    def set_params(self, **params):
        self.regressor_cols = params.get('regressor_cols', self.regressor_cols)
        self.holiday_df = params.get('holiday_df', self.holiday_df)
        self.prophet_estimator = ProphetEstimator(self.regressor_cols, self.holiday_df)
        self.dlt_estimator = DLTModelEstimator(self.regressor_cols)
        return self

# Function to calculate RMSE
def rmse(y_true, y_pred):
    return np.sqrt(mean_squared_error(y_true, y_pred))

def calculate_metrics(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    y_eval_abs = abs(test_value.astype(float))
    lr_predictions_abs = abs(forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    return results_lr

def fit_and_predict(input_df, holiday_df):
    train_data = input_df.loc[input_df['test_indicator'] == 0]
    test_data = input_df.loc[input_df['test_indicator'] == 1]
    
    prophet_model = ProphetEstimator(regressor_cols, holiday_df).fit(train_data, train_data['y'])
    dlt_model = DLTModelEstimator(regressor_cols).fit(train_data, train_data['y'])
    
    prophet_train_forecast = prophet_model.predict(train_data)
    prophet_test_forecast = prophet_model.predict(test_data)
    
    dlt_train_forecast = dlt_model.predict(train_data)
    dlt_test_forecast = dlt_model.predict(test_data)
    
    train_forecast = pd.DataFrame({
        'tsid': train_data['tsid'].values,
        'city': train_data['city'].values,
        'ds': train_data['ds'].values,
        'y': train_data['y'].values,
        'prophet_yhat': prophet_train_forecast,
        'orbit_prediction': dlt_train_forecast
    })
    
    test_forecast = pd.DataFrame({
        'tsid': test_data['tsid'].values,
        'city': test_data['city'].values,
        'ds': test_data['ds'].values,
        'y': test_data['y'].values,
        'prophet_yhat': prophet_test_forecast,
        'orbit_prediction': dlt_test_forecast
    })
    
    forecast = pd.concat([train_forecast, test_forecast])
    return forecast

if __name__ == "__main__":

    input_df = fileUtils.read_csv('aq_major_cities_cleaned_data')
    holiday_df = fileUtils.read_csv('aqi_delhi_holidays')
    holiday_df['type'] = 'non_working_day'
    holiday_df['lower_window'] = 0
    holiday_df['upper_window'] = 1
    holiday_df.rename(columns={'type':'holiday','holiday':'ds'}, inplace=True)
    print(holiday_df.head(5))

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    change_points = pd.to_datetime(holiday_df['ds'].values)
    change_point_series = pd.Series(0, index=input_df.index)
    for cp in change_points:
        change_point_series.loc[input_df['ds'] >= cp] += 1
    input_df['change_point_effect'] = change_point_series

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    series = [input_df.loc[input_df.tsid == x] for x in input_df.tsid.unique()]

    regressor_cols = ['pm2_5', 'pm10', 'no', 'no2', 'nox', 'nh3', 'benzene', 'change_point_effect']
    fit_and_predict_with_holidays = partial(fit_and_predict, holiday_df=holiday_df)

    p = Pool(cpu_count())
    forecasts: List[Any] = list(p.imap(fit_and_predict_with_holidays, series))

    forecast_dataframe = pd.concat([f for f in forecasts if f is not None])
    forecast_dataframe = forecast_dataframe[['tsid', 'city', 'ds', 'y', 'prophet_yhat', 'orbit_prediction']]
    print(forecast_dataframe.head(10))
    print(forecast_dataframe.tail(10))
    print("Forecast data set shape is", forecast_dataframe.shape)

    train_df = forecast_dataframe[forecast_dataframe['ds'] < '2020-01-01']
    test_df = forecast_dataframe[forecast_dataframe['ds'] >= '2020-01-01']

    combined_forecast_train = train_df[['prophet_yhat', 'orbit_prediction']]
    combined_forecast_test = test_df[['prophet_yhat', 'orbit_prediction']]
    y_train = train_df['y']
    y_test = test_df['y']

    # Train the meta-model (XGBoost)
    meta_model = xgb.XGBRegressor(n_estimators=170, learning_rate=0.05, gamma=0, reg_lambda=1, reg_alpha=0,
                                  min_child_weight=1, subsample=0.8, colsample_bytree=0.8, max_depth=3, seed=42)
    meta_model.fit(combined_forecast_train, y_train)

    # Predict using the meta-model
    final_predictions = meta_model.predict(combined_forecast_test)
    final_predictions_df = pd.DataFrame({
        'ds': test_df['ds'].values,
        'xgboost_predictions': final_predictions
    })
    print(final_predictions_df.head(10))

    mse = mean_squared_error(y_test, final_predictions_df['xgboost_predictions'])
    rmse = np.sqrt(mse)
    print(f'Final RMSE of XGBoost ensemble: {rmse}')

    plt.figure(figsize=(12, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual')
    plt.plot(test_df['ds'], final_predictions, label='Ensemble Prediction')
    plt.xlabel('Date')
    plt.ylabel('AQI')
    plt.legend()
    plt.show()

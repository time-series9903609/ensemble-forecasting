import pandas as pd 
import numpy as np 
import fileUtils
import os
import seaborn as sns
import matplotlib.pyplot as plt
import os
import re


if __name__ == "__main__":
    
    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    train =pd.read_csv(os.path.join(data_folder, "train.csv"))
    test=pd.read_csv(os.path.join(data_folder, "test.csv"))
    oil=pd.read_csv(os.path.join(data_folder, "oil.csv"))
    stores=pd.read_csv(os.path.join(data_folder, "stores.csv"))
    transactions=pd.read_csv(os.path.join(data_folder, "transactions.csv"))
    holidays=pd.read_csv(os.path.join(data_folder, "holidays_events.csv"))

    train['test_indicator'] = 0
    test['test_indicator'] = 1

    data = pd.concat([train, test], axis=0)
    print(type(data))
    print("Data size is", data.shape)

    data = data.merge(holidays, on='date', how='left')
    print("Data size is", data.shape)
    data = data.merge(stores, on='store_nbr', how='left')
    print("Data size is", data.shape)
    data = data.merge(oil, on='date', how='left')
    print("Data size is", data.shape)
    data = data.merge(transactions, on=['date', 'store_nbr'], how='left')
    print("Data size is", data.shape)

    data['sales'] = data['sales'].ffill()
    data['dcoilwtico'] = data['dcoilwtico'].bfill()
    data['transactions'] = data['transactions'].fillna(data['transactions'].mean())
    data.drop(columns={'type_x','locale','locale_name','description','transferred'}, inplace=True)
    print(data.isna().sum())

    data['date'] = pd.to_datetime(data['date'])
    min_date = data['date'].min()
    max_date = data['date'].max()
    expected_dates = pd.date_range(start=min_date, end=max_date)
    missing_dates = expected_dates[~expected_dates.isin(data['date'])]
    if len(missing_dates) == 0:
        print("The train dataset is complete. It includes all the required dates.")
    else:
        print("The train dataset is incomplete. The following dates are missing:")
        print(missing_dates)
    missing_df = pd.DataFrame({'date':missing_dates})
    dataframe = pd.concat([data, missing_df], ignore_index=True)
    dataframe.sort_values('date', inplace=True)

    categories = ['AUTOMOTIVE', 'CELEBRATION', 'BREAD/BAKERY', 'BOOKS', 'BEVERAGES', 'BEAUTY',
              'BABY CARE', 'SEAFOOD', 'SCHOOL AND OFFICE SUPPLIES', 'PRODUCE',
              'PREPARED FOODS', 'POULTRY', 'PLAYERS AND ELECTRONICS', 'PET SUPPLIES',
              'PERSONAL CARE', 'MEATS', 'MAGAZINES', 'LIQUOR,WINE,BEER', 'LINGERIE',
              'LAWN AND GARDEN', 'LADIESWEAR', 'HOME CARE', 'HOME APPLIANCES', 'CLEANING',
              'DAIRY', 'DELI', 'EGGS', 'HOME AND KITCHEN II', 'HOME AND KITCHEN I',
              'HARDWARE', 'GROCERY II', 'GROCERY I', 'FROZEN FOODS']
    d_df = dataframe[~dataframe['family'].isin(categories)]
    # print(d_df.shape)
    dataframe = dataframe.ffill()
    d_df = dataframe[~dataframe['family'].isin(categories)]
    # print(d_df.shape)
    # print(d_df.head(20))
    dataframe['family'] = dataframe['family'].apply(lambda x: x.lower() if isinstance(x, str) else x)
    dataframe['family'] = dataframe['family'].apply(lambda x: re.sub(r'[^a-zA-Z0-9]+', '_', x))

    # Aggregate dataset
    agg_dataframe_df = dataframe.groupby('date').agg({'sales':'sum',
                                                      'onpromotion':'sum',
                                                      'dcoilwtico':'mean',
                                                      'transactions':'sum',
                                                      'test_indicator':'first'}).reset_index()
        
    agg_dataframe_df['date'] = pd.to_datetime(agg_dataframe_df['date'])
    min_date = agg_dataframe_df['date'].min()
    max_date = agg_dataframe_df['date'].max()
    expected_dates = pd.date_range(start=min_date, end=max_date)
    missing_dates = expected_dates[~expected_dates.isin(agg_dataframe_df['date'])]
    if len(missing_dates) == 0:
        print("The train dataset is complete. It includes all the required dates.")
    else:
        print("The train dataset is incomplete. The following dates are missing:")
        print(missing_dates)
        missing_df = pd.DataFrame({'date':missing_dates})
        agg_dataframe_df = pd.concat([agg_dataframe_df, missing_df], ignore_index=True)
    agg_dataframe_df.ffill()
    agg_dataframe_df.sort_values('date', inplace=True)
    print(agg_dataframe_df.isna().sum())
    agg_dataframe_df['TSId'] = 0
    agg_dataframe_df.rename(columns={'date':'ds', 'sales':'y'}, inplace=True)
    agg_dataframe_df.to_csv(os.path.join(data_folder, "agg_store_sales.csv"))

from sklearn.base import BaseEstimator, RegressorMixin
from prophet import Prophet
import orbit
from orbit.models import DLT
from sklearn.ensemble import BaggingRegressor, StackingRegressor
from sklearn.linear_model import LinearRegression
import pandas as pd
import fileUtils
import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils import resample
from collections import Counter   

def fit_and_predict_prophet(train_df, test_df):
    model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    weekly_seasonality=True,
                    changepoint_range=0.95,
                    changepoint_prior_scale=0.1,
                    # holidays=holiday_df,
                    # holidays_mode='multiplicative',
                    seasonality_mode='additive',
                    seasonality_prior_scale=10.0)
    model.add_regressor('PM2.5')
    model.add_regressor('PM10')
    # model.add_regressor('dcoilwtico')
    model.fit(train_df)
    p_fitted = model.predict(train_df)
    p_forecast = model.predict(test_df)
    return p_fitted, p_forecast

def fit_and_predict_orbit(train_df, test_df):
    column_names = ['PM2.5', 'PM10']
    model = DLT(response_col='y', 
                date_col='ds', 
                seasonality=730,
                estimator='stan-map',
                global_trend_option='linear', 
                damped_factor=0.7, 
                regressor_col=column_names)
    model.fit(train_df)
    o_fitted = model.predict(train_df)
    o_forecast = model.predict(test_df)
    return o_fitted, o_forecast

def bagging_ensemble(train_df, test_df, n_estimators=10):
    # Store all predictions
    prophet_predictions = []
    orbit_predictions = []

    # Create multiple bootstrap samples and fit both models
    for _ in range(n_estimators):

        # Resample the training data
        bootstrap_sample = resample(train_df, replace=True, n_samples=len(train_df), random_state=None)

        # Fit and predict using Prophet
        _, p_forecast = fit_and_predict_prophet(bootstrap_sample, test_df)
        prophet_predictions.append(p_forecast['yhat'].values)  # Assume yhat is the forecast column for Prophet

        # Fit and predict using Orbit
        _, o_forecast = fit_and_predict_orbit(bootstrap_sample, test_df)
        orbit_predictions.append(o_forecast['prediction'].values)  # Assume prediction is the forecast column for Orbit

    # Average predictions across all bootstrap samples
    avg_prophet_predictions = np.mean(prophet_predictions, axis=0)
    avg_orbit_predictions = np.mean(orbit_predictions, axis=0)

    return avg_prophet_predictions, avg_orbit_predictions


if __name__ == "__main__":
    # Read input data
    input_df = fileUtils.read_csv('aq_hyd_cleaned_data')
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['Date'] = pd.to_datetime(input_df['Date'])
    input_df.rename(columns={'Date':'ds','AQI':'y'}, inplace=True)
    # train_df = input_df[input_df['ds'].dt.year.isin([2015, 2016, 2017, 2018,2019])]
    # test_df = input_df[input_df['ds'].dt.year == 2020]

    # Splitting the data into train and test
    input_df['test_indicator'] = 0
    input_df.loc[input_df['ds'].dt.year == 2020, 'test_indicator'] = 1
    train_df = input_df[input_df['test_indicator'] == 0]
    test_df = input_df[input_df['test_indicator'] == 1]

    # Apply bagging ensemble
    prophet_bagging, orbit_bagging = bagging_ensemble(train_df, test_df, n_estimators=10)

    # Visualization
    plt.figure(figsize=(14, 7))
    plt.plot(test_df['ds'], test_df['y'], label='Actual')
    plt.plot(test_df['ds'], prophet_bagging, label='Prophet Bagging Predictions')
    plt.plot(test_df['ds'], orbit_bagging, label='Orbit Bagging Predictions')
    plt.legend()
    plt.show()


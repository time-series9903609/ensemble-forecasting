import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.metrics import mean_squared_error, mean_squared_log_error
from prophet import Prophet
from orbit.models import DLT
import fileUtils
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import os

def fit_and_predict_prophet(train_df, test_df, holiday_df):
    model = Prophet(interval_width=0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    weekly_seasonality=True,
                    changepoint_range=0.95,
                    changepoint_prior_scale=0.1,
                    holidays=holiday_df,
                    holidays_mode='additive',
                    seasonality_mode='additive',
                    seasonality_prior_scale=1.0)
    model.add_regressor('onpromotion')
    model.add_regressor('transactions')
    model.add_regressor('dcoilwtico')
    model.fit(train_df)
    p_fitted = model.predict(train_df)
    p_forecast = model.predict(test_df)
    return p_fitted, p_forecast

def fit_and_predict_orbit(train_df, test_df):
    column_names = ['onpromotion', 'transactions', 'dcoilwtico', 'change_point_effect']
    model = DLT(response_col='y',
                date_col='ds',
                seasonality=182,
                estimator='stan-map',
                global_trend_option='loglinear',
                damped_factor=0.7,
                regressor_col=column_names)
    model.fit(train_df)
    o_fitted = model.predict(train_df)
    o_forecast = model.predict(test_df)
    return o_fitted, o_forecast

def calculate_metrics_2(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(abs(test_value.astype(float)), abs(forecast_value)))
    results_lr = pd.DataFrame({'Model': [model_name],
                               'RMSLE': [lr_rmsle],
                               'RMSE': [np.sqrt(lr_mse)]
                              }).round(2)
    return np.sqrt(lr_mse)

if __name__ == "__main__":

    input_df = fileUtils.read_csv('egg_one_store')
    holiday_df = fileUtils.read_csv('eggs_store1_holidays')
    holiday_df['type'] = 'non_working_day'
    holiday_df['lower_window'] = 0
    holiday_df['upper_window'] = 1
    holiday_df.rename(columns={'type': 'holiday', 'holiday': 'ds'}, inplace=True)

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    input_df = input_df.sort_values(by='ds').drop_duplicates(subset=['ds'], keep='first')
    change_points = pd.to_datetime(holiday_df['ds'].values)
    change_point_series = pd.Series(0, index=input_df.index)
    for cp in change_points:
        change_point_series.loc[input_df['ds'] >= cp] += 1
    input_df['change_point_effect'] = change_point_series

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    train_df = input_df[input_df['test_indicator'] == 0]
    test_df = input_df[input_df['test_indicator'] == 1]

    # Fit and predict using Prophet model
    p_fitted, p_forecast = fit_and_predict_prophet(train_df, test_df, holiday_df)
    prophet_rmse = calculate_metrics_2(test_df['y'], p_forecast['yhat'], 'Prophet')

    # Fit and predict using Orbit model
    o_fitted, o_forecast = fit_and_predict_orbit(train_df, test_df)
    orbit_rmse = calculate_metrics_2(test_df['y'], o_forecast['prediction'], 'Orbit')

    # Prepare training data for meta-model
    meta_train = pd.DataFrame({
        'DLT': o_fitted['prediction'].values,
        'Prophet': p_fitted['yhat'].values,
        'Actual': train_df['y']})
    
    # Prepare test data for meta-model
    meta_test = pd.DataFrame({'DLT': o_forecast['prediction'], 'Prophet': p_forecast['yhat']})
    print("meta_test shape is ", meta_test.shape)

    # Train the meta-model
    meta_model = LinearRegression()
    meta_model.fit(meta_train[['DLT', 'Prophet']], meta_train['Actual'])

    # Final predictions
    final_predictions = meta_model.predict(meta_test)

    # Display or use these predictions
    test_df['final_predictions'] = final_predictions
    print(type(test_df['final_predictions']))
    print(test_df[['ds', 'final_predictions']])
    print(type(test_df[['ds', 'final_predictions']]))
    print(test_df['final_predictions'].shape)

    # Calculate metrics
    stacking_rmse = calculate_metrics_2(test_df['y'], test_df['final_predictions'], 'Stacking Model')

    rmse_results = pd.DataFrame({'Ensemble Models': ['RMSE'],
                                 'Prophet': [prophet_rmse],
                                 'Orbit': [orbit_rmse],
                                 'Stacking': [stacking_rmse]
                            }).round(2)
    
    print(rmse_results)

    forecasted_data = pd.DataFrame({'date': test_df['ds'].values,
                                    'Sales': test_df['y'].values,
                                    'Prophet': p_forecast['yhat'],
                                    'Orbit': o_forecast['prediction'],
                                    'Stacking': test_df['final_predictions'].values
                                    }).round(2)

    print(forecasted_data.head(10))

    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'egg_store1_stacking_LR_forecasting.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    forecasted_data.to_csv(file_path, index=False)

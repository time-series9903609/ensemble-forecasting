import pandas as pd
import os 
import numpy as np
import fileUtils 
from prophet import Prophet
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from multiprocessing import Pool, cpu_count
from typing import List, Any

def fit_model(train_data):
    try:        
        model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    weekly_seasonality=True,
                    changepoint_range=0.95,
                    # holidays=holiday_df,
                    # holidays_mode='additive',
                    seasonality_mode='additive')
        model.add_regressor('pm2_5')
        model.add_regressor('pm10')
        model.fit(train_data)
        return model
    except Exception as e:
        print(f"Error fitting model: {e}")
        return None

def predict_forecast(model, test_data):
    try:
        forecast = model.predict(test_data)
        return forecast
    except Exception as e:
        print(f"Error predicting forecast: {e} for TSId {test_data['tsid']}")
        return None

def fit_and_predict(args):
    df = args
    train_data = df.loc[df['test_indicator'] == 0]
    test_data = df.loc[df['test_indicator'] == 1]
    model = fit_model(train_data)
    if model:
        forecast = predict_forecast(model, test_data)
        forecast[['tsid','city']] = test_data[['tsid','city']].values
        forecast['y'] = test_data['y'].values
        return forecast
    else:
        return None
    
def calculate_metrics_2(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    y_eval_abs = abs(test_value.astype(float))
    lr_predictions_abs = abs(forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)

    return results_lr

if __name__ == "__main__":

    input_df = fileUtils.read_csv('aq_major_cities_cleaned_data')
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)


    series = [input_df.loc[input_df.tsid == x] for x in input_df.tsid.unique()]



    p = Pool(cpu_count())        
    forecast: List[Any] = list(p.imap(fit_and_predict, series))


    forecast_dataframe = pd.concat(forecast)
    forecast_dataframe = forecast_dataframe[['tsid','city','ds','y','yhat']]
    print(forecast_dataframe.head(10))
    print("Forecast data set shape is",forecast_dataframe.shape)

    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'aqi_prophet_major_cities_forecasted_data' + '.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    forecast_dataframe.to_csv(file_path, index=False)

    rmse_results = calculate_metrics_2(forecast_dataframe['y'],forecast_dataframe['yhat'],'DLT Model')
    print(rmse_results)




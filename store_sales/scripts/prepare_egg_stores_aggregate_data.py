import pandas as pd
import numpy as np
import datetime 
import os
import re
import fileUtils
from collections import Counter
from sklearn.preprocessing import StandardScaler


if __name__ == "__main__":

    input_df = fileUtils.read_csv('eggs_stores_sorted')

    print(input_df.shape)
    print(input_df.info())
    print(input_df.describe())
    input_df = input_df.rename(columns={'TSId':'tsid'})
    input_df = input_df.groupby('ds').agg({'y':'sum','onpromotion':'sum','dcoilwtico':'mean','transactions':'sum','tsid':'first','test_indicator':'first'}).reset_index()
    print(input_df.shape)
    print(input_df.head(5))
    # # Initialize scalers for each variable
    # sales_scaler = StandardScaler()
    # transactions_scaler = StandardScaler()
    # oilprice_scaler = StandardScaler()
    # promotions_scaler = StandardScaler()

    # # Fit and transform each variable separately
    # input_df['y'] = sales_scaler.fit_transform(input_df[['y']])
    # input_df['transactions'] = transactions_scaler.fit_transform(input_df[['transactions']])
    # input_df['dcoilwtico'] = oilprice_scaler.fit_transform(input_df[['dcoilwtico']])
    # input_df['onpromotion'] = promotions_scaler.fit_transform(input_df[['onpromotion']])
    # scaler = StandardScaler()
    # input_df[['y', 'transactions', 'dcoilwtico', 'onpromotion']] = scaler.fit_transform(input_df[['y', 'transactions', 'dcoilwtico', 'onpromotion']])
    # print(input_df['test_indicator'].unique())
    print(input_df.head(5))
    print(input_df.columns)
    print(input_df.isna().sum())
    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    input_df.to_csv(os.path.join(data_folder, "eggs_agg_data.csv"), index=False)
    
    
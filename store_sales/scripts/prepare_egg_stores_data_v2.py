import pandas as pd
import numpy as np
import datetime
import os
import re
import fileUtils
from collections import Counter
import logging

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def process_tsid_data(tsid_df):
    """
    Process data for a single TSId.
    :param tsid_df: DataFrame for a single TSId.
    :return: Processed DataFrame and missing dates counter.
    """
    tsid_df['ds'] = pd.to_datetime(tsid_df['ds'])
    min_date, max_date = tsid_df['ds'].min(), tsid_df['ds'].max()
    expected_dates = pd.date_range(start=min_date, end=max_date)
    missing_dates = expected_dates[~expected_dates.isin(tsid_df['ds'])]

    if missing_dates.empty:
        logging.info(f"The train dataset for TSId {tsid_df['TSId'].iloc[0]} is complete.")
    else:
        logging.warning(f"The train dataset for TSId {tsid_df['TSId'].iloc[0]} is incomplete. Missing dates: {missing_dates}")

    missing_df = pd.DataFrame({'ds': missing_dates})
    dataframe = pd.concat([tsid_df, missing_df], ignore_index=True).drop_duplicates(subset=['ds']).sort_values(by='ds')

    fill_columns = ['y', 'dcoilwtico', 'onpromotion']
    for col in fill_columns:
        dataframe[col] = dataframe[col].bfill().astype(int)

    dataframe['transactions'] = dataframe['transactions'].fillna(dataframe['transactions'].mean()).astype(int)
    dataframe['store_nbr'] = dataframe['store_nbr'].fillna(dataframe['store_nbr'].mode()[0]).astype(int)
    dataframe['test_indicator'] = dataframe['ds'].apply(lambda x: 0 if x.year < 2017 else 1)
    dataframe['store_nbr_family'] = dataframe['store_nbr_family'].fillna(dataframe['store_nbr_family'].mode()[0])
    dataframe['TSId'] = dataframe['TSId'].fillna(dataframe['TSId'].mode()[0]).astype(int)
    
    return dataframe, missing_dates

def main():
    """
    Main function to process egg stores data.
    """
    try:
        egg_stores_df = fileUtils.read_csv('eggs_stores')
    except FileNotFoundError:
        logging.error("The file 'eggs_stores' was not found.")
        return
    
    unique_ids = egg_stores_df['TSId'].unique()
    eggs_df = pd.DataFrame()
    missing_dates_counter = Counter()

    for unique_id in unique_ids:
        tsid_df = egg_stores_df[egg_stores_df['TSId'] == unique_id]
        processed_df, missing_dates = process_tsid_data(tsid_df)
        missing_dates_counter.update(missing_dates)
        logging.info(processed_df.isna().sum())
        logging.info(processed_df.info())
        eggs_df = pd.concat([eggs_df, processed_df])

    logging.info("Missing dates counter:")
    logging.info(missing_dates_counter)
    logging.info(eggs_df.head(5))
    logging.info(eggs_df[eggs_df['TSId'] == 0].tail(5))
    logging.info(eggs_df.shape)
    logging.info(eggs_df.tail(5))
    logging.info(eggs_df.columns)
    
    data_folder = "../data"
    os.makedirs(data_folder, exist_ok=True)
    eggs_df.to_csv(os.path.join(data_folder, "eggs_stores_sorted.csv"), index=False)

if __name__ == "__main__":
    main()

import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from prophet import Prophet
import orbit
from orbit.models import DLT 
import fileUtils 
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import xgboost as xgb
import os
from collections import Counter   

def fit_and_predict_prophet(train_df, test_df):
    model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    changepoint_range=0.95,
                    # holidays=holiday_df,
                    # holidays_mode='multiplicative',
                    seasonality_mode='additive')
    model.add_regressor('onpromotion')
    model.add_regressor('transactions')
    model.add_regressor('dcoilwtico')
    model.fit(train_df)
    p_fitted = model.predict(train_df)
    p_forecast = model.predict(test_df)
    return p_fitted, p_forecast

def fit_and_predict_orbit(train_df, test_df):
    column_names = ['onpromotion', 'transactions','dcoilwtico']
    model = DLT(response_col='y', 
                date_col='ds', 
                seasonality=365,
                estimator='stan-map',
                global_trend_option='linear', 
                damped_factor=0.85, 
                regressor_col=column_names)
    model.fit(train_df)
    o_fitted = model.predict(train_df)
    o_forecast = model.predict(test_df)
    return o_fitted, o_forecast

def simple_averaging_ensemble(test_df, p_forecast, o_forecast):
    test_df.reset_index(inplace=True)
    p_forecast.reset_index(inplace=True)
    o_forecast.reset_index(inplace=True)
    print("test dataframe shape",test_df.shape)
    print("p forecast dataframe shape",p_forecast.shape)
    print("o forecast dataframe shape",o_forecast.shape)
    ensemble_forecast = pd.DataFrame()
    ensemble_forecast['ds'] = test_df['ds'] 
    ensemble_forecast['actual_aqi'] = test_df['y'] 
    ensemble_forecast['prophet_forecast'] = p_forecast['yhat']
    ensemble_forecast['orbit_forecast'] = o_forecast['prediction']
    ensemble_forecast['ensemble_forecast'] = (p_forecast['yhat'] + o_forecast['prediction']) / 2
    # print(ensemble_forecast)
    return ensemble_forecast

def weighted_average_weights(train_df, p_fitted, o_fitted):
    # Prepare data for the regression model
    X_train = pd.DataFrame({'Prophet_Forecast': p_fitted['yhat'], 'Orbit_Forecast': o_fitted['prediction']})
    y_train = train_df['y'].astype(float)  # Use actual target values as the target variable

    # Train a linear regression model
    regression_model = LinearRegression()
    regression_model.fit(X_train, y_train)

    # Get the coefficients as weights
    weights = regression_model.coef_ 

    # Normalize weights (optional)
    weights_normalized = weights / np.sum(weights)
    print(weights_normalized)

    return weights_normalized

def stacking_ensemble_with_xgboost(train_df, p_fitted, o_fitted,test_df, p_forecast, o_forecast):
    # Merge forecasts with training data on 'ds'
    train_with_forecasts = pd.merge(train_df, p_fitted, on='ds', how='left') # fitted values and train_df merging
    train_with_forecasts = pd.merge(train_with_forecasts, o_fitted, on='ds', how='left') # fitted values and train_df merging

    # Prepare input features for the meta-model
    X_train = train_with_forecasts[['yhat', 'prediction']] # Prohet & orbit fitted_values --> p_fitted, o_fitted
    y_train = train_with_forecasts['y'] # --train_df 

    # Train the XGBoost model on the training data
    xgb_model = xgb.XGBRegressor()
    xgb_model.fit(X_train, y_train)

    # Merge forecasts with test data on 'ds'
    test_with_forecasts = pd.merge(test_df, p_forecast, on='ds', how='left')
    test_with_forecasts = pd.merge(test_with_forecasts, o_forecast, on='ds', how='left')

    # Prepare input features for the test data
    X_test = test_with_forecasts[['yhat', 'prediction']] # orbit & Prophet predictions --> p_forecast, o_forecast

    # Predict using the XGBoost model
    ensemble_forecast = xgb_model.predict(X_test)
    ensemble_forecast = pd.DataFrame(ensemble_forecast, columns=['ensemble_forecast'])
    ensemble_forecast['ds'] = test_df['ds'].values
    ensemble_forecast = ensemble_forecast[['ds', 'ensemble_forecast']]
    ensemble_forecast['actual_sales'] = test_df['y'].values
    ensemble_forecast = ensemble_forecast[['ds', 'actual_sales', 'ensemble_forecast']]
    ensemble_forecast[['prophet_prediction', 'orbit_prediction']] = test_with_forecasts[['yhat', 'prediction']].values

    return ensemble_forecast.round(2)

def boosting_ensemble_with_xgboost(train_df, p_fitted, o_fitted, test_df, p_forecast, o_forecast, random_seed=42):
    # Initialize XGBoost models
    xgb_prophet = xgb.XGBRegressor(seed=random_seed)
    xgb_orbit = xgb.XGBRegressor(seed=random_seed)
    
    # Fit XGBoost models on the fitted (training) forecasts
    # Assuming 'yhat' from Prophet and 'prediction' from Orbit as features,
    # and 'y' from train_df as the target
    xgb_prophet.fit(p_fitted[['yhat']], train_df['y'])
    xgb_orbit.fit(o_fitted[['prediction']], train_df['y'])
    
    # Use the XGBoost models to refine the forecasts on the test set
    # Refining forecasts from Prophet
    p_forecast_refined = xgb_prophet.predict(p_forecast[['yhat']])
    # Refining forecasts from Orbit
    o_forecast_refined = xgb_orbit.predict(o_forecast[['prediction']])
    
    # Combine the refined forecasts to create an ensemble forecast
    # Here, we take the simple average of the refined forecasts
    ensemble_forecast_values = (p_forecast_refined + o_forecast_refined) / 2
    
    # Create a DataFrame to hold the ensemble forecast along with the actuals and original forecasts
    ensemble_forecast_df = pd.DataFrame({
        'ds': test_df['ds'].values,
        'actual_sales': test_df['y'].values,
        'ensemble_forecast': ensemble_forecast_values,
        'prophet_prediction': p_forecast['yhat'].values,
        'orbit_prediction': o_forecast['prediction'].values
    })
    
    return ensemble_forecast_df

def bagging_ensemble_with_xgboost(train_df, p_fitted, o_fitted, test_df, p_forecast, o_forecast, random_seed=42, n_estimators=10):
    np.random.seed(random_seed)

    # Train XGBoost regressors for Prophet forecast
    xgb_prophet_models = []
    for _ in range(n_estimators):
        indices = np.random.choice(len(p_fitted), len(p_fitted), replace=True)
        xgb_prophet = xgb.XGBRegressor(random_state=random_seed)
        xgb_prophet.fit(p_fitted.iloc[indices][['yhat']], train_df.iloc[indices]['y'])
        xgb_prophet_models.append(xgb_prophet)

    # Train XGBoost regressors for Orbit forecast
    xgb_orbit_models = []
    for _ in range(n_estimators):
        indices = np.random.choice(len(o_fitted), len(o_fitted), replace=True)
        xgb_orbit = xgb.XGBRegressor(random_state=random_seed)
        xgb_orbit.fit(o_fitted.iloc[indices][['prediction']], train_df.iloc[indices]['y'])
        xgb_orbit_models.append(xgb_orbit)

    # Predict using XGBoost models
    p_predictions = np.mean([model.predict(p_forecast[['yhat']]) for model in xgb_prophet_models], axis=0)
    o_predictions = np.mean([model.predict(o_forecast[['prediction']]) for model in xgb_orbit_models], axis=0)

    # Combine predictions using weighted average
    # ensemble_forecast = (p_predictions * 0.4) + (o_predictions * 0.6)
    ensemble_forecast = (p_predictions + o_predictions) / 2

    ensemble_forecast = pd.DataFrame(ensemble_forecast, columns=['ensemble_forecast'])
    ensemble_forecast['ds'] = test_df['ds'].values
    ensemble_forecast = ensemble_forecast[['ds', 'ensemble_forecast']]
    ensemble_forecast['actual_sales'] = test_df['y'].values
    ensemble_forecast = ensemble_forecast[['ds', 'actual_sales', 'ensemble_forecast']]
    ensemble_forecast['prophet_prediction'] = p_forecast['yhat'].values
    ensemble_forecast['orbit_prediction'] = o_forecast['prediction'].values
    ensemble_forecast = ensemble_forecast.round(2)
    ensemble_forecast = ensemble_forecast.reset_index(drop=True)
    return ensemble_forecast

def calculate_metrics(test_df, ensemble_forecast, model_name):
    lr_mse = mean_squared_error(test_df['y'].astype(float), ensemble_forecast['ensemble_forecast'])
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(ensemble_forecast['ensemble_forecast'])
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    # print(results_lr)
    return np.sqrt(lr_mse)

def calculate_metrics_2(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    # print(results_lr)
    return np.sqrt(lr_mse)


if __name__ == "__main__":

    input_df = fileUtils.read_csv('egg_one_store')
    # print(input_df.shape)
    # input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    # input_df = input_df.drop(columns=['id'])
    # print(input_df.head(5))
    # print(input_df.tail(5))
    # input_df['date'] = pd.to_datetime(input_df['date'])
    # min_date = input_df['date'].min()
    # max_date = input_df['date'].max()
    # expected_dates = pd.date_range(start=min_date, end=max_date)
    # missing_dates = expected_dates[~expected_dates.isin(input_df['date'])]
    # if len(missing_dates) == 0:
    #     print("The train dataset is complete. It includes all the required dates.")
    # else:
    #     print("The train dataset is incomplete. The following dates are missing:")
    #     print(missing_dates)
    #     missing_df = pd.DataFrame({'date':missing_dates})
    #     eggs1_dataframe = pd.concat([input_df, missing_df], ignore_index=True)
    #     missing_dates = expected_dates[~expected_dates.isin(eggs1_dataframe['date'])]
    #     if len(missing_dates) == 0:
    #         print("The train dataset is complete. It includes all the required dates.")
    # input_df.rename(columns={'date':'ds','sales':'y'}, inplace=True)
    # input_df = input_df.fillna(method='ffill')
    # input_df['test_indicator'] = 0
    # input_df.loc[input_df['ds'].dt.year == 2017, 'test_indicator'] = 1
    # # print(input_df.isna().sum())
    # input_df['TSId'] = 1

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    input_df.drop_duplicates(subset=['ds'], keep='first', inplace=True)
    train_df = input_df[input_df['test_indicator']==0]
    test_df = input_df[input_df['test_indicator']==1]
    # test_df.drop(columns=['y'], inplace=True)

    # Count the occurrences of each date
    date_counts = Counter(input_df['ds'])

    # Find duplicate dates
    duplicates = {date: count for date, count in date_counts.items() if count > 1}

    # Print the duplicate dates
    for date, count in duplicates.items():
        print(f"{date} is duplicated {count} times.")

   # Fit and predict using Prophet model
    p_fitted, p_forecast = fit_and_predict_prophet(train_df, test_df)

    prophet_rmse = calculate_metrics_2(test_df['y'], p_forecast['yhat'], 'Prophet')

    # Fit and predict using Orbit model
    o_fitted, o_forecast = fit_and_predict_orbit(train_df, test_df)

    orbit_rmse = calculate_metrics_2(test_df['y'], o_forecast['prediction'],'Orbit')

    # Simple Averaging Ensemble
    simple_ensemble_forecast = simple_averaging_ensemble(test_df, p_forecast, o_forecast)

    # Calculate metrics
    sa_rmse = calculate_metrics_2(test_df['y'], simple_ensemble_forecast['ensemble_forecast'], 'Simple Average Model')

    # Calculate weights for the weighted average ensemble
    weights = weighted_average_weights(train_df, p_fitted, o_fitted)

     # Weighted Average Ensemble
    weighted_ensemble_forecast = pd.DataFrame()
    weighted_ensemble_forecast['ds'] = test_df['ds']
    weighted_ensemble_forecast['prophet_forecast'] = p_forecast['yhat'].values
    weighted_ensemble_forecast['orbit_forecast'] = o_forecast['prediction'].values
    weighted_ensemble_forecast['ensemble_forecast'] = (p_forecast['yhat'].values * weights[0]) + (o_forecast['prediction'].values * weights[1])

    # Calculate metrics
    wa_rmse = calculate_metrics_2(test_df['y'], weighted_ensemble_forecast['ensemble_forecast'], 'Weighted Average Model')


    # Stacking Ensemble with Linear Regression
    stacking_ensemble_forecast = stacking_ensemble_with_xgboost(train_df, 
                                                       p_fitted, 
                                                       o_fitted, 
                                                       test_df, 
                                                       p_forecast,
                                                       o_forecast)
    
    # Calculate metrics
    stacking_rmse = calculate_metrics_2(test_df['y'], stacking_ensemble_forecast['ensemble_forecast'], 'Stacking Average Model')
    
    # Boosting Ensemble with XGBoost
    boosting_ensemble_forecast = boosting_ensemble_with_xgboost(train_df, p_fitted, o_fitted, test_df, p_forecast, o_forecast, random_seed=42)

    # Calculate metrics
    boosting_rmse = calculate_metrics_2(test_df['y'], boosting_ensemble_forecast['ensemble_forecast'], 'Boosting Average Model')

    # Bagging Ensemble with XGBoost
    bagging_ensemble_forecast = bagging_ensemble_with_xgboost(train_df, p_fitted, o_fitted, test_df, p_forecast, o_forecast, random_seed=42)

    # Calculate metrics
    bagging_rmse = calculate_metrics_2(test_df['y'], bagging_ensemble_forecast['ensemble_forecast'], 'Bagging Average Model')

    rmse_results = pd.DataFrame({'Ensemble Models': ['RMSE'],
                                 'Prophet': [prophet_rmse],
                                 'Orbit': [orbit_rmse],
                                 'Simple Average': [sa_rmse],
                                 'Weighted Average': [wa_rmse],
                                 'Stacking': [stacking_rmse],
                                 'Boosting': [boosting_rmse],
                                 'Bagging': [bagging_rmse]
                            }).round(2)
    
    print(rmse_results)

    forecasted_data = pd.DataFrame({'date': test_df['ds'],
                                    'AQI': test_df['y'],
                                    'Prophet': p_forecast['yhat'],
                                    'Orbit': o_forecast['prediction'],
                                    'Simple_Average': simple_ensemble_forecast['ensemble_forecast'],
                                    'Weighted_Average': weighted_ensemble_forecast['ensemble_forecast'],
                                    'Stacking': stacking_ensemble_forecast['ensemble_forecast'],
                                    'Boosting': boosting_ensemble_forecast['ensemble_forecast'],
                                    'Bagging': bagging_ensemble_forecast['ensemble_forecast']}).round(2)

    print(forecasted_data.head(10))

    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'aq_cummulative_forecasting' + '.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    forecasted_data.to_csv(file_path, index=False)




    


    # # Plotting the test vs. forecast data
    # plt.figure(figsize=(10, 6))
    # plt.plot(test_df['ds'], test_df['y'], label='Actual', color='blue')
    # plt.plot(test_df['ds'], p_forecast['yhat'], label='Prophet', color='yellow')
    # plt.plot(test_df['ds'], o_forecast['prediction'], label='Orbit', color='green')
    # plt.plot(ensemble_forecast['ds'], ensemble_forecast['ensemble_forecast'], label='Forecast', color='red')
    # # plt.fill_between(forecast_df['ds'], forecast_df['yhat_lower'], forecast_df['yhat_upper'], color='gray', alpha=0.3)
    # plt.title('Actual vs. Forecast Data (Simple Average)')
    # plt.xlabel('Date')
    # plt.ylabel('Sales')
    # plt.legend()
    # plt.show()

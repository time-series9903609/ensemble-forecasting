import pandas as pd
import os
import numpy as np
from sklearn.metrics import mean_squared_error, mean_squared_log_error
from sklearn.base import BaseEstimator, RegressorMixin
from prophet import Prophet
import orbit
from orbit.models.dlt import DLT
import matplotlib.pyplot as plt
import xgboost as xgb
import fileUtils
from multiprocessing import Pool, cpu_count
from functools import partial
from typing import List, Any

# Custom estimator for Prophet
class ProphetEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols, holiday_df):
        self.model = None
        self.regressor_cols = regressor_cols
        self.holiday_df = holiday_df
        
    def fit(self, X, y):
        try:
            if isinstance(X, np.ndarray):
                X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
            df = X.copy()
            df['y'] = y
            self.model = Prophet(interval_width=0.95,
                                 growth='linear',
                                 yearly_seasonality=True,
                                 weekly_seasonality=True,
                                 changepoint_range=0.95,
                                 changepoint_prior_scale=0.1,
                                 holidays=self.holiday_df,
                                 holidays_mode='additive',
                                 seasonality_mode='additive',
                                 seasonality_prior_scale=1.01)
            for reg in self.regressor_cols:
                if reg != 'change_point_effect':
                    self.model.add_regressor(reg)
            self.model.fit(df)
        except Exception as e:
            print(f"Error fitting Prophet model: {e}")
        return self
    
    def predict(self, X):
        try:
            if isinstance(X, np.ndarray):
                X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
            forecast = self.model.predict(X)
            return forecast['yhat'].values
        except Exception as e:
            print(f"Error predicting with Prophet model: {e}")
            return np.array([])

# Custom estimator for Orbit's DLT
class DLTModelEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols):
        self.model = None
        self.regressor_cols = regressor_cols
        
    def fit(self, X, y):
        try:
            if isinstance(X, np.ndarray):
                X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
            df = X.copy()
            df['y'] = y
            self.model = DLT(response_col='y',
                             date_col='ds',
                             seasonality=182,
                             estimator='stan-map',
                             global_trend_option='loglinear',
                             damped_factor=0.7,
                             regressor_col=self.regressor_cols)
            self.model.fit(df)
        except Exception as e:
            print(f"Error fitting DLT model: {e}")
        return self
    
    def predict(self, X):
        try:
            if isinstance(X, np.ndarray):
                X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
            forecast = self.model.predict(X)
            return forecast['prediction'].values
        except Exception as e:
            print(f"Error predicting with DLT model: {e}")
            return np.array([])

def fit_and_predict(input_df, holiday_df):
    try:
        train_data = input_df.loc[input_df['test_indicator'] == 0]
        test_data = input_df.loc[input_df['test_indicator'] == 1]
        
        prophet_model = ProphetEstimator(regressor_cols, holiday_df).fit(train_data, train_data['y'])
        dlt_model = DLTModelEstimator(regressor_cols).fit(train_data, train_data['y'])
        
        prophet_train_forecast = prophet_model.predict(train_data)
        prophet_test_forecast = prophet_model.predict(test_data)
        
        dlt_train_forecast = dlt_model.predict(train_data)
        dlt_test_forecast = dlt_model.predict(test_data)
        
        train_forecast = pd.DataFrame({
            'tsid': train_data['TSId'].values,
            'store_nbr': train_data['store_nbr'].values,
            'ds': train_data['ds'].values,
            'y': train_data['y'].values,
            'prophet_yhat': prophet_train_forecast,
            'orbit_prediction': dlt_train_forecast
        })
        
        test_forecast = pd.DataFrame({
            'tsid': test_data['TSId'].values,
            'store_nbr': test_data['store_nbr'].values,
            'ds': test_data['ds'].values,
            'y': test_data['y'].values,
            'prophet_yhat': prophet_test_forecast,
            'orbit_prediction': dlt_test_forecast
        })
        
        forecast = pd.concat([train_forecast, test_forecast])
        return forecast
    except Exception as e:
        print(f"Error in fit_and_predict: {e}")
        return None

if __name__ == "__main__":
    try:
        input_df = fileUtils.read_csv('15stores_data')
        holiday_df = fileUtils.read_csv('eggs_store1_holidays')
        holiday_df['type'] = 'non_working_day'
        holiday_df['lower_window'] = 0
        holiday_df['upper_window'] = 1
        holiday_df.rename(columns={'type':'holiday','holiday':'ds'}, inplace=True)
        print(holiday_df.head(5))

        input_df['ds'] = pd.to_datetime(input_df['ds'])
        # input_df = input_df.sort_values(by='ds').drop_duplicates(subset=['ds'], keep='first')
        change_points = pd.to_datetime(holiday_df['ds'].values)
        change_point_series = pd.Series(0, index=input_df.index)
        for cp in change_points:
            change_point_series.loc[input_df['ds'] >= cp] += 1
        input_df['change_point_effect'] = change_point_series

        # input_df['ds'] = pd.to_datetime(input_df['ds'])
        series = [input_df.loc[input_df.TSId == x] for x in input_df.TSId.unique()]

        regressor_cols = ['onpromotion', 'dcoilwtico', 'transactions', 'change_point_effect']
        fit_and_predict_with_holidays = partial(fit_and_predict, holiday_df=holiday_df)

        p = Pool(cpu_count())
        forecasts: List[Any] = list(p.imap(fit_and_predict_with_holidays, series))

        forecast_dataframe = pd.concat([f for f in forecasts if f is not None])
        forecast_dataframe = forecast_dataframe[['tsid', 'store_nbr', 'ds', 'y', 'prophet_yhat', 'orbit_prediction']]
        print(forecast_dataframe.head(10))
        print(forecast_dataframe.tail(10))
        print("Forecast data set shape is", forecast_dataframe.shape)

        train_df = forecast_dataframe[forecast_dataframe['ds'] < '2017-01-01']
        test_df = forecast_dataframe[forecast_dataframe['ds'] >= '2017-01-01']

        combined_forecast_train = train_df[['prophet_yhat', 'orbit_prediction']]
        combined_forecast_test = test_df[['prophet_yhat', 'orbit_prediction']]
        y_train = train_df['y']
        y_test = test_df['y']

        # Boosting with XGBoost
        xgb_model = xgb.XGBRegressor(n_estimators=100, random_state=42)
        xgb_model.fit(combined_forecast_train, y_train)

        final_predictions = xgb_model.predict(combined_forecast_test)
        final_predictions_df = pd.DataFrame({
            'ds': test_df['ds'].values,
            'xgboost_predictions': final_predictions
        })
        print(final_predictions_df.head(10))
        print("Forecast data set shape is", final_predictions_df.shape)
        mse = mean_squared_error(y_test, final_predictions_df['xgboost_predictions'])
        rmse = np.sqrt(mse)
        print(f'Final RMSE of XGBoost Boosting: {rmse}')

        current_dir = os.path.dirname(os.path.abspath(__file__))
        data_folder = os.path.join(current_dir, '../data')
        filename_with_extension = 'eggs_multiprocessing_xgboost_boosting_forecasting.csv'
        file_path = os.path.join(data_folder, filename_with_extension)
        final_predictions_df.to_csv(file_path, index=False)
    except Exception as e:
        print(f"Error in main execution: {e}")

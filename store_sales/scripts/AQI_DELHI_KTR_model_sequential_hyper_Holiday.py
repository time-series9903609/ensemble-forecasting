import pandas as pd
import numpy as np
# from sklearn.preprocessing import StandardScaler
from multiprocessing import Pool, cpu_count
from typing import List, Any
import orbit
from orbit.models import KTR 
from functools import partial
import fileUtils
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
import matplotlib.pyplot as plt
from datetime import datetime
import itertools 

# Function to calculate RMSE
def rmse(y_true, y_pred):
    return np.sqrt(mean_squared_error(y_true, y_pred))

if __name__ == "__main__":
    
 # Read input data
    input_df = fileUtils.read_csv('aqi_delhi_cleaned_data')
    holiday_df = fileUtils.read_csv('aqi_delhi_holidays')
   
    holiday_df['type'] = 'non_working_day'
    holiday_df['lower_window'] = 0
    holiday_df['upper_window'] = 1
    holiday_df.rename(columns={'type':'holiday','holiday':'ds'}, inplace=True)
    input_df['ds'] = pd.to_datetime(input_df['ds'])

    change_points = pd.to_datetime(holiday_df['ds'].values)
    change_point_series = pd.Series(0, index=input_df.index)
    for cp in change_points:
        # Mark the period after each change point
        change_point_series.loc[input_df['ds'] >= cp] += 1
    input_df['level_knot_dates'] = change_point_series

    # Splitting the data into train and test
    input_df['test_indicator'] = 0
    input_df.loc[input_df['ds'].dt.year == 2020, 'test_indicator'] = 1
    train_df = input_df[input_df['test_indicator'] == 0]
    test_df = input_df[input_df['test_indicator'] == 1]


    param_grid = {
    'seasonality': [[365.25], [7, 365.25]],  # Yearly and weekly seasonality
    'estimator': ['pyro-svi'],
    'num_steps': [50, 100],  # Only applicable if using 'pyro-svi'
    'learning_rate': [0.01, 0.1]  # Only applicable if using 'pyro-svi'
    }

    
    column_names = ['pm2_5', 'pm10', 'no', 'no2', 'nox', 'nh3', 'benzene']

    # Best parameters tracker
    best_rmse = float('inf')
    best_params = {}

    # Iterate over all combinations of parameters
    for params in itertools.product(*param_grid.values()):
        param_dict = dict(zip(param_grid.keys(), params))
        
        # Configure the model with the current set of parameters
        try:
            model = KTR(
                response_col='y',
                date_col='ds',
                level_knot_dates = train_df['level_knot_dates'],
                seasonality=param_dict.get('seasonality', [365.25]),
                estimator=param_dict.get('estimator', 'pyro-svi'),
                regressor_col=column_names,
                prediction_percentiles=[5, 95],
                # regressor_sign=['+', '+'],
                seed=2000,
                # num_steps=param_dict.get('num_steps', 100),  # default if not pyro-svi
                learning_rate=param_dict.get('learning_rate', 0.1)  # default if not pyro-svi
            )
            
            # Fit the model
            model.fit(train_df)
            forecast = model.predict(test_df)
            predictions = forecast['prediction']

            # Evaluate the model
            current_rmse = rmse(test_df['y'], predictions)
            print(f"Testing with params: {param_dict}, RMSE: {current_rmse}")

            # Check if this is the best model so far
            if current_rmse < best_rmse:
                best_rmse = current_rmse
                best_params = param_dict
        except Exception as e:
            print(f"Error with parameters {param_dict}: {e}")

    print(f"Best RMSE: {best_rmse} with parameters {best_params}")


import pandas as pd
import numpy as np
import datetime 
import os
import re
import fileUtils
from collections import Counter


if __name__ == "__main__":

    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    train =pd.read_csv(os.path.join(data_folder, "train.csv"))
    test=pd.read_csv(os.path.join(data_folder, "test.csv"))
    oil=pd.read_csv(os.path.join(data_folder, "oil.csv"))
    stores=pd.read_csv(os.path.join(data_folder, "stores.csv"))
    transactions=pd.read_csv(os.path.join(data_folder, "transactions.csv"))
    holidays=pd.read_csv(os.path.join(data_folder, "holidays_events.csv"))

    print(holidays.head(5))

    train['test_indicator'] = 0
    test['test_indicator'] = 1

    data = pd.concat([train, test], axis=0)
    print(type(data))
    print("Data size is", data.shape)

    data = data.merge(holidays, on='date', how='left')
    print("Data size is", data.shape)
    data = data.merge(stores, on='store_nbr', how='left')
    print("Data size is", data.shape)
    data = data.merge(oil, on='date', how='left')
    print("Data size is", data.shape)
    data = data.merge(transactions, on=['date', 'store_nbr'], how='left')
    print("Data size is", data.shape)
    data['sales'] = data['sales'].ffill()
    data['dcoilwtico'] = data['dcoilwtico'].bfill()
    data['transactions'] = data['transactions'].fillna(data['transactions'].mean())
    data.drop(columns={'type_x','locale','locale_name','description','transferred'}, inplace=True)
    print(data.isna().sum())
    dataframe = data.copy()

    categories = ['AUTOMOTIVE', 'CELEBRATION', 'BREAD/BAKERY', 'BOOKS', 'BEVERAGES', 'BEAUTY',
              'BABY CARE', 'SEAFOOD', 'SCHOOL AND OFFICE SUPPLIES', 'PRODUCE',
              'PREPARED FOODS', 'POULTRY', 'PLAYERS AND ELECTRONICS', 'PET SUPPLIES',
              'PERSONAL CARE', 'MEATS', 'MAGAZINES', 'LIQUOR,WINE,BEER', 'LINGERIE',
              'LAWN AND GARDEN', 'LADIESWEAR', 'HOME CARE', 'HOME APPLIANCES', 'CLEANING',
              'DAIRY', 'DELI', 'EGGS', 'HOME AND KITCHEN II', 'HOME AND KITCHEN I',
              'HARDWARE', 'GROCERY II', 'GROCERY I', 'FROZEN FOODS']
    dataframe = dataframe.ffill()
    dataframe['family'] = dataframe['family'].apply(lambda x: x.lower() if isinstance(x, str) else x)
    dataframe['family'] = dataframe['family'].apply(lambda x: re.sub(r'[^a-zA-Z0-9.]+', '_', x))

    # # eggs dataframe
    eggs_dataframe = dataframe.copy()
    eggs_dataframe['store_nbr'] = eggs_dataframe['store_nbr'].astype(int)
    dataframe_eggs = eggs_dataframe[eggs_dataframe['family']=='eggs']
    dataframe_eggs['store_nbr_family'] = dataframe_eggs['store_nbr'].astype(str) + '_' + dataframe_eggs['family']
    print(dataframe_eggs.columns)
    dataframe_eggs['date'] = pd.to_datetime(dataframe_eggs['date'])
    unique_values = dataframe_eggs['store_nbr_family'].unique()
    id_mapping = {value: index for index, value in enumerate(unique_values)}
    dataframe_eggs['TSId'] = dataframe_eggs['store_nbr_family'].map(id_mapping)
    dataframe_eggs['test_indicator'] = 0
    print(dataframe_eggs.columns)
    dataframe_eggs.drop(columns=['id','family','city','state','type_y','cluster'], inplace=True)
    dataframe_eggs.rename(columns={'date':'ds', 'sales':'y'}, inplace=True)
    print(dataframe_eggs.columns)
    print(dataframe_eggs.head(5))
    dataframe_eggs.to_csv(os.path.join(data_folder, "eggs_stores.csv"), index=False)

    """ This is final data """
    egg_stores_df = fileUtils.read_csv('eggs_stores')

    unique_ids = egg_stores_df['TSId'].unique()
    eggs_df = pd.DataFrame()
    missing_dates_counter = Counter()

    for unique_id in unique_ids:
        tsid_df = egg_stores_df[egg_stores_df['TSId'] == unique_id]
        tsid_df['ds'] = pd.to_datetime(tsid_df['ds'])
        min_date = tsid_df['ds'].min()
        max_date = tsid_df['ds'].max()
        expected_dates = pd.date_range(start=min_date, end=max_date)
        missing_dates = expected_dates[~expected_dates.isin(tsid_df['ds'])]

        if len(missing_dates) == 0:
            print(f"The train dataset for TSId {unique_id} is complete. It includes all the required dates.")
        else:
            print(f"The train dataset for TSId {unique_id} is incomplete. The following dates are missing:")
            print(missing_dates)
            missing_dates_counter.update(missing_dates)

        missing_df = pd.DataFrame({'ds': missing_dates})
        dataframe = pd.concat([tsid_df, missing_df], ignore_index=True)
        dataframe = dataframe.drop_duplicates(subset=['ds'], keep='first')
        dataframe.sort_values(by='ds', inplace=True)
        dataframe['y'] = dataframe['y'].bfill().astype(int)
        dataframe['dcoilwtico'] = dataframe['dcoilwtico'].bfill().astype(int)
        dataframe['transactions'] = dataframe['transactions'].fillna(dataframe['transactions'].mean()).astype(int)
        dataframe['store_nbr'] = dataframe['store_nbr'].fillna(dataframe['store_nbr'].mode()[0]).astype(int)
        dataframe['onpromotion'] = dataframe['onpromotion'].bfill().astype(int)
        dataframe['test_indicator'] = dataframe['test_indicator'].fillna(dataframe['test_indicator'].mode()[0]).astype(int)
        dataframe['store_nbr_family'] = dataframe['store_nbr_family'].fillna(dataframe['store_nbr_family'].mode()[0])
        dataframe['TSId'] = dataframe['TSId'].fillna(dataframe['TSId'].mode()[0]).astype(int)
        dataframe['test_indicator'] = dataframe['ds'].apply(lambda x: 0 if x.year < 2017 else 1)
        print(dataframe.isna().sum())
        print(dataframe.info())
        eggs_df = pd.concat([eggs_df, dataframe])

    print("Missing dates counter:")
    print(missing_dates_counter)
    
    print(eggs_df.head(5))
    print(eggs_df[eggs_df['TSId']==0].tail(5))
    print(eggs_df.shape)
    print(eggs_df.tail(5))
    print(eggs_df.columns)
    eggs_df.to_csv(os.path.join(data_folder, "eggs_stores_sorted.csv"), index=False)
import pandas as pd

# Assuming `train_df` and `test_df` are already split and available
# Model 1: DLT
dlt_model = DLT(
    response_col='y', 
    date_col='ds', 
    seasonality=365,
    estimator='stan-map',
    global_trend_option='linear',
    damped_factor=0.85,
    regressor_col=column_names
)
dlt_model.fit(train_df)
dlt_predictions_train = dlt_model.predict(train_df)
dlt_predictions_test = dlt_model.predict(test_df)

# Model 2: Prophet
prophet_model = Prophet(
    interval_width=0.95,
    growth='linear',
    yearly_seasonality=True,
    weekly_seasonality=True,
    changepoint_range=0.95,
    changepoint_prior_scale=0.1,
    seasonality_mode='additive',
    seasonality_prior_scale=1.0
)
prophet_model.add_regressor('onpromotion')
prophet_model.add_regressor('transactions')
prophet_model.add_regressor('dcoilwtico')
prophet_model.fit(train_df)
prophet_predictions_train = prophet_model.predict(train_df)[['ds', 'yhat']]
prophet_predictions_test = prophet_model.predict(test_df)[['ds', 'yhat']]

# Prepare training data for meta-model
meta_train = pd.DataFrame({
    'DLT': dlt_predictions_train['prediction'],
    'Prophet': prophet_predictions_train['yhat'],
    'True': train_df['y']
})

# Prepare test data for meta-model
meta_test = pd.DataFrame({
    'DLT': dlt_predictions_test['prediction'],
    'Prophet': prophet_predictions_test['yhat']
})


from sklearn.linear_model import LinearRegression

# Train the meta-model
meta_model = LinearRegression()
meta_model.fit(meta_train[['DLT', 'Prophet']], meta_train['True'])


# Final predictions
final_predictions = meta_model.predict(meta_test)

# If you need to display or use these predictions:
test_df['final_predictions'] = final_predictions
print(test_df[['ds', 'final_predictions']])

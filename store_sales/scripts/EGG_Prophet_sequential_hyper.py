import pandas as pd
import numpy as np
from sklearn.model_selection import ParameterGrid
from prophet import Prophet
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from datetime import datetime
from statsmodels.tsa.seasonal import seasonal_decompose
import fileUtils

if __name__ == "__main__":
    # Load the data
    input_df = fileUtils.read_csv('egg_one_store')
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['ds'] = pd.to_datetime(input_df['ds'])
    input_df.loc[input_df['ds'].dt.year == 2017, 'test_indicator'] = 1
    train_df = input_df[input_df['test_indicator'] == 0]
    test_df = input_df[input_df['test_indicator'] == 1]

    # Define the grid of parameters to search
    param_grid = {
        'changepoint_prior_scale': [0.01, 0.1, 0.5],
        'seasonality_prior_scale': [0.1, 1.0, 10.0],
        'seasonality_mode': ['additive', 'multiplicative']
    }

    # Setup a dataframe to store the results of each configuration
    results = pd.DataFrame(columns=['params', 'RMSE'])

    # Loop over all combinations of parameters
    for params in ParameterGrid(param_grid):
        model = Prophet(
            interval_width=0.95,
            growth='linear',
            yearly_seasonality=True,
            weekly_seasonality=True,
            changepoint_range=0.95,
            seasonality_mode=params['seasonality_mode'],
            changepoint_prior_scale=params['changepoint_prior_scale'],
            seasonality_prior_scale=params['seasonality_prior_scale']
        )
        model.add_regressor('onpromotion')
        model.add_regressor('transactions')
        model.add_regressor('dcoilwtico')

        # Fit the model and make a forecast
        model.fit(train_df)
        forecast = model.predict(test_df)

        # Calculate RMSE for the current set of parameters
        rmse = np.sqrt(mean_squared_error(test_df['y'], forecast['yhat']))

        # Store the results
        new_row = pd.DataFrame({'params': [params], 'RMSE': [rmse]})
        results = pd.concat([results, new_row], ignore_index=True)

    # Find the best set of parameters
    best_params = results.loc[results['RMSE'].idxmin()]
    print("Best parameters found:", best_params['params'])
    print("Lowest RMSE achieved:", best_params['RMSE'])

    # Refit the model with the best parameters
    model = Prophet(
        interval_width=0.95,
        growth='linear',
        yearly_seasonality=True,
        weekly_seasonality=True,
        changepoint_range=0.95,
        seasonality_mode=best_params['params']['seasonality_mode'],
        changepoint_prior_scale=best_params['params']['changepoint_prior_scale'],
        seasonality_prior_scale=best_params['params']['seasonality_prior_scale']
    )
    model.add_regressor('onpromotion')
    model.add_regressor('transactions')
    model.add_regressor('dcoilwtico')

    model.fit(train_df)
    forecast_df = model.predict(test_df)

    # Calculate and print final metrics
    final_rmse = np.sqrt(mean_squared_error(test_df['y'], forecast_df['yhat']))
    print(f"Final RMSE with best parameters: {final_rmse}")

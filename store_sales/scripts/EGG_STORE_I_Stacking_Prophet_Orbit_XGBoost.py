import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from prophet import Prophet
import orbit
from orbit.models import DLT 
import fileUtils 
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import xgboost as xgb
import os
from collections import Counter   

def fit_and_predict_prophet(train_df, test_df):
    model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    weekly_seasonality=True,
                    changepoint_range=0.95,
                    changepoint_prior_scale=0.1,
                    # holidays=holiday_df,
                    # holidays_mode='multiplicative',
                    seasonality_mode='additive',
                    seasonality_prior_scale=1.0)
    model.add_regressor('onpromotion')
    model.add_regressor('transactions')
    model.add_regressor('dcoilwtico')
    model.fit(train_df)
    p_fitted = model.predict(train_df)
    p_forecast = model.predict(test_df)
    return p_fitted, p_forecast

def fit_and_predict_orbit(train_df, test_df):
    column_names = ['onpromotion', 'transactions','dcoilwtico']
    model = DLT(response_col='y', 
                date_col='ds', 
                seasonality=182,
                estimator='stan-map',
                global_trend_option='loglinear', 
                damped_factor=0.7, 
                regressor_col=column_names)
    model.fit(train_df)
    o_fitted = model.predict(train_df)
    o_forecast = model.predict(test_df)
    return o_fitted, o_forecast

def calculate_metrics(test_df, ensemble_forecast, model_name):
    lr_mse = mean_squared_error(test_df['y'].astype(float), ensemble_forecast['ensemble_forecast'])
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(ensemble_forecast['ensemble_forecast'])
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    # print(results_lr)
    return np.sqrt(lr_mse)

def calculate_metrics_2(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    # print(results_lr)
    return np.sqrt(lr_mse)


if __name__ == "__main__":

    input_df = fileUtils.read_csv('egg_one_store')
    # print(input_df.shape)
    # input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    # input_df = input_df.drop(columns=['id'])
    # print(input_df.head(5))
    # print(input_df.tail(5))
    input_df['ds'] = pd.to_datetime(input_df['ds'])
    # min_date = input_df['date'].min()
    # max_date = input_df['date'].max()
    # expected_dates = pd.date_range(start=min_date, end=max_date)
    # missing_dates = expected_dates[~expected_dates.isin(input_df['date'])]
    # if len(missing_dates) == 0:
    #     print("The train dataset is complete. It includes all the required dates.")
    # else:
    #     print("The train dataset is incomplete. The following dates are missing:")
    #     print(missing_dates)
    #     missing_df = pd.DataFrame({'date':missing_dates})
    #     eggs1_dataframe = pd.concat([input_df, missing_df], ignore_index=True)
    #     missing_dates = expected_dates[~expected_dates.isin(eggs1_dataframe['date'])]
    #     if len(missing_dates) == 0:
    #         print("The train dataset is complete. It includes all the required dates.")
    # input_df.rename(columns={'date':'ds','sales':'y'}, inplace=True)
    # input_df = input_df.fillna(method='ffill')
    # input_df['test_indicator'] = 0
    # input_df.loc[input_df['ds'].dt.year == 2017, 'test_indicator'] = 1
    # # print(input_df.isna().sum())
    # input_df['TSId'] = 1

    input_df.drop_duplicates(subset=['ds'], keep='first', inplace=True)
    train_df = input_df[input_df['test_indicator']==0]
    test_df = input_df[input_df['test_indicator']==1]

    # Count the occurrences of each date
    date_counts = Counter(input_df['ds'])

    # Find duplicate dates
    duplicates = {date: count for date, count in date_counts.items() if count > 1}

    # Print the duplicate dates
    for date, count in duplicates.items():
        print(f"{date} is duplicated {count} times.")

   # Fit and predict using Prophet model
    p_fitted, p_forecast = fit_and_predict_prophet(train_df, test_df)

    prophet_rmse = calculate_metrics_2(test_df['y'], p_forecast['yhat'], 'Prophet')

    # Fit and predict using Orbit model
    o_fitted, o_forecast = fit_and_predict_orbit(train_df, test_df)

    orbit_rmse = calculate_metrics_2(test_df['y'], o_forecast['prediction'],'Orbit')

    # Prepare training data for meta-model
    meta_train = pd.DataFrame({
        'DLT': o_fitted['prediction'].values,
        'Prophet': p_fitted['yhat'].values,
        'Actual': train_df['y']})
    
    # Prepare test data for meta-model
    meta_test = pd.DataFrame({'DLT': o_forecast['prediction'],'Prophet': p_forecast['yhat']})
    print("meta_test shape is ",meta_test.shape)

    # Train the meta-model
    meta_model = xgb.XGBRegressor(n_estimators=170, learning_rate=0.05, gamma=0,reg_lambda=1,reg_alpha=0,min_child_weight=1, subsample=0.8,colsample_bytree=0.8,max_depth=3, seed=42)
    meta_model.fit(meta_train[['DLT', 'Prophet']], meta_train['Actual'])


    # Final predictions
    final_predictions = meta_model.predict(meta_test)

    # If you need to display or use these predictions:
    test_df['final_predictions'] = final_predictions
    print(type(test_df['final_predictions']))
    print(test_df[['ds', 'final_predictions']])
    print(type(test_df[['ds', 'final_predictions']]))
    print(test_df['final_predictions'].shape)

    

    # Calculate metrics
    stacking_rmse = calculate_metrics_2(test_df['y'], test_df['final_predictions'], 'Stacking Model')

    rmse_results = pd.DataFrame({'Ensemble Models': ['RMSE'],
                                 'Prophet': [prophet_rmse],
                                 'Orbit': [orbit_rmse],
                                 'Stacking': [stacking_rmse]
                            }).round(2)
    
    print(rmse_results)

    forecasted_data = pd.DataFrame({'date': test_df['ds'].values,
                                    'Sales': test_df['y'].values,
                                    'Prophet': p_forecast['yhat'],
                                    'Orbit': o_forecast['prediction'],
                                    'Stacking': test_df['final_predictions'].values
                                    }).round(2)

    print(forecasted_data.head(10))

    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'egg_stacking_XGBoost_forecasting' + '.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    forecasted_data.to_csv(file_path, index=False)



import pandas as pd
import numpy as np
from sklearn.experimental import enable_hist_gradient_boosting
from sklearn.ensemble import HistGradientBoostingRegressor
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from sklearn.base import BaseEstimator, RegressorMixin
from prophet import Prophet
import orbit
from orbit.models.dlt import DLT
import matplotlib.pyplot as plt
import fileUtils

# Custom estimator for Prophet
class ProphetEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols, holiday_df):
        self.model = None
        self.regressor_cols = regressor_cols
        self.holiday_df = holiday_df
        
    def fit(self, X, y):
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        df = X.copy()
        df['y'] = y  # Ensure y is a pandas Series and align indices
        self.model = Prophet(interval_width=0.95,
                             growth='linear',
                             yearly_seasonality=False,
                             weekly_seasonality=False,
                             changepoint_range=0.95,
                             changepoint_prior_scale=0.1,
                             holidays=self.holiday_df,
                             holidays_mode='multiplicative',
                             seasonality_mode='additive',
                             seasonality_prior_scale=0.01)
        for reg in self.regressor_cols:
            if reg != 'change_point_effect':  # Exclude change_point_effect for Prophet
                self.model.add_regressor(reg)
        self.model.fit(df)
        return self
    
    def predict(self, X):
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        forecast = self.model.predict(X)
        return forecast['yhat'].values

# Custom estimator for Orbit's DLT
class DLTModelEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols):
        self.model = None
        self.regressor_cols = regressor_cols
        
    def fit(self, X, y):
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        df = X.copy()
        df['y'] = y  # Ensure y is a pandas Series and align indices
        self.model = DLT(response_col='y',
                         date_col='ds',
                         seasonality=730,
                         estimator='stan-map',
                         global_trend_option='linear',
                         damped_factor=0.7,
                         regressor_col=self.regressor_cols)
        self.model.fit(df)
        return self
    
    def predict(self, X):
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        forecast = self.model.predict(X)
        return forecast['prediction'].values

# Function to calculate RMSE
def rmse(y_true, y_pred):
    return np.sqrt(mean_squared_error(y_true, y_pred))

def calculate_metrics(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    return results_lr

if __name__ == "__main__":

    input_df = fileUtils.read_csv('aqi_delhi_cleaned_data')
    holiday_df = fileUtils.read_csv('aqi_delhi_holidays')  
    holiday_df['type'] = 'non_working_day'
    holiday_df['lower_window'] = 0
    holiday_df['upper_window'] = 1
    holiday_df.rename(columns={'type':'holiday','holiday':'ds'}, inplace=True)
    print(holiday_df.head(5))

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    change_points = pd.to_datetime(holiday_df['ds'].values)
    change_point_series = pd.Series(0, index=input_df.index)
    for cp in change_points:
        # Mark the period after each change point
        change_point_series.loc[input_df['ds'] >= cp] += 1
    input_df['change_point_effect'] = change_point_series

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    train_df = input_df[input_df['test_indicator'] == 0]
    test_df = input_df[input_df['test_indicator'] == 1]

    # Define regressor columns
    regressor_cols = ['pm2_5', 'pm10', 'no', 'no2', 'nox', 'nh3', 'benzene', 'change_point_effect']

    # Ensure 'ds' column is included in the training and test DataFrames
    train_df_with_ds = train_df[regressor_cols + ['ds']]
    test_df_with_ds = test_df[regressor_cols + ['ds']]

    # Create and fit the custom estimators
    prophet_estimator = ProphetEstimator(regressor_cols=regressor_cols, holiday_df=holiday_df)
    dlt_estimator = DLTModelEstimator(regressor_cols=regressor_cols)
    
    prophet_estimator.fit(train_df_with_ds, train_df['y'])
    dlt_estimator.fit(train_df_with_ds, train_df['y'])

    # Predict on the training data to create combined features for HistGradientBoostingRegressor
    prophet_train_pred = prophet_estimator.predict(train_df_with_ds)
    dlt_train_pred = dlt_estimator.predict(train_df_with_ds)
    combined_train_pred = pd.DataFrame({
        'prophet': prophet_train_pred,
        'dlt': dlt_train_pred,
        'ds': train_df_with_ds['ds']
    })

    prophet_test_pred = prophet_estimator.predict(test_df_with_ds)
    dlt_test_pred = dlt_estimator.predict(test_df_with_ds)
    combined_test_pred = pd.DataFrame({
        'prophet': prophet_test_pred,
        'dlt': dlt_test_pred,
        'ds': test_df_with_ds['ds']
    })

    # Remove 'ds' column from the combined predictions for HistGradientBoostingRegressor
    combined_train_pred_numeric = combined_train_pred[['prophet', 'dlt']]
    combined_test_pred_numeric = combined_test_pred[['prophet', 'dlt']]

    # Train HistGradientBoostingRegressor with the custom estimators
    hgb_model = HistGradientBoostingRegressor()
    hgb_model.fit(combined_train_pred_numeric, train_df['y'])  # Use combined_train_pred_numeric

    # Predict using HistGradientBoostingRegressor
    final_predictions = hgb_model.predict(combined_test_pred_numeric)

    # Evaluate the ensemble's performance
    test_rmse = rmse(test_df['y'], final_predictions)
    print(f'Final RMSE of HistGradientBoostingRegressor ensemble: {test_rmse}')

    # Plot the results
    plt.figure(figsize=(12, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual')
    plt.plot(test_df['ds'], final_predictions, label='Ensemble Prediction')
    plt.xlabel('Date')
    plt.ylabel('Value')
    plt.legend()
    plt.show()

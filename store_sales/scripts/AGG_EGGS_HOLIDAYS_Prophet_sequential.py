import pandas as pd
import os 
import numpy as np
import fileUtils 
from prophet import Prophet
import matplotlib.pyplot as plt  
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from datetime import datetime
import matplotlib.pyplot as plt
from statsmodels.tsa.seasonal import seasonal_decompose
from sklearn.preprocessing import StandardScaler
import seaborn as sns
from statsmodels.nonparametric.smoothers_lowess import lowess
from statsmodels.tsa.seasonal import STL

if __name__ == "__main__":

    input_df = fileUtils.read_csv('eggs_agg_data')
    holiday_df = fileUtils.read_csv('eggs_store1_holidays')
    print(input_df.columns)
   
    holiday_df['type'] = 'non_working_day'
    holiday_df['lower_window'] = 0
    holiday_df['upper_window'] = 1
    holiday_df.rename(columns={'type':'holiday','holiday':'ds'}, inplace=True)
    print(holiday_df.head(5))

    # Initialize scalers for each variable
    sales_scaler = StandardScaler()
    transactions_scaler = StandardScaler()
    oilprice_scaler = StandardScaler()
    promotions_scaler = StandardScaler()

    # Fit and transform each variable separately
    input_df['y'] = sales_scaler.fit_transform(input_df[['y']])
    input_df['transactions'] = transactions_scaler.fit_transform(input_df[['transactions']])
    input_df['dcoilwtico'] = oilprice_scaler.fit_transform(input_df[['dcoilwtico']])
    input_df['onpromotion'] = promotions_scaler.fit_transform(input_df[['onpromotion']])

    input_df['ds'] = pd.to_datetime(input_df['ds'])

    train_df = input_df[input_df['test_indicator']==0]
    test_df = input_df[input_df['test_indicator']==1]

    #Plot-1: Time Series plot
    plt.plot(train_df['ds'], train_df['y'] ,color='blue')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.title('EGG Sales in Store-I')
    plt.show()

    #Plot-2: Linear and Non-linear trend analysis graph
    egg1_data = input_df['y']

    # Prepare data for trend analysis
    x = np.arange(len(egg1_data))
    y = egg1_data.values

    # Linear trend - Polynomial fitting
    z = np.polyfit(x, y, 1)  # 1 means linear
    p = np.poly1d(z)

    # Non-linear trend - LOESS (Locally Estimated Scatterplot Smoothing)
    loess_smoothed = lowess(y, x, frac=0.1)  # frac controls the degree of smoothing

    # Plotting
    plt.figure(figsize=(12, 6))
    plt.plot(egg1_data.index, y, label='Original Data', color='lightgray')
    plt.plot(egg1_data.index, p(x), label='Linear Trend', color='red')
    plt.plot(egg1_data.index, loess_smoothed[:, 1], label='LOESS Smoothed Curve', color='blue')
    plt.title('Linear and Non-linear Trend Analysis')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()

    # Plot-3: Seasonal De-composition analysis 
    egg1_data = input_df['y']

    # STL for Yearly Seasonality
    stl_yearly = STL(egg1_data, period=365)
    result_yearly = stl_yearly.fit()

    # STL for Monthly Seasonality
    stl_monthly = STL(egg1_data, period=30)
    result_monthly = stl_monthly.fit()

    # STL for Weekly Seasonality
    stl_weekly = STL(egg1_data, period=7)
    result_weekly = stl_weekly.fit()

    # Function to plot STL results
    def plot_stl(result, title):
        fig, ax = plt.subplots(4, 1, figsize=(10, 8), sharex=True)
        ax[0].plot(result.observed)
        ax[0].set_ylabel('Observed')
        ax[0].set_title(title)

        ax[1].plot(result.trend)
        ax[1].set_ylabel('Trend')

        ax[2].plot(result.seasonal)
        ax[2].set_ylabel('Seasonal')

        ax[3].plot(result.resid)
        ax[3].set_ylabel('Residual')
        ax[3].set_xlabel('Date')

        plt.tight_layout()
        plt.show()

    # Plotting results
    plot_stl(result_yearly, 'Yearly Seasonality')
    plot_stl(result_monthly, 'Monthly Seasonality')
    plot_stl(result_weekly, 'Weekly Seasonality')

    corr_df = train_df.copy()
    corr_df = corr_df[['onpromotion', 'dcoilwtico',
       'transactions', 'y']]
    corr_df.rename(columns={'y':'sales'},inplace=True)
    print(corr_df.columns)

    corr_matrix = corr_df.corr()

    plt.figure(figsize=(10, 8))
    sns.heatmap(corr_matrix, annot=True, fmt=".2f", cmap='coolwarm', cbar=True, square=True)
    plt.title('Correlation Matrix of Store-I Sales data')
    plt.show()

    # holiday_df = fileUtils.read_csv('prophet_holidays')
    # print(holiday_df.columns)
    # holiday_df = holiday_df.drop(holiday_df.columns[holiday_df.columns.str.contains('Unnamed',case=False)], axis=1)
    # print(holiday_df.columns)

    model_fitting_time = datetime.now()
    print("Model Fit starting time :", model_fitting_time)
    model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    weekly_seasonality=True,
                    changepoint_range=0.95,
                    changepoint_prior_scale = 0.1,
                    holidays=holiday_df,
                    holidays_mode='additive',
                    seasonality_mode='additive',
                    seasonality_prior_scale = 1.0)
    model.add_regressor('onpromotion')
    model.add_regressor('transactions')
    model.add_regressor('dcoilwtico')

    model.fit(train_df)
    # model_fitting_timef = datetime.now()
    # print("Model Fitting Ending time :", model_fitting_timef)
    # print("Total Fitting time :",model_fitting_timef-model_fitting_time )
    # print("Model Predict Start time :", model_fitting_timef)
    forecast_df = model.predict(test_df)
    forecast_df['yhat'] = sales_scaler.inverse_transform(forecast_df[['yhat']])
    test_df['y'] = sales_scaler.inverse_transform(test_df[['y']])
    # forecast_df['TSId'] = test_df['TSId'].values
    # model_predict_timef = datetime.now()
    # print("Model Predict Ending time :", model_predict_timef)
    # print("Total Predicting time :",model_predict_timef-model_predict_timef )

    # print(forecast_df.head())

    model.plot_components(forecast_df)
    plt.show()

    # Calculate metrics
    lr_mse = mean_squared_error(test_df['y'].astype(float), forecast_df['yhat'])
    lr_mae = mean_absolute_error(test_df['y'].astype(float), forecast_df['yhat'])

    # Apply the absolute value function to both y_eval and lr_predictions
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(forecast_df['yhat'])

    # Calculate the Root Mean Squared Logarithmic Error (RMSLE)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))

    # Create a DataFrame to store results for Linear Regression
    results_lr = pd.DataFrame({'Model': ['Prophet Model'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]}).round(2)

    # Print the results_lr dataframe
    print(results_lr)



import pandas as pd
import numpy as np
import datetime 
import os
import re


if __name__ == "__main__":

    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    train =pd.read_csv(os.path.join(data_folder, "train.csv"))
    test=pd.read_csv(os.path.join(data_folder, "test.csv"))
    oil=pd.read_csv(os.path.join(data_folder, "oil.csv"))
    stores=pd.read_csv(os.path.join(data_folder, "stores.csv"))
    transactions=pd.read_csv(os.path.join(data_folder, "transactions.csv"))
    holidays=pd.read_csv(os.path.join(data_folder, "holidays_events.csv"))

    print(holidays.head(5))

    train['test_indicator'] = 0
    test['test_indicator'] = 1

    data = pd.concat([train, test], axis=0)
    print(type(data))
    print("Data size is", data.shape)

    data = data.merge(holidays, on='date', how='left')
    print("Data size is", data.shape)
    data = data.merge(stores, on='store_nbr', how='left')
    print("Data size is", data.shape)
    data = data.merge(oil, on='date', how='left')
    print("Data size is", data.shape)
    data = data.merge(transactions, on=['date', 'store_nbr'], how='left')
    print("Data size is", data.shape)
    data['sales'] = data['sales'].fillna(method='ffill')
    data['dcoilwtico'] = data['dcoilwtico'].fillna(method='bfill')
    data['transactions'] = data['transactions'].fillna(data['transactions'].mean())
    data.drop(columns={'type_x','locale','locale_name','description','transferred'}, inplace=True)
    print(data.isna().sum())

    data['date'] = pd.to_datetime(data['date'])
    min_date = data['date'].min()
    max_date = data['date'].max()
    expected_dates = pd.date_range(start=min_date, end=max_date)
    missing_dates = expected_dates[~expected_dates.isin(data['date'])]
    if len(missing_dates) == 0:
        print("The train dataset is complete. It includes all the required dates.")
    else:
        print("The train dataset is incomplete. The following dates are missing:")
        print(missing_dates)
    missing_df = pd.DataFrame({'date':missing_dates})
    dataframe = pd.concat([data, missing_df], ignore_index=True)
    dataframe.sort_values('date', inplace=True)

    categories = ['AUTOMOTIVE', 'CELEBRATION', 'BREAD/BAKERY', 'BOOKS', 'BEVERAGES', 'BEAUTY',
              'BABY CARE', 'SEAFOOD', 'SCHOOL AND OFFICE SUPPLIES', 'PRODUCE',
              'PREPARED FOODS', 'POULTRY', 'PLAYERS AND ELECTRONICS', 'PET SUPPLIES',
              'PERSONAL CARE', 'MEATS', 'MAGAZINES', 'LIQUOR,WINE,BEER', 'LINGERIE',
              'LAWN AND GARDEN', 'LADIESWEAR', 'HOME CARE', 'HOME APPLIANCES', 'CLEANING',
              'DAIRY', 'DELI', 'EGGS', 'HOME AND KITCHEN II', 'HOME AND KITCHEN I',
              'HARDWARE', 'GROCERY II', 'GROCERY I', 'FROZEN FOODS']
    d_df = dataframe[~dataframe['family'].isin(categories)]
    print(d_df.shape)
    dataframe = dataframe.fillna(method='ffill')
    d_df = dataframe[~dataframe['family'].isin(categories)]
    print(d_df.shape)
    print(d_df.head(20))
    dataframe['family'] = dataframe['family'].apply(lambda x: x.lower() if isinstance(x, str) else x)
    dataframe['family'] = dataframe['family'].apply(lambda x: re.sub(r'[^a-zA-Z0-9]+', '_', x))

    # Aggregate dataset
    agg_dataframe_df = dataframe.groupby('date').agg({'sales':'sum',
                                                      'onpromotion':'sum',
                                                      'dcoilwtico':'mean',
                                                      'transactions':'sum'}).reset_index()
    
    agg_dataframe_df['date'] = pd.to_datetime(agg_dataframe_df['date'])
    min_date = agg_dataframe_df['date'].min()
    max_date = agg_dataframe_df['date'].max()
    expected_dates = pd.date_range(start=min_date, end=max_date)
    missing_dates = expected_dates[~expected_dates.isin(agg_dataframe_df['date'])]
    if len(missing_dates) == 0:
        print("The train dataset is complete. It includes all the required dates.")
    else:
        print("The train dataset is incomplete. The following dates are missing:")
        print(missing_dates)
        missing_df = pd.DataFrame({'date':missing_dates})
        agg_dataframe_df = pd.concat([agg_dataframe_df, missing_df], ignore_index=True)
    agg_dataframe_df.fillna()
    agg_dataframe_df.sort_values('date', inplace=True)
    agg_dataframe_df['TSId'] = 0
    # agg_dataframe_df.to_csv(os.path.join(data_folder, "agg_store_sales.csv"))

    # Multiple series of item & store
    multi_dataframe = dataframe.copy()
    multi_dataframe['store_nbr_family'] = multi_dataframe['store_nbr'].astype(str) + '_' + multi_dataframe['family']
    print(multi_dataframe.columns)
    print(multi_dataframe.shape)
    unique_values = multi_dataframe['store_nbr_family'].unique()
    id_mapping = {value: index for index, value in enumerate(unique_values)}
    multi_dataframe['TSId'] = multi_dataframe['store_nbr_family'].map(id_mapping)
    multi_dataframe['test_indicator'] = 0
    # multi_dataframe.to_csv(os.path.join(data_folder, "multi_store_item_sales.csv"))

    # eggs dataframe
    eggs_dataframe = dataframe.copy()
    dataframe_eggs = eggs_dataframe[eggs_dataframe['family']=='eggs']
    dataframe_eggs['store_nbr_family'] = dataframe_eggs['store_nbr'].astype(str) + '_' + dataframe_eggs['family']
    dataframe_eggs['date'] = pd.to_datetime(dataframe_eggs['date'])
    min_date = dataframe_eggs['date'].min()
    max_date = dataframe_eggs['date'].max()
    expected_dates = pd.date_range(start=min_date, end=max_date)
    missing_dates = expected_dates[~expected_dates.isin(dataframe_eggs['date'])]
    if len(missing_dates) == 0:
        print("The train dataset is complete. It includes all the required dates.")
    else:
        print("The train dataset is incomplete. The following dates are missing:")
        print(missing_dates)
        missing_df = pd.DataFrame({'date':missing_dates})
        dataframe_eggs = pd.concat([dataframe_eggs, missing_df], ignore_index=True)
        missing_dates = expected_dates[~expected_dates.isin(dataframe_eggs['date'])]
        if len(missing_dates) == 0:
            print("The train dataset is complete. It includes all the required dates.")
    dataframe_eggs.sort_values('date', inplace=True)
    unique_values = dataframe_eggs['store_nbr_family'].unique()
    id_mapping = {value: index for index, value in enumerate(unique_values)}
    dataframe_eggs['TSId'] = dataframe_eggs['store_nbr_family'].map(id_mapping)
    dataframe_eggs.to_csv(os.path.join(data_folder, "eggs_stores.csv"))

    # eggs - store=1
    eggs1_dataframe = dataframe.copy()
    eggs1_dataframe = eggs1_dataframe[(eggs1_dataframe['family']=='eggs') & (eggs1_dataframe['store_nbr']== 1)]
    eggs1_dataframe['store_nbr_family'] = eggs1_dataframe['store_nbr'].astype(str) + '_' + eggs1_dataframe['family']
    eggs1_dataframe['date'] = pd.to_datetime(eggs1_dataframe['date'])
    min_date = eggs1_dataframe['date'].min()
    max_date = eggs1_dataframe['date'].max()
    expected_dates = pd.date_range(start=min_date, end=max_date)
    missing_dates = expected_dates[~expected_dates.isin(eggs1_dataframe['date'])]
    if len(missing_dates) == 0:
        print("The train dataset is complete. It includes all the required dates.")
    else:
        print("The train dataset is incomplete. The following dates are missing:")
        print(missing_dates)
        missing_df = pd.DataFrame({'date':missing_dates})
        eggs1_dataframe = pd.concat([eggs1_dataframe, missing_df], ignore_index=True)
        missing_dates = expected_dates[~expected_dates.isin(eggs1_dataframe['date'])]
        if len(missing_dates) == 0:
            print("The train dataset is complete. It includes all the required dates.")
    eggs1_dataframe.sort_values('date', inplace=True)
    eggs1_dataframe['TSId'] = 1
    eggs1_dataframe.rename(columns={'date':'ds','sales':'y'}, inplace=True)
    eggs1_dataframe = eggs1_dataframe.ffill()
    eggs1_dataframe['test_indicator'] = 0
    eggs1_dataframe.loc[eggs1_dataframe['ds'].dt.year == 2017, 'test_indicator'] = 1
    eggs1_dataframe.to_csv(os.path.join(data_folder, "egg_one_store.csv"))







    
    
    

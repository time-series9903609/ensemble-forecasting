import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error, mean_squared_log_error
from prophet import Prophet
from orbit.models.dlt import DLT
import matplotlib.pyplot as plt
import fileUtils
from sklearn.ensemble import GradientBoostingRegressor

# Custom function to fit Prophet model
def fit_and_predict_prophet(train_df, test_df, regressor_cols, holiday_df):
    model = Prophet(interval_width=0.95,
                    growth='linear',
                    yearly_seasonality=False,
                    weekly_seasonality=False,
                    changepoint_range=0.95,
                    changepoint_prior_scale=0.1,
                    holidays=holiday_df,
                    holidays_mode='multiplicative',
                    seasonality_mode='additive',
                    seasonality_prior_scale=0.01)
    for reg in regressor_cols:
        if reg != 'change_point_effect':  # Exclude change_point_effect for Prophet
            model.add_regressor(reg)
    model.fit(train_df)
    p_fitted = model.predict(train_df)
    p_forecast = model.predict(test_df)
    return p_fitted['yhat'].values, p_forecast['yhat'].values

# Custom function to fit DLT model
def fit_and_predict_orbit(train_df, test_df, regressor_cols):
    model = DLT(response_col='y',
                date_col='ds',
                seasonality=730,
                estimator='stan-map',
                global_trend_option='linear',
                damped_factor=0.7,
                regressor_col=regressor_cols)
    model.fit(train_df)
    o_fitted = model.predict(train_df)
    o_forecast = model.predict(test_df)
    return o_fitted['prediction'].values, o_forecast['prediction'].values

# Function to calculate RMSE
def rmse(y_true, y_pred):
    return np.sqrt(mean_squared_error(y_true, y_pred))

# Function to calculate metrics
def calculate_metrics(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(abs(test_value.astype(float)), abs(forecast_value)))
    results_lr = pd.DataFrame({'Model': [model_name],
                               'RMSLE': [lr_rmsle],
                               'RMSE': [np.sqrt(lr_mse)]
                              }).round(2)
    return np.sqrt(lr_mse)

if __name__ == "__main__":
    input_df = fileUtils.read_csv('aqi_delhi_cleaned_data')
    holiday_df = fileUtils.read_csv('aqi_delhi_holidays')
    holiday_df['type'] = 'non_working_day'
    holiday_df['lower_window'] = 0
    holiday_df['upper_window'] = 1
    holiday_df.rename(columns={'type': 'holiday', 'holiday': 'ds'}, inplace=True)
    print(holiday_df.head(5))

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    change_points = pd.to_datetime(holiday_df['ds'].values)
    change_point_series = pd.Series(0, index=input_df.index)
    for cp in change_points:
        change_point_series.loc[input_df['ds'] >= cp] += 1
    input_df['change_point_effect'] = change_point_series

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    train_df = input_df[input_df['test_indicator'] == 0]
    test_df = input_df[input_df['test_indicator'] == 1]

    # Define regressor columns
    regressor_cols = ['pm2_5', 'pm10', 'no', 'no2', 'nox', 'nh3', 'benzene', 'change_point_effect']

    # Fit and predict using Prophet model
    prophet_train_pred, prophet_test_pred = fit_and_predict_prophet(train_df, test_df, regressor_cols, holiday_df)

    # Fit and predict using Orbit model
    orbit_train_pred, orbit_test_pred = fit_and_predict_orbit(train_df, test_df, regressor_cols)

    # Calculate residuals from Prophet predictions
    residuals_train = train_df['y'] - prophet_train_pred
    residuals_test = test_df['y'] - prophet_test_pred

    # Prepare data for GradientBoostingRegressor
    train_df_with_preds = train_df.copy()
    train_df_with_preds['prophet_pred'] = prophet_train_pred
    test_df_with_preds = test_df.copy()
    test_df_with_preds['prophet_pred'] = prophet_test_pred

    gbr_model = GradientBoostingRegressor(n_estimators=100, random_state=42)
    gbr_model.fit(train_df_with_preds[['prophet_pred']], residuals_train)

    # Predict residuals using GradientBoostingRegressor
    residuals_train_pred = gbr_model.predict(train_df_with_preds[['prophet_pred']])
    residuals_test_pred = gbr_model.predict(test_df_with_preds[['prophet_pred']])

    # Combine predictions
    final_train_pred = prophet_train_pred + residuals_train_pred
    final_test_pred = prophet_test_pred + residuals_test_pred

    # Evaluate the ensemble's performance
    test_rmse = rmse(test_df['y'], final_test_pred)
    print(f'Final RMSE of Boosting ensemble: {test_rmse}')

    # Plot the results
    plt.figure(figsize=(12, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual')
    plt.plot(test_df['ds'], final_test_pred, label='Ensemble Prediction')
    plt.xlabel('Date')
    plt.ylabel('Value')
    plt.legend()
    plt.show()

import pandas as pd
import numpy as np
from collections import Counter
from orbit.models import DLT 
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import fileUtils

if __name__ == "__main__":
    input_df = fileUtils.read_csv('egg_one_store')
    input_df = input_df.drop_duplicates(subset='ds', keep='first')
    input_df['ds'] = pd.to_datetime(input_df['ds'])
    input_df.loc[input_df['ds'].dt.year == 2017, 'test_indicator'] = 1
    train_df = input_df[input_df['test_indicator']==0]
    test_df = input_df[input_df['test_indicator']==1]

    # Count the occurrences of each date
    date_counts = Counter(train_df['ds'])

    # Find duplicate dates
    duplicates = {date: count for date, count in date_counts.items() if count > 1}

    # Print the duplicate dates
    for date, count in duplicates.items():
        print(f"{date} is duplicated {count} times.")

    # Parameter grid for hyperparameter tuning
    damped_factors = [0.7, 0.8,0.85, 0.95]
    seasonality_values = [182, 365, 730]  # Assuming yearly and bi-yearly seasonality
    trend_options = ['linear', 'loglinear']

    best_rmse = float('inf')
    best_params = {}

    for damped_factor in damped_factors:
        for seasonality in seasonality_values:
            for trend_option in trend_options:
                model = DLT(response_col='y', 
                            date_col='ds', 
                            seasonality=seasonality,
                            estimator='stan-map',
                            global_trend_option=trend_option, 
                            damped_factor=damped_factor, 
                            regressor_col=['onpromotion', 'transactions', 'dcoilwtico'])
                model.fit(train_df)
                forecast = model.predict(test_df)
                
                # Calculate RMSE for the current set of parameters
                rmse = np.sqrt(mean_squared_error(test_df['y'], forecast['prediction']))

                if rmse < best_rmse:
                    best_rmse = rmse
                    best_params = {'damped_factor': damped_factor, 'seasonality': seasonality, 'trend_option': trend_option}

    # Output the best parameters and RMSE
    print("Best Parameters:", best_params)
    print("Lowest RMSE:", best_rmse)

    # Fit the best model
    model = DLT(response_col='y', 
                date_col='ds', 
                seasonality=best_params['seasonality'],
                estimator='stan-map',
                global_trend_option=best_params['trend_option'], 
                damped_factor=best_params['damped_factor'], 
                regressor_col=['onpromotion', 'transactions', 'dcoilwtico'])
    model.fit(train_df)
    forecast = model.predict(test_df)

    # Plot results
    plt.figure(figsize=(10, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual', color='blue')
    plt.plot(forecast['ds'], forecast['prediction'], label='Forecast', color='red')
    plt.title('Actual vs. Forecast Data')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()

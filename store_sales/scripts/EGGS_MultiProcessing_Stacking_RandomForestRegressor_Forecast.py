import pandas as pd
import os
import numpy as np
import fileUtils
from prophet import Prophet
import orbit
from orbit.models import DLT
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, mean_squared_log_error
from multiprocessing import Pool, cpu_count
from typing import List, Any
from functools import partial
import matplotlib.pyplot as plt
from sklearn.base import BaseEstimator, RegressorMixin

class ProphetEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols, holiday_df):
        self.holiday_df = holiday_df
        self.model = None
        self.regressor_cols = regressor_cols
        
    def fit(self, X, y):
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        df = X.copy()
        df['y'] = y
        self.model = Prophet(interval_width=0.95,
                             growth='linear',
                             yearly_seasonality=True,
                             weekly_seasonality=True,
                             changepoint_range=0.95,
                             changepoint_prior_scale=0.1,
                             holidays=self.holiday_df,
                             holidays_mode='additive',
                             seasonality_mode='additive',
                             seasonality_prior_scale=1.0)
        for reg in self.regressor_cols:
            if reg != 'change_point_effect':
                self.model.add_regressor(reg)
        self.model.fit(df)
        return self
    
    def predict(self, X):
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        forecast = self.model.predict(X)
        return forecast['yhat'].values

class DLTModelEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols):
        self.model = None
        self.regressor_cols = regressor_cols
        
    def fit(self, X, y):
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        df = X.copy()
        df['y'] = y
        self.model = DLT(response_col='y',
                         date_col='ds',
                         seasonality=182,
                         estimator='stan-map',
                         global_trend_option='loglinear',
                         damped_factor=0.7,
                         regressor_col=self.regressor_cols)
        self.model.fit(df)
        return self
    
    def predict(self, X):
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        forecast = self.model.predict(X)
        return forecast['prediction'].values

class StackedEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols, holiday_df):
        self.regressor_cols = regressor_cols
        self.holiday_df = holiday_df
        self.prophet_estimator = ProphetEstimator(regressor_cols, holiday_df)
        self.dlt_estimator = DLTModelEstimator(regressor_cols)
        self.meta_model = RandomForestRegressor(n_estimators=100, random_state=42)
        
    def fit(self, X, y):
        prophet_predictions = self.prophet_estimator.fit(X, y).predict(X)
        dlt_predictions = self.dlt_estimator.fit(X, y).predict(X)
        
        meta_features = np.vstack((prophet_predictions, dlt_predictions)).T
        self.meta_model.fit(meta_features, y)
        return self
    
    def predict(self, X):
        prophet_predictions = self.prophet_estimator.predict(X)
        dlt_predictions = self.dlt_estimator.predict(X)
        
        meta_features = np.vstack((prophet_predictions, dlt_predictions)).T
        return self.meta_model.predict(meta_features)

def fit_and_predict(input_df, holiday_df):
    train_data = input_df.loc[input_df['test_indicator'] == 0]
    test_data = input_df.loc[input_df['test_indicator'] == 1]
    
    stacked_estimator = StackedEstimator(regressor_cols, holiday_df)
    stacked_estimator.fit(train_data.drop(columns=['y']), train_data['y'])
    
    train_forecast = stacked_estimator.predict(train_data.drop(columns=['y']))
    test_forecast = stacked_estimator.predict(test_data.drop(columns=['y']))
    
    train_forecast_df = pd.DataFrame({
        'tsid': train_data['TSId'].values,
        'store_nbr': train_data['store_nbr'].values,
        'ds': train_data['ds'].values,
        'y': train_data['y'].values,
        'stacked_forecast': train_forecast
    })
    
    test_forecast_df = pd.DataFrame({
        'tsid': test_data['TSId'].values,
        'store_nbr': test_data['store_nbr'].values,
        'ds': test_data['ds'].values,
        'y': test_data['y'].values,
        'stacked_forecast': test_forecast
    })
    
    forecast = pd.concat([train_forecast_df, test_forecast_df])
    return forecast

def calculate_metrics_2(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(abs(test_value.astype(float)), abs(forecast_value)))
    results_lr = pd.DataFrame({'Model': [model_name],
                               'RMSLE': [lr_rmsle],
                               'RMSE': [np.sqrt(lr_mse)]
                              }).round(2)
    return np.sqrt(lr_mse)

if __name__ == "__main__":
    input_df = fileUtils.read_csv('15stores_data')
    holiday_df = fileUtils.read_csv('eggs_store1_holidays')
    holiday_df['type'] = 'non_working_day'
    holiday_df['lower_window'] = 0
    holiday_df['upper_window'] = 1
    holiday_df.rename(columns={'type':'holiday','holiday':'ds'}, inplace=True)
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    change_points = pd.to_datetime(holiday_df['ds'].values)
    change_point_series = pd.Series(0, index=input_df.index)
    for cp in change_points:
        change_point_series.loc[input_df['ds'] >= cp] += 1
    input_df['change_point_effect'] = change_point_series

    series = [input_df.loc[input_df.TSId == x] for x in input_df.TSId.unique()]

    regressor_cols = ['onpromotion', 'dcoilwtico', 'transactions', 'change_point_effect']
    fit_and_predict_with_holidays = partial(fit_and_predict, holiday_df=holiday_df)

    p = Pool(cpu_count())
    forecasts: List[Any] = list(p.imap(fit_and_predict_with_holidays, series))

    forecast_dataframe = pd.concat([f for f in forecasts if f is not None])
    forecast_dataframe = forecast_dataframe[['tsid', 'store_nbr', 'ds', 'y', 'stacked_forecast']]
    print(forecast_dataframe.head(10))
    print(forecast_dataframe.tail(10))
    print("Forecast data set shape is", forecast_dataframe.shape)

    train_df = forecast_dataframe[forecast_dataframe['ds'] < '2017-01-01']
    test_df = forecast_dataframe[forecast_dataframe['ds'] >= '2017-01-01']

    y_train = train_df['y']
    y_test = test_df['y']

    final_predictions_df = pd.DataFrame({
        'ds': test_df['ds'].values,
        'stacked_predictions': test_df['stacked_forecast']
    })
    print(final_predictions_df.head(10))

    mse = mean_squared_error(y_test, final_predictions_df['stacked_predictions'])
    rmse = np.sqrt(mse)
    print(f'Final RMSE of Stacked ensemble: {rmse}')


    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'aqi_multiprocessing_randomforest_forecasting.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    final_predictions_df.to_csv(file_path, index=False)

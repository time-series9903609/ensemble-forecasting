import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from prophet import Prophet
import orbit
from orbit.models import DLT 
import fileUtils 
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import xgboost as xgb
import os
from collections import Counter
from sklearn.ensemble import AdaBoostRegressor   

def fit_and_predict_prophet(train_df, test_df):
    model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    weekly_seasonality=True,
                    changepoint_range=0.95,
                    changepoint_prior_scale=0.1,
                    holidays=holiday_df,
                    holidays_mode='additive',
                    seasonality_mode='additive',
                    seasonality_prior_scale=1.0)
    model.add_regressor('onpromotion')
    model.add_regressor('dcoilwtico')
    model.add_regressor('transactions')
    model.fit(train_df)
    p_fitted = model.predict(train_df)
    p_forecast = model.predict(test_df)
    return p_fitted, p_forecast

def fit_and_predict_orbit(train_df, test_df):
    column_names = ['onpromotion', 'dcoilwtico', 'transactions','change_point_effect']
    model = DLT(response_col='y', 
                date_col='ds', 
                seasonality=182,
                estimator='stan-map',
                global_trend_option='loglinear', 
                damped_factor=0.7, 
                regressor_col=column_names)
    model.fit(train_df)
    o_fitted = model.predict(train_df)
    o_forecast = model.predict(test_df)
    return o_fitted, o_forecast


def calculate_metrics(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    # print(results_lr)
    return np.sqrt(lr_mse)


if __name__ == "__main__":

    input_df = fileUtils.read_csv('agg_store_sales')
    holiday_df = fileUtils.read_csv('eggs_store1_holidays')  
    holiday_df['type'] = 'non_working_day'
    holiday_df['lower_window'] = 0
    holiday_df['upper_window'] = 1
    holiday_df.rename(columns={'type':'holiday','holiday':'ds'}, inplace=True)

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    input_df = input_df.sort_values(by='ds').drop_duplicates(subset=['ds'], keep='first')
    change_points = pd.to_datetime(holiday_df['ds'].values)
    change_point_series = pd.Series(0, index=input_df.index)
    for cp in change_points:
        # Mark the period after each change point
        change_point_series.loc[input_df['ds'] >= cp] += 1
    input_df['change_point_effect'] = change_point_series

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    train_df = input_df[input_df['test_indicator']==0]
    test_df = input_df[input_df['test_indicator']==1]

   # Fit and predict using Prophet model
    p_fitted, p_forecast = fit_and_predict_prophet(train_df, test_df)

    prophet_rmse = calculate_metrics(test_df['y'], p_forecast['yhat'], 'Prophet')

    print("Prophet RMSE value is ",prophet_rmse)

    # Fit and predict using Orbit model
    o_fitted, o_forecast = fit_and_predict_orbit(train_df, test_df)

    orbit_rmse = calculate_metrics(test_df['y'], o_forecast['prediction'],'Orbit')

    print("Orbit RMSE value is ",orbit_rmse)

    # Combine the forecasts into a single DataFrame for AdaBoost training
    combined_forecast_train = pd.DataFrame({
        'Prophet': p_fitted['yhat'],
        'DLT': o_fitted['prediction']
    })
    combined_forecast_test = pd.DataFrame({
        'Prophet': p_forecast['yhat'],
        'DLT': o_forecast['prediction']
    })
    y_train = train_df['y']
    y_test = test_df['y']

    # Train AdaBoostRegressor
    ada_boost_model = AdaBoostRegressor(n_estimators=50, learning_rate=1.0, random_state=42)
    ada_boost_model.fit(combined_forecast_train, y_train)

    # Predict using AdaBoostRegressor
    final_predictions = ada_boost_model.predict(combined_forecast_test)
    # print("Data type of final prediction is ", type(final_predictions))
    final_predictions = pd.DataFrame({'adaboost_predictions':final_predictions})
    final_predictions['ds'] = test_df['ds'].values
    final_predictions['sales'] = test_df['y'].values

    final_predictions=final_predictions[['ds','sales','adaboost_predictions']]

    print(final_predictions.head(10))

    mse = mean_squared_error(y_test, final_predictions['adaboost_predictions'])
    rmse = np.sqrt(mse)
    print(f'Final RMSE of AdaBoost ensemble: {rmse}')
    
    

    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'egg_store1_Adaboosting_forecasting' + '.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    final_predictions.to_csv(file_path, index=False)



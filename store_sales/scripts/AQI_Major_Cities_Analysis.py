import pandas as pd
import os 
import numpy as np
import fileUtils 
from prophet import Prophet
import matplotlib.pyplot as plt
from statsmodels.nonparametric.smoothers_lowess import lowess
from statsmodels.tsa.seasonal import STL
import ruptures as rpt


if __name__ == "__main__":

    input_df = fileUtils.read_csv('aq_major_cities_cleaned_data')
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    print(input_df.columns)
    delhi_dataframe = input_df[input_df['city'] == 'Delhi']

    #date
    input_df['ds'] = pd.to_datetime(input_df['ds'])
    input_df.set_index('ds', inplace=True)

    # Time Series plot
    cities = input_df['city'].unique()
    num_cities = len(cities)

    # Create a colormap
    cm = plt.get_cmap('viridis')
    colors = [cm(1.*i/num_cities) for i in range(num_cities)]

    plt.figure(figsize=(15, 8))

    for city, color in zip(cities, colors):
        city_data = input_df[input_df['city'] == city]
        plt.plot(city_data['y'], label=f'{city} AQI', color=color)


    plt.title('AQI Time Series Analysis for Major Cities')
    plt.xlabel('Date')
    plt.ylabel('AQI')
    plt.legend()
    plt.show()


    # Selecting a sample city's AQI data
    city_data = input_df[input_df['city'] == 'Delhi']['y']

    # Prepare data for trend analysis
    x = np.arange(len(city_data))
    y = city_data.values

    # Linear trend - Polynomial fitting
    z = np.polyfit(x, y, 1)  # 1 means linear
    p = np.poly1d(z)

    # Non-linear trend - LOESS (Locally Estimated Scatterplot Smoothing)
    loess_smoothed = lowess(y, x, frac=0.1)  # frac controls the degree of smoothing

    # Plotting
    plt.figure(figsize=(12, 6))
    plt.plot(city_data.index, y, label='Original Data', color='lightgray')
    plt.plot(city_data.index, p(x), label='Linear Trend', color='red')
    plt.plot(city_data.index, loess_smoothed[:, 1], label='LOESS Smoothed Curve', color='blue')

    plt.title('Linear and Non-linear Trend Analysis')
    plt.xlabel('Date')
    plt.ylabel('AQI')
    plt.legend()
    plt.show()


    # Select the AQI data (ensure your data is at least a few cycles long to see the seasonal pattern)
    city_data = input_df[input_df['city'] == 'Delhi']
    aqi_data = city_data['y']

    # STL for Yearly Seasonality
    stl_yearly = STL(aqi_data, period=365)
    result_yearly = stl_yearly.fit()

    # STL for Monthly Seasonality
    stl_monthly = STL(aqi_data, period=30)
    result_monthly = stl_monthly.fit()

    # STL for Weekly Seasonality
    stl_weekly = STL(aqi_data, period=7)
    result_weekly = stl_weekly.fit()

    # Function to plot STL results
    def plot_stl(result, title):
        fig, ax = plt.subplots(4, 1, figsize=(10, 8), sharex=True)
        ax[0].plot(result.observed)
        ax[0].set_ylabel('Observed')
        ax[0].set_title(title)

        ax[1].plot(result.trend)
        ax[1].set_ylabel('Trend')

        ax[2].plot(result.seasonal)
        ax[2].set_ylabel('Seasonal')

        ax[3].plot(result.resid)
        ax[3].set_ylabel('Residual')
        ax[3].set_xlabel('Date')

        plt.tight_layout()
        plt.show()

    # Plotting results
    plot_stl(result_yearly, 'Yearly Seasonality')
    plot_stl(result_monthly, 'Monthly Seasonality')
    plot_stl(result_weekly, 'Weekly Seasonality')

   # Load your data (assuming the DataFrame 'input_df' and a date column already exists)
    # input_df['ds'] = pd.to_datetime(input_df['ds'])
    # input_df.set_index('ds', inplace=True)

    # Select data for the city 'Delhi'
    city_data = input_df[input_df['city'] == 'Delhi']['y']
    delhi_data = input_df[input_df['city'] == 'Delhi']
    data = city_data.values  # This retains only the values for changepoint detection


    # Changepoint detection configuration
    model = "l2"  # Use the L2 norm (Euclidean), other options include "l1", "rbf", etc.
    algo = rpt.Pelt(model=model, min_size=1, jump=5).fit(data)
    result = algo.predict(pen=10)  # Set penalty value; higher values lead to fewer breakpoints

    # Ensure that we have valid change points before proceeding
    change_point_dates = None
    if len(result) > 1:  # More than one element means we have at least one change point
        change_point_dates = city_data.index[result[:-1]]  # Get the dates for the change points
        print(change_point_dates)
        # change_point_dates.to_frame(index=False)
        print(type(change_point_dates))
        # change_point_dates
        holiday_df = pd.DataFrame({'holiday': change_point_dates})
        # holiday_df = pd.DataFrame(change_point_dates, columns=['holiday'])
        print(">>>>>>>>>>>>>>>>>>>>>>>>> \n",holiday_df.head(10))
    else:
        print("No change points detected.")

    # Plotting
    plt.figure(figsize=(10, 6))
    plt.plot(city_data.index, data, label='AQI')
    for cp in change_point_dates:
        plt.axvline(x=cp, color='r', linestyle='--', lw=2)  # Red dashed line at each changepoint
    plt.title('Detected Change Points in Delhi AQI Time Series')
    plt.xlabel('Date')
    plt.ylabel('AQI')
    plt.legend()
    plt.show()

    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'aqi_delhi_cleaned_data' + '.csv'
    holidays_file = 'aqi_delhi_holidays'+'.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    holiday_file_path = os.path.join(data_folder, holidays_file)
    delhi_dataframe.to_csv(file_path, index=False)
    holiday_df.to_csv(holiday_file_path,index=False)




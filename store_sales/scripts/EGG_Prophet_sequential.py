import pandas as pd
import os 
import numpy as np
import fileUtils 
from prophet import Prophet
import matplotlib.pyplot as plt  
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from datetime import datetime
import matplotlib.pyplot as plt
from statsmodels.tsa.seasonal import seasonal_decompose

if __name__ == "__main__":

    input_df = fileUtils.read_csv('egg_one_store')
    print(input_df.shape)
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df = input_df.drop(columns=['id'])
    input_df['ds']=pd.to_datetime(input_df['ds'])
    train_df = input_df[input_df['test_indicator']==0]
    test_df = input_df[input_df['test_indicator']==1]

    plt.plot(train_df['ds'], train_df['y'] ,color='blue')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.title('Eggs Sales over time period')
    plt.show()

    decomposition = seasonal_decompose(train_df['y'], model='additive', period=365)

    # Plot the decomposed components
    plt.figure(figsize=(12, 8))

    plt.subplot(411)
    plt.plot(train_df['ds'], train_df['y'], label='Original')
    plt.legend()

    plt.subplot(412)
    plt.plot(train_df['ds'],decomposition.trend, label='Trend')
    plt.legend()

    plt.subplot(413)
    plt.plot(train_df['ds'],decomposition.seasonal, label='Seasonal')
    plt.legend()

    plt.subplot(414)
    plt.plot(train_df['ds'],decomposition.resid, label='Residual')
    plt.legend()

    plt.tight_layout()
    plt.show()


    # holiday_df = fileUtils.read_csv('prophet_holidays')
    # print(holiday_df.columns)
    # holiday_df = holiday_df.drop(holiday_df.columns[holiday_df.columns.str.contains('Unnamed',case=False)], axis=1)
    # print(holiday_df.columns)

    model_fitting_time = datetime.now()
    print("Model Fit starting time :", model_fitting_time)
    model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    weekly_seasonality=True,
                    changepoint_range=0.95,
                    changepoint_prior_scale = 0.1,
                    # holidays=holiday_df,
                    # holidays_mode='additive',
                    seasonality_mode='additive',
                    seasonality_prior_scale = 1.0)
    model.add_regressor('onpromotion')
    model.add_regressor('transactions')
    model.add_regressor('dcoilwtico')

    model.fit(train_df)
    # model_fitting_timef = datetime.now()
    # print("Model Fitting Ending time :", model_fitting_timef)
    # print("Total Fitting time :",model_fitting_timef-model_fitting_time )
    # print("Model Predict Start time :", model_fitting_timef)
    forecast_df = model.predict(test_df)
    forecast_df['TSId'] = test_df['TSId'].values
    # model_predict_timef = datetime.now()
    # print("Model Predict Ending time :", model_predict_timef)
    # print("Total Predicting time :",model_predict_timef-model_predict_timef )

    # print(forecast_df.head())

    model.plot_components(forecast_df)
    plt.show()

    # Calculate metrics
    lr_mse = mean_squared_error(test_df['y'].astype(float), forecast_df['yhat'])
    lr_mae = mean_absolute_error(test_df['y'].astype(float), forecast_df['yhat'])

    # Apply the absolute value function to both y_eval and lr_predictions
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(forecast_df['yhat'])

    # Calculate the Root Mean Squared Logarithmic Error (RMSLE)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))

    # Create a DataFrame to store results for Linear Regression
    results_lr = pd.DataFrame({'Model': ['Prophet Model'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]}).round(2)

    # Print the results_lr dataframe
    print(results_lr)



import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from prophet import Prophet
import orbit
from orbit.models import DLT 
import fileUtils 
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import xgboost as xgb
import os
from collections import Counter   

def fit_and_predict_prophet(train_df, test_df):
    model = Prophet(interval_width = 0.95,
                    growth='linear',
                    yearly_seasonality=True,
                    weekly_seasonality=True,
                    changepoint_range=0.95,
                    changepoint_prior_scale=0.1,
                    # holidays=holiday_df,
                    # holidays_mode='multiplicative',
                    seasonality_mode='additive',
                    seasonality_prior_scale=10.0)
    model.add_regressor('PM2.5')
    model.add_regressor('PM10')
    # model.add_regressor('dcoilwtico')
    model.fit(train_df)
    p_fitted = model.predict(train_df)
    p_forecast = model.predict(test_df)
    return p_fitted, p_forecast

def fit_and_predict_orbit(train_df, test_df):
    column_names = ['PM2.5', 'PM10']
    model = DLT(response_col='y', 
                date_col='ds', 
                seasonality=730,
                estimator='stan-map',
                global_trend_option='linear', 
                damped_factor=0.7, 
                regressor_col=column_names)
    model.fit(train_df)
    o_fitted = model.predict(train_df)
    o_forecast = model.predict(test_df)
    return o_fitted, o_forecast

def calculate_metrics(test_df, ensemble_forecast, model_name):
    lr_mse = mean_squared_error(test_df['y'].astype(float), ensemble_forecast['ensemble_forecast'])
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(ensemble_forecast['ensemble_forecast'])
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    # print(results_lr)
    return np.sqrt(lr_mse)

def calculate_metrics_2(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    # print(results_lr)
    return np.sqrt(lr_mse)


if __name__ == "__main__":

    # Read input data
    input_df = fileUtils.read_csv('aq_hyd_cleaned_data')
    input_df = input_df.drop(input_df.columns[input_df.columns.str.contains('Unnamed', case=False)], axis=1)
    input_df['Date'] = pd.to_datetime(input_df['Date'])
    input_df.rename(columns={'Date':'ds','AQI':'y'}, inplace=True)
    # train_df = input_df[input_df['ds'].dt.year.isin([2015, 2016, 2017, 2018,2019])]
    # test_df = input_df[input_df['ds'].dt.year == 2020]

    # Splitting the data into train and test
    input_df['test_indicator'] = 0
    input_df.loc[input_df['ds'].dt.year == 2020, 'test_indicator'] = 1
    train_df = input_df[input_df['test_indicator'] == 0]
    test_df = input_df[input_df['test_indicator'] == 1]

   # Fit and predict using Prophet model
    p_fitted, p_forecast = fit_and_predict_prophet(train_df, test_df)

    prophet_rmse = calculate_metrics_2(test_df['y'], p_forecast['yhat'], 'Prophet')

    # Fit and predict using Orbit model
    o_fitted, o_forecast = fit_and_predict_orbit(train_df, test_df)

    orbit_rmse = calculate_metrics_2(test_df['y'], o_forecast['prediction'],'Orbit')

    # Prepare training data for meta-model
    meta_train = pd.DataFrame({
        'DLT': o_fitted['prediction'].values,
        'Prophet': p_fitted['yhat'].values,
        'Actual': train_df['y']})
    
    # Prepare test data for meta-model
    meta_test = pd.DataFrame({'DLT': o_forecast['prediction'],'Prophet': p_forecast['yhat']})
    print("meta_test shape is ",meta_test.shape)

    # Train the meta-model
    meta_model = LinearRegression()
    meta_model.fit(meta_train[['DLT', 'Prophet']], meta_train['Actual'])


    # Final predictions
    final_predictions = meta_model.predict(meta_test)

    # If you need to display or use these predictions:
    test_df['final_predictions'] = final_predictions
    print(type(test_df['final_predictions']))
    print(test_df[['ds', 'final_predictions']])
    print(type(test_df[['ds', 'final_predictions']]))
    print(test_df['final_predictions'].shape)

    

    # Calculate metrics
    stacking_rmse = calculate_metrics_2(test_df['y'], test_df['final_predictions'], 'Stacking Model')

    rmse_results = pd.DataFrame({'Ensemble Models': ['RMSE'],
                                 'Prophet': [prophet_rmse],
                                 'Orbit': [orbit_rmse],
                                 'Stacking': [stacking_rmse]
                            }).round(2)
    
    print(rmse_results)

    forecasted_data = pd.DataFrame({'date': test_df['ds'].values,
                                    'Sales': test_df['y'].values,
                                    'Prophet': p_forecast['yhat'],
                                    'Orbit': o_forecast['prediction'],
                                    'Stacking': test_df['final_predictions'].values
                                    }).round(2)

    print(forecasted_data.head(10))

    current_dir = os.path.dirname(os.path.abspath(__file__))
    data_folder = os.path.join(current_dir, '../data')
    filename_with_extension = 'aqi_stacking_lr_forecasting' + '.csv'
    file_path = os.path.join(data_folder, filename_with_extension)
    forecasted_data.to_csv(file_path, index=False)



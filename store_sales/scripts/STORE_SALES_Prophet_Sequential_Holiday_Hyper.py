import pandas as pd
import os 
import numpy as np
import fileUtils 
from prophet import Prophet
import matplotlib.pyplot as plt  
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
from datetime import datetime
import matplotlib.pyplot as plt
from statsmodels.tsa.seasonal import seasonal_decompose
from sklearn.model_selection import ParameterGrid
import ruptures as rpt
from statsmodels.nonparametric.smoothers_lowess import lowess
from statsmodels.tsa.seasonal import STL
import seaborn as sns

if __name__ == "__main__":

    input_df = fileUtils.read_csv('agg_store_sales')
    holiday_df = fileUtils.read_csv('eggs_store1_holidays')
    print(input_df.columns)
   
    holiday_df['type'] = 'non_working_day'
    holiday_df['lower_window'] = 0
    holiday_df['upper_window'] = 1
    holiday_df.rename(columns={'type':'holiday','holiday':'ds'}, inplace=True)
    # print(holiday_df.head(5))
    # input_df['cap'] = input_df['y'].max() * 1.1
    input_df['ds'] = pd.to_datetime(input_df['ds'])
    train_df = input_df[input_df['test_indicator']==0]
    test_df = input_df[input_df['test_indicator']==1]


    #Plot-1: Time Series plot
    plt.plot(train_df['ds'], train_df['y'] ,color='blue')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.title('Aggregate Store Sales')
    plt.show()

    #Plot-2: Linear and Non-linear trend analysis graph
    store_data = input_df['y']

    # Prepare data for trend analysis
    x = np.arange(len(store_data))
    y = store_data.values

    # Linear trend - Polynomial fitting
    z = np.polyfit(x, y, 1)  # 1 means linear
    p = np.poly1d(z)

    # Non-linear trend - LOESS (Locally Estimated Scatterplot Smoothing)
    loess_smoothed = lowess(y, x, frac=0.1)  # frac controls the degree of smoothing

    # Plotting
    plt.figure(figsize=(12, 6))
    plt.plot(store_data.index, y, label='Original Data', color='lightgray')
    plt.plot(store_data.index, p(x), label='Linear Trend', color='red')
    plt.plot(store_data.index, loess_smoothed[:, 1], label='LOESS Smoothed Curve', color='blue')
    plt.title('Linear and Non-linear Trend Analysis')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()

    # Plot-3: Seasonal De-composition analysis 
    store_sales_data = input_df['y']

    # STL for Yearly Seasonality
    stl_yearly = STL(store_sales_data, period=365)
    result_yearly = stl_yearly.fit()

    # STL for Monthly Seasonality
    stl_monthly = STL(store_sales_data, period=30)
    result_monthly = stl_monthly.fit()

    # STL for Weekly Seasonality
    stl_weekly = STL(store_sales_data, period=7)
    result_weekly = stl_weekly.fit()

    # Function to plot STL results
    def plot_stl(result, title):
        fig, ax = plt.subplots(4, 1, figsize=(10, 8), sharex=True)
        ax[0].plot(result.observed)
        ax[0].set_ylabel('Observed')
        ax[0].set_title(title)

        ax[1].plot(result.trend)
        ax[1].set_ylabel('Trend')

        ax[2].plot(result.seasonal)
        ax[2].set_ylabel('Seasonal')

        ax[3].plot(result.resid)
        ax[3].set_ylabel('Residual')
        ax[3].set_xlabel('Date')

        plt.tight_layout()
        plt.show()

    # Plotting results
    plot_stl(result_yearly, 'Yearly Seasonality')
    plot_stl(result_monthly, 'Monthly Seasonality')
    plot_stl(result_weekly, 'Weekly Seasonality')

    corr_df = train_df.copy()
    corr_df = corr_df[['onpromotion', 'dcoilwtico','transactions', 'y']]
    corr_df.rename(columns={'y':'sales'},inplace=True)
    print(corr_df.columns)

    corr_matrix = corr_df.corr()

    plt.figure(figsize=(10, 8))
    sns.heatmap(corr_matrix, annot=True, fmt=".2f", cmap='coolwarm', cbar=True, square=True)
    plt.title('Correlation Matrix of Store-I Sales data')
    plt.show()

    # # Strong features: onpromotion, dcoilwtico, transactions
    # Define the grid of parameters to search
    param_grid = {
        'changepoint_prior_scale': [0.01, 0.1, 0.5],
        'seasonality_prior_scale': [0.1, 1.0, 10.0],
        'seasonality_mode': ['additive', 'multiplicative'],
        # 'growth': ['linear','logistic'],
        'yearly_seasonality': [True,False],
        'weekly_seasonality': [True,False],
        'holidays_mode': ['additive', 'multiplicative']
    }

    # Setup a dataframe to store the results of each configuration
    results = pd.DataFrame(columns=['params', 'RMSE'])

    # Loop over all combinations of parameters
    for params in ParameterGrid(param_grid):
        model = Prophet(
            interval_width=0.95,
            growth='linear',
            yearly_seasonality=params['yearly_seasonality'],
            weekly_seasonality=params['weekly_seasonality'],
            changepoint_range=0.95,
            holidays=holiday_df,
            holidays_mode = params['holidays_mode'],
            seasonality_mode=params['seasonality_mode'],
            changepoint_prior_scale=params['changepoint_prior_scale'],
            seasonality_prior_scale=params['seasonality_prior_scale']
        )
        model.add_regressor('onpromotion')
        model.add_regressor('dcoilwtico')
        model.add_regressor('transactions')


        # Fit the model and make a forecast
        model.fit(train_df)
        forecast = model.predict(test_df)

        # Calculate RMSE for the current set of parameters
        rmse = np.sqrt(mean_squared_error(test_df['y'], forecast['yhat']))

        # Store the results
        new_row = pd.DataFrame({'params': [params], 'RMSE': [rmse]})
        results = pd.concat([results, new_row], ignore_index=True)
    
    # Find the best set of parameters
    best_params = results.loc[results['RMSE'].idxmin()]
    print("Best parameters found:", best_params['params'])
    print("Lowest RMSE achieved:", best_params['RMSE'])

    # Refit the model with the best parameters
    best_model = Prophet(
        interval_width=0.95,
        growth='linear',
        yearly_seasonality=best_params['params']['yearly_seasonality'],
        weekly_seasonality=best_params['params']['weekly_seasonality'],
        changepoint_range=0.95,
        holidays=holiday_df,
        holidays_mode = best_params['params']['holidays_mode'],
        # seasonality_mode=best_params['params']['seasonality_mode'],
        seasonality_mode=best_params['params']['seasonality_mode'],
        changepoint_prior_scale=best_params['params']['changepoint_prior_scale'],
        seasonality_prior_scale=best_params['params']['seasonality_prior_scale']
    )
    best_model.add_regressor('onpromotion')
    best_model.add_regressor('dcoilwtico')
    best_model.add_regressor('transactions')


    best_model.fit(train_df)
    predict_df = best_model.predict(test_df)

    forecast_df = pd.DataFrame({})
    forecast_df[['ds','yhat']] = predict_df[['ds','yhat']]
    forecast_df['sales'] = test_df['y'].values
    print(forecast_df.head(10))

    # Calculate and print final metrics
    final_rmse = np.sqrt(mean_squared_error(test_df['y'], forecast_df['yhat']))
    print(f"Final RMSE with best parameters: {final_rmse}")

    # Calculate metrics
    lr_mse = mean_squared_error(test_df['y'].astype(float), forecast_df['yhat'])
    lr_mae = mean_absolute_error(test_df['y'].astype(float), forecast_df['yhat'])

    # Apply the absolute value function to both y_eval and lr_predictions
    y_eval_abs = abs(test_df['y'].astype(float))
    lr_predictions_abs = abs(forecast_df['yhat'])

    # Calculate the Root Mean Squared Logarithmic Error (RMSLE)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))

    # Create a DataFrame to store results for Linear Regression
    results_lr = pd.DataFrame({'Model': ['Prophet Model'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]}).round(2)

    # Print the results_lr dataframe
    print(results_lr)




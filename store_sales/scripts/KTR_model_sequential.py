import pandas as pd
import numpy as np
# from sklearn.preprocessing import StandardScaler
from multiprocessing import Pool, cpu_count
from typing import List, Any
import orbit
from orbit.models import KTR 
from functools import partial
import fileUtils
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error
import matplotlib.pyplot as plt
from datetime import datetime 


if __name__ == "__main__":
    # try:
    input_df = fileUtils.read_csv('grocery_one_series')

    input_df['date'] = pd.to_datetime(input_df['date'], errors='coerce')
    input_df = input_df.sort_values(by='date')
    grouped_df = input_df.groupby(['store_nbr', 'date']).agg({'sales':'sum', 
                                                              'transactions':'sum', 
                                                              'onpromotion':'sum'}).reset_index()
    
    grouped_df['test_indicator'] = 0
    grouped_df.loc[grouped_df['date'].dt.year >= 2017, 'test_indicator'] = 1

    train_data = grouped_df.loc[grouped_df['test_indicator'] == 0]
    test_data = grouped_df.loc[grouped_df['test_indicator'] == 1]


    column_names = ['onpromotion', 'transactions']
    
    model_time = datetime.now()
    # model_timef = model_time.strftime("%Y-%m-%d %H:%M:%S")
    print("Model time:", model_time)
    model = KTR(response_col='sales', 
                date_col='date',
                estimator='pyro-svi',
                seed=2000, 
                seasonality=[182.625,365.25],
                regressor_col=column_names,
                prediction_percentiles=[5, 95],
                regressor_sign=['+','+'])
    model.fit(train_data)
    predict_time = datetime.now()
    # predict_timef = predict_time.strftime("%Y-%m-%d %H:%M:%S")
    # print("Training time is ",str((predict_timef-model_timef))," secs")
    print("Fitting Total time:", predict_time-model_time)
    print("Predict start time:", predict_time)
    forecast = model.predict(test_data)
    predict_complete_time = datetime.now()
    # predict_complete_timef = predict_complete_time.strftime("%Y-%m-%d %H:%M:%S")
    print("Predict End time:", predict_complete_time)
    print("Total Predict complete time:", predict_complete_time-predict_time)
    # print("Predict time is ",str((predict_complete_timef-predict_timef))," secs")
    print(forecast.head(20))

    forecast.to_csv('ktr_forecast_14_03_2024.csv')

    plt.figure(figsize=(10, 6))
    plt.plot(test_data['date'], test_data['sales'], label='Actual', color='blue')
    plt.plot(forecast['date'], forecast['prediction'], label='Forecast', color='red')
    plt.fill_between(forecast['date'], forecast['prediction_5'], forecast['prediction_95'], color='gray', alpha=0.3)
    plt.title('Actual vs. Forecast Data')
    plt.xlabel('Date')
    plt.ylabel('Sales')
    plt.legend()
    plt.show()

    # Calculate metrics
    lr_mse = mean_squared_error(test_data['sales'].astype(float), forecast['prediction'])
    lr_mae = mean_absolute_error(test_data['sales'].astype(float), forecast['prediction'])

    # Apply the absolute value function to both y_eval and lr_predictions
    y_eval_abs = abs(test_data['sales'].astype(float))
    lr_predictions_abs = abs(forecast['prediction'])

    # Calculate the Root Mean Squared Logarithmic Error (RMSLE)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))

    # Create a DataFrame to store results for Linear Regression
    results_lr = pd.DataFrame({'Model': ['Orbit KTR Model'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)],
                            # 'MSE': [lr_mse],
                            # 'MAE': [lr_mae]
                            }).round(2)

    # Print the results_lr dataframe
    print(results_lr)


    # plt.plot(test_data.index, test_data.values, label='Actual Data')
    # plt.plot(forecast.index, forecast.values, label='Forecast Data')
    # plt.xlabel('Date')
    # plt.ylabel('Sales')  
    # plt.title('Actual Data vs Forecast Data')
    # plt.legend()
    # plt.show()

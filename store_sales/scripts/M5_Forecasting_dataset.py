import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import fileUtils
import os


if __name__ == "__main__":
    # df = fileUtils.read_csv('sales_train_evaluation')

    # # Melt the dataframe to long format
    # df_melted = pd.melt(df, id_vars=['id', 'item_id', 'dept_id', 'cat_id', 'store_id', 'state_id'], 
    #                     var_name='d', value_name='sales')

    # # Extract the day number from the 'd' column and convert to date
    # df_melted['d'] = df_melted['d'].str.extract('(\d+)', expand=False).astype(int)

    # # Assuming the start date is known, e.g., '2011-01-29'
    # start_date = pd.to_datetime('2011-01-29')
    # df_melted['ds'] = start_date + pd.to_timedelta(df_melted['d'] - 1, unit='D')

    # # Drop the 'd' column as it's no longer needed
    # df_melted = df_melted.drop(columns=['d'])

    # # Renaming columns for Prophet
    # df_melted = df_melted.rename(columns={'sales': 'y'})

    # # Now df_melted is in a suitable format for Prophet
    # print(df_melted.head())

    # current_dir = os.path.dirname(os.path.abspath(__file__))
    # data_folder = os.path.join(current_dir, '../data')
    # filename_with_extension = 'm5_forecast_accuracy' + '.csv'
    # file_path = os.path.join(data_folder, filename_with_extension)
    # df_melted.to_csv(file_path, index=False)

    # df1 = fileUtils.read_csv('sales_train_validation')

    # # Melt the dataframe to long format
    # df1_melted = pd.melt(df1, id_vars=['id', 'item_id', 'dept_id', 'cat_id', 'store_id', 'state_id'], 
    #                     var_name='d', value_name='sales')

    # # Extract the day number from the 'd' column and convert to date
    # df1_melted['d'] = df1_melted['d'].str.extract('(\d+)', expand=False).astype(int)

    # # Assuming the start date is known, e.g., '2011-01-29'
    # start_date = pd.to_datetime(df_melted['ds'].max() + pd.Timedelta(days=1))
    # df1_melted['ds'] = start_date + pd.to_timedelta(df1_melted['d'] - 1, unit='D')

    # # Drop the 'd' column as it's no longer needed
    # df1_melted = df1_melted.drop(columns=['d'])

    # # Renaming columns for Prophet
    # df1_melted = df1_melted.rename(columns={'sales': 'y'})

    # # Now df_melted is in a suitable format for Prophet
    # print(df1_melted.head())

    # current_dir = os.path.dirname(os.path.abspath(__file__))
    # data_folder = os.path.join(current_dir, '../data')
    # filename_with_extension = 'm5_forecast_accuracy_test' + '.csv'
    # file_path = os.path.join(data_folder, filename_with_extension)
    # df1_melted.to_csv(file_path, index=False)

    df3 = fileUtils.read_csv('m5_forecast_accuracy')
    print(df3.shape)
    print(df3.columns)



import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error, mean_squared_log_error
from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.ensemble import RandomForestRegressor
from prophet import Prophet
from orbit.models.dlt import DLT
import matplotlib.pyplot as plt
import fileUtils

# Custom estimator for Prophet
class ProphetEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols, holiday_df):
        self.model = None
        self.regressor_cols = regressor_cols
        self.holiday_df = holiday_df
        
    def fit(self, X, y):
        # Convert numpy array back to DataFrame if necessary
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        df = X.copy()
        df['y'] = y  # Ensure y is a pandas Series and align indices
        self.model = Prophet(interval_width=0.95,
                             growth='linear',
                             yearly_seasonality=False,
                             weekly_seasonality=False,
                             changepoint_range=0.95,
                             changepoint_prior_scale=0.1,
                             holidays=self.holiday_df,
                             holidays_mode='multiplicative',
                             seasonality_mode='additive',
                             seasonality_prior_scale=0.01)
        for reg in self.regressor_cols:
            if reg != 'change_point_effect':  # Exclude change_point_effect for Prophet
                self.model.add_regressor(reg)
        self.model.fit(df)
        return self
    
    def predict(self, X):
        # Convert numpy array back to DataFrame if necessary
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        forecast = self.model.predict(X)
        return forecast['yhat'].values

# Custom estimator for Orbit's DLT
class DLTModelEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols):
        self.model = None
        self.regressor_cols = regressor_cols
        
    def fit(self, X, y):
        # Convert numpy array back to DataFrame if necessary
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        df = X.copy()
        df['y'] = y  # Ensure y is a pandas Series and align indices
        self.model = DLT(response_col='y',
                         date_col='ds',
                         seasonality=730,
                         estimator='stan-map',
                         global_trend_option='linear',
                         damped_factor=0.7,
                         regressor_col=self.regressor_cols)
        self.model.fit(df)
        return self
    
    def predict(self, X):
        # Convert numpy array back to DataFrame if necessary
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        forecast = self.model.predict(X)
        return forecast['prediction'].values

# Custom stacked estimator that combines Prophet and DLT
class StackedEstimator(BaseEstimator, RegressorMixin):
    def __init__(self, regressor_cols, holiday_df):
        self.regressor_cols = regressor_cols
        self.holiday_df = holiday_df
        self.prophet_estimator = ProphetEstimator(regressor_cols, holiday_df)
        self.dlt_estimator = DLTModelEstimator(regressor_cols)
        
    def fit(self, X, y):
        # Ensure X is a DataFrame
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        self.prophet_estimator.fit(X, y)
        self.dlt_estimator.fit(X, y)
        return self
    
    def predict(self, X):
        # Ensure X is a DataFrame
        if isinstance(X, np.ndarray):
            X = pd.DataFrame(X, columns=self.regressor_cols + ['ds'])
        prophet_pred = self.prophet_estimator.predict(X)
        dlt_pred = self.dlt_estimator.predict(X)
        return np.vstack((prophet_pred, dlt_pred)).T  # Combine predictions as features
    
    def get_params(self, deep=True):
        return {'regressor_cols': self.regressor_cols, 'holiday_df': self.holiday_df}
    
    def set_params(self, **params):
        self.regressor_cols = params.get('regressor_cols', self.regressor_cols)
        self.holiday_df = params.get('holiday_df', self.holiday_df)
        self.prophet_estimator = ProphetEstimator(self.regressor_cols, self.holiday_df)
        self.dlt_estimator = DLTModelEstimator(self.regressor_cols)
        return self

# Function to calculate RMSE
def rmse(y_true, y_pred):
    return np.sqrt(mean_squared_error(y_true, y_pred))

def calculate_metrics(test_value, forecast_value, model_name):
    lr_mse = mean_squared_error(test_value.astype(float), forecast_value)
    y_eval_abs = abs(test_value.astype(float))
    lr_predictions_abs = abs(forecast_value)
    lr_rmsle = np.sqrt(mean_squared_log_error(y_eval_abs, lr_predictions_abs))
    results_lr = pd.DataFrame({'Model': [f'{model_name}'],
                            'RMSLE': [lr_rmsle],
                            'RMSE': [np.sqrt(lr_mse)]
                            }).round(2)
    return results_lr

if __name__ == "__main__":
    input_df = fileUtils.read_csv('aqi_delhi_cleaned_data')
    holiday_df = fileUtils.read_csv('aqi_delhi_holidays')  
    holiday_df['type'] = 'non_working_day'
    holiday_df['lower_window'] = 0
    holiday_df['upper_window'] = 1
    holiday_df.rename(columns={'type':'holiday','holiday':'ds'}, inplace=True)
    print(holiday_df.head(5))

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    change_points = pd.to_datetime(holiday_df['ds'].values)
    change_point_series = pd.Series(0, index=input_df.index)
    for cp in change_points:
        # Mark the period after each change point
        change_point_series.loc[input_df['ds'] >= cp] += 1
    input_df['change_point_effect'] = change_point_series

    input_df['ds'] = pd.to_datetime(input_df['ds'])
    train_df = input_df[input_df['test_indicator'] == 0]
    test_df = input_df[input_df['test_indicator'] == 1]

    # Define regressor columns
    regressor_cols = ['pm2_5', 'pm10', 'no', 'no2', 'nox', 'nh3', 'benzene', 'change_point_effect']

    # Ensure 'ds' column is included in the training and test DataFrames
    train_df_with_ds = train_df[regressor_cols + ['ds']]
    test_df_with_ds = test_df[regressor_cols + ['ds']]

    # Create and fit the custom stacked estimator
    stacked_estimator = StackedEstimator(regressor_cols=regressor_cols, holiday_df=holiday_df)
    stacked_estimator.fit(train_df_with_ds, train_df['y'])

    # Predict on the training data to create combined features for RandomForestRegressor
    combined_train_pred = stacked_estimator.predict(train_df_with_ds)
    combined_fitted_pred = pd.DataFrame(combined_train_pred, columns=['prophet_fitted', 'dlt_fitted'])
    combined_test_pred = stacked_estimator.predict(test_df_with_ds)
    combined_pred = pd.DataFrame(combined_test_pred, columns=['prophet_fitted', 'dlt_fitted'])

    # Train RandomForestRegressor with the custom stacked estimator
    rf_model = RandomForestRegressor(n_estimators=100, random_state=42)
    rf_model.fit(combined_fitted_pred, train_df['y'])  # Use combined_fitted_pred

    # Predict using RandomForestRegressor
    final_predictions = rf_model.predict(combined_pred)

    # Evaluate the ensemble's performance
    test_rmse = rmse(test_df['y'], final_predictions)
    print(f'Final RMSE of Random Forest ensemble: {test_rmse}')

    # Plot the results
    plt.figure(figsize=(12, 6))
    plt.plot(test_df['ds'], test_df['y'], label='Actual')
    plt.plot(test_df['ds'], final_predictions, label='Ensemble Prediction')
    plt.xlabel('Date')
    plt.ylabel('Value')
    plt.legend()
    plt.show()
